# Flashcard backend application to ease learning

## Environment
* `Java: 1.8.0.201`
* `Docker version 19.03.8`

## Development:
#### Starting application locally in debug mode:
* Run docker containers: `docker stack deploy -c stack.yml flashcards-stack`
* In intellij use maven configuration: `clean spring-boot:run -Dspring-boot.run.profiles=dev -Dspring-boot.run.fork=false`

#### Sample user

* login: `learner1`
* pass: `learner1`

#### Managing/monitoring kafka
To manage queue log into http://localhost:15672

#### Managing/monitoring PostgreSql
	POSTGRES_USER: dev
	POSTGRES_PASSWORD: dev-pass
	POSTGRES_DB: local-flashcard
	
#### Swagger API
`http://localhost:8080/swagger-ui.html`

#### Tests running
`mvn clean test -Dtag=unit,it`

####  Kafka on docker, development useful commands
* producer: `docker exec -it container_id /opt/bitnami/kafka/bin/kafka-console-producer.sh --broker-list localhost:9092 --topic my-topic`
* receicer: `docker exec -it container_id /opt/bitnami/kafka/bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic my-topic --from-beginning --max-messages 100`
* topics:   `docker exec -it container_id /opt/bitnami/kafka/bin/kafka-topics.sh --list --zookeeper zookeeper:2181`

#### Another info
* It is required to install lombok in Intellij

## TODO
* Provide SSL lithlad.flashcards.communication
* https://codecouple.pl/2017/09/22/piata-planeta-w-ukladzie-czyli-junit-5-jupiter/
* Tests to assure packaging correctness
