package lithlad.flashcards.communication.learner.event.infrastructure;

public class LearnerInfrastructureConstants {

	private LearnerInfrastructureConstants() {}

	public static final String PACKAGE = "lithlad.flashcards.communication.learner.event";
}
