package lithlad.flashcards.communication.learner.event;

import org.springframework.lang.Nullable;

import lithlad.flashcards.communication.learner.LearnerEventsIdentifires;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
@EqualsAndHashCode(exclude = { "concurrencyVersion", "eventDate" })
public class LearnerWasCreatedEvent extends LearnerEvent {

    @NonNull
    private String email;

    @NonNull
    private String mothertongue;

    @Nullable
    private String defaultForeignLanguage;

    @NonNull
    private long creationDate;

    public LearnerWasCreatedEvent(final @NonNull String login, final @NonNull String email, final @NonNull String mothertongue,
                                  @Nullable final String defaultForeignLanguage, final @NonNull long creationDate, final int concurrencyVersion) {
        super(login, concurrencyVersion);
        this.email = email;
        this.mothertongue = mothertongue;
        this.defaultForeignLanguage = defaultForeignLanguage;
        this.creationDate = creationDate;
    }

    @Override
    public String eventIdentifier() {
        return LearnerEventsIdentifires.LEARNER_WAS_CREATED;
    }

    @Override
    public String toString() {
        return "LearnerWasCreatedEvent{" +
                "email='" + email + '\'' +
                ", mothertongue='" + mothertongue + '\'' +
                ", defaultForeignLanguage='" + defaultForeignLanguage + '\'' +
                ", creationDate=" + creationDate +
                '}';
    }
}
