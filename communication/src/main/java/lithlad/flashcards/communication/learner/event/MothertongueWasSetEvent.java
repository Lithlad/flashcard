package lithlad.flashcards.communication.learner.event;

import lithlad.flashcards.communication.learner.LearnerEventsIdentifires;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;

@Getter
@EqualsAndHashCode(exclude = { "concurrencyVersion", "eventDate" })
public class MothertongueWasSetEvent extends LearnerEvent {

    @NonNull
    private final String language;

    public MothertongueWasSetEvent(@NonNull final String login, @NonNull final String language, final int concurrencyVersion) {
        super(login, concurrencyVersion);
        this.language = language;
    }

    @Override
    public String eventIdentifier() {
        return LearnerEventsIdentifires.MOTHERTONGUE_WAS_SET;
    }

    @Override
    public String toString() {
        return "MothertongueWasSetEvent{" +
                "language='" + language + '\'' +
                '}';
    }
}
