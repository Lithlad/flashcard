package lithlad.flashcards.communication.learner.event;

import lithlad.flashcards.communication.learner.LearnerEventsIdentifires;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;

@Getter
@EqualsAndHashCode(callSuper = true)
public final class DefaultLanguageWasSetEvent extends LearnerEvent {

    @NonNull
    private final String language;

    public DefaultLanguageWasSetEvent(@NonNull final String login, @NonNull final String language, final int concurrencyVersion) {
        super(login, concurrencyVersion);
        this.language = language;
    }

    @Override
    public String eventIdentifier() {
        return LearnerEventsIdentifires.DEFAULT_LANGUAGE_WAS_SET;
    }
}
