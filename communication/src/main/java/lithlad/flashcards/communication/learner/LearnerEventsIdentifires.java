package lithlad.flashcards.communication.learner;

public class LearnerEventsIdentifires {

    private LearnerEventsIdentifires() {}

    public static final String LEARNER_DOMAIN = "learner_domain";

    public static final String DEFAULT_LANGUAGE_WAS_SET = "default_language_was_set_queue";
    public static final String MOTHERTONGUE_WAS_SET = "mothertongue_was_set_queue";
    public static final String LEARNER_WAS_CREATED = "learner_was_created_queue";
}
