package lithlad.flashcards.communication.learner.event;

import lithlad.flashcards.communication.learner.LearnerEventsIdentifires;
import design.domain.DomainEvent;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public abstract class LearnerEvent extends DomainEvent {

    @NonNull
    private String login;

    LearnerEvent(@NonNull final String login, final int concurrencyVersion) {
        super(concurrencyVersion);
        this.login = login;
    }

    @Override
    public String eventDomain() {
        return LearnerEventsIdentifires.LEARNER_DOMAIN;
    }
}
