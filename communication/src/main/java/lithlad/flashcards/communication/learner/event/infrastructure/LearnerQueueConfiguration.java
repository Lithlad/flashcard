package lithlad.flashcards.communication.learner.event.infrastructure;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lithlad.flashcards.communication.learner.LearnerEventsIdentifires;
import design.infrastructure.queue.configuration.QueueConfiguration;

@Configuration
class LearnerQueueConfiguration extends QueueConfiguration {

	@Bean(name = "mothertongueWasSetTopic")
	public NewTopic mothertongueWasSetTopic() {
		return generateTopic(LearnerEventsIdentifires.MOTHERTONGUE_WAS_SET);
	}

	@Bean(name = "learnerWasCreatedTopic")
	public NewTopic learnerWasCreatedTopic() {
		return generateTopic(LearnerEventsIdentifires.LEARNER_WAS_CREATED);
	}
}
