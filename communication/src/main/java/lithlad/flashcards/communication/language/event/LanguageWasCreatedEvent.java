package lithlad.flashcards.communication.language.event;

import lithlad.flashcards.communication.language.LanguageEventsIdentifires;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;

@Getter
@EqualsAndHashCode(callSuper = true)
public final class LanguageWasCreatedEvent extends LanguageEvent {

	@NonNull
	private final String country;

	@NonNull
	private final long creationDate;

	public LanguageWasCreatedEvent(final @NonNull String code, final @NonNull String country, final @NonNull long creationDate, final int concurrencyVersion) {
		super(code, concurrencyVersion);
		this.country = country;
		this.creationDate = creationDate;
	}

	@Override
	public String eventIdentifier() {
		return LanguageEventsIdentifires.LANGUAGE_WAS_CREATED;
	}
}
