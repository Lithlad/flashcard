package lithlad.flashcards.communication.language;

import design.infrastructure.queue.configuration.QueueConfiguration;

abstract class LanguageQueueConfiguration extends QueueConfiguration {}
