package lithlad.flashcards.communication.language;

public class LanguageEventsIdentifires {

	private LanguageEventsIdentifires() {}

	public static final String LANGUAGE_DOMAIN = "language_domain";

	public static final String LANGUAGE_WAS_CREATED = "language_was_created_queue";
}
