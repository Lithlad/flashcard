package lithlad.flashcards.communication.language.event;

import lithlad.flashcards.communication.language.LanguageEventsIdentifires;
import design.domain.DomainEvent;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;

@Getter
@EqualsAndHashCode(callSuper = true)
public abstract class LanguageEvent extends DomainEvent {

	@NonNull
	private final String code;

	protected LanguageEvent(final @NonNull String code, final int concurrencyVersion) {
		super(concurrencyVersion);
		this.code = code;
	}

	@Override
	public String eventDomain() {
		return LanguageEventsIdentifires.LANGUAGE_DOMAIN;
	}
}
