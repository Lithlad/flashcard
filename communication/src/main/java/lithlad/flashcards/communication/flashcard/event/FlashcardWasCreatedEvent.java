package lithlad.flashcards.communication.flashcard.event;

import java.time.LocalDateTime;

import lithlad.flashcards.communication.flashcard.FlashcardEventsIdentifires;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class FlashcardWasCreatedEvent extends FlashcardEvent {
	private String translatedWord;
	private String ownerLogin;
	private String visibility;
	private LocalDateTime creationDate;

	public FlashcardWasCreatedEvent(final @NonNull String domesticLanguage,
	                                final @NonNull String translationLanguage,
	                                final @NonNull String domesticWord,
	                                final @NonNull String translatedWord,
	                                final @NonNull String ownerLogin,
	                                final String visibility,
	                                final @NonNull LocalDateTime creationDate,
	                                final int concurrencyVersion) {
		super(domesticLanguage, translationLanguage, domesticWord, concurrencyVersion);
		this.translatedWord = translatedWord;
		this.ownerLogin = ownerLogin;
		this.visibility = visibility;
		this.creationDate = creationDate;
	}

	@Override
	public String eventIdentifier() {
		return FlashcardEventsIdentifires.FLASHCARD_WAS_CREATED;
	}
}
