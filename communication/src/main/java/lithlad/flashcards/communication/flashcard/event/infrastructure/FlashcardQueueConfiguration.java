package lithlad.flashcards.communication.flashcard.event.infrastructure;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lithlad.flashcards.communication.flashcard.FlashcardEventsIdentifires;
import design.infrastructure.queue.configuration.QueueConfiguration;

@Configuration
public class FlashcardQueueConfiguration extends QueueConfiguration {

	@Bean(name = "flashcardWasCreatedEventTopic")
	public NewTopic flashcardWasCreatedTopic() {
		return generateTopic(FlashcardEventsIdentifires.FLASHCARD_WAS_CREATED);
	}
}
