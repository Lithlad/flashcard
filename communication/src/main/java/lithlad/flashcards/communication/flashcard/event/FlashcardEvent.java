package lithlad.flashcards.communication.flashcard.event;

import lithlad.flashcards.communication.flashcard.FlashcardEventsIdentifires;
import design.domain.DomainEvent;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public abstract class FlashcardEvent extends DomainEvent {

	@NonNull
	private String domesticLanguage;

	@NonNull
	private String translationLanguage;

	@NonNull
	private String domesticWord;

	protected FlashcardEvent(final String domesticLanguage, final String translationLanguage, final String domesticWord, final int concurrencyVersion) {
		super(concurrencyVersion);
		this.domesticLanguage = domesticLanguage;
		this.translationLanguage = translationLanguage;
		this.domesticWord = domesticWord;
	}

	@Override
	public String eventDomain() {
		return FlashcardEventsIdentifires.FLASHCARD_DOMAIN;
	}
}
