package lithlad.flashcards.communication.flashcard.event.infrastructure;

public class FlashcardInfrastructureConstants {

	private FlashcardInfrastructureConstants() {}

	public static final String PACKAGE = "lithlad.flashcards.communication.flashcard.event";
}
