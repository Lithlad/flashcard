package lithlad.flashcards.communication.flashcard;

public class FlashcardEventsIdentifires {

	private FlashcardEventsIdentifires() {}

	public static final String FLASHCARD_DOMAIN = "flashcard_domain";

	public static final String FLASHCARD_WAS_CREATED = "flashcard_was_created_queue";
}
