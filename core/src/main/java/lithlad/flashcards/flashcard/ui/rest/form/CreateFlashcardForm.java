package lithlad.flashcards.flashcard.ui.rest.form;

import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lithlad.flashcards.flashcard.application.command.commands.CreateFlashcardCommand;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@ApiModel
@EqualsAndHashCode
public class CreateFlashcardForm implements CreateFlashcardCommand {

	@NotBlank
	@ApiModelProperty(value = "Domestic language", required = true, example="PL")
	private final String domesticLang;

	@NotBlank
	@ApiModelProperty(value = "Translation language", required = true, example="EN")
	private final String translationLang;

	@NotBlank
	@ApiModelProperty(value = "Domestic word", required = true, example="pies")
	private final String domesticWord;

	@NotBlank
	@ApiModelProperty(value = "Translated word", required = true, example="dog")
	private final String translatedWord;

	public CreateFlashcardForm(final @NotBlank String domesticLang, final @NotBlank String translationLang,
	                           final @NotBlank String domesticWord, final @NotBlank String translatedWord) {
		this.domesticLang = domesticLang;
		this.translationLang = translationLang;
		this.domesticWord = domesticWord;
		this.translatedWord = translatedWord;
	}
}
