package lithlad.flashcards.flashcard.ui.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lithlad.flashcards.flashcard.application.command.FlashcardApplicationService;
import lithlad.flashcards.flashcard.domain.dto.FlashcardDTO;
import lithlad.flashcards.flashcard.ui.rest.form.CreateFlashcardForm;
import lombok.NonNull;

@Api
@RestController
@RequestMapping("/api/flashcard")
public class FlashcardController {

	private final FlashcardApplicationService flashcardApplicationService;

	@Autowired
	public FlashcardController(final FlashcardApplicationService flashcardApplicationService) {
		this.flashcardApplicationService = flashcardApplicationService;
	}

	@ApiOperation("Flashcard creation")
	@PostMapping("/create")
	protected ResponseEntity<FlashcardDTO> registerLearner(@ApiParam(required = true)
	                                                            @NonNull @Validated @RequestBody final CreateFlashcardForm form) {
		final FlashcardDTO flashcard = flashcardApplicationService.create(form);
		return ResponseEntity.ok(flashcard);
	}
}
