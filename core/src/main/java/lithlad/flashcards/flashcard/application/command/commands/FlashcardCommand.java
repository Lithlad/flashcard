package lithlad.flashcards.flashcard.application.command.commands;

public interface FlashcardCommand {

	String getDomesticLang();

	String getTranslationLang();

	String getDomesticWord();
}
