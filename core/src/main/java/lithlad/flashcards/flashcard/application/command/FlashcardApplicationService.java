package lithlad.flashcards.flashcard.application.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionalEventListener;

import lithlad.flashcards.communication.flashcard.event.FlashcardEvent;
import lithlad.flashcards.crossdomain.domain.LanguageCode;
import design.application.command.ApplicationService;
import design.domain.CreationDate;
import design.domain.DomainEvent;
import design.infrastructure.TimeDefinitions;
import design.user.application.query.UserViewQueryService;
import design.user.domain.Login;
import lithlad.flashcards.flashcard.application.command.commands.CreateFlashcardCommand;
import lithlad.flashcards.flashcard.domain.dto.FlashcardDTO;
import lithlad.flashcards.flashcard.domain.Flashcard;
import lithlad.flashcards.flashcard.domain.FlashcardFactory;
import lithlad.flashcards.flashcard.domain.FlashcardRepository;
import lithlad.flashcards.flashcard.domain.Word;
import lithlad.flashcards.flashcard.domain.exceptions.FlashcardAlreadyExistsException;
import lombok.NonNull;

@Service
public class FlashcardApplicationService extends ApplicationService {

	private final FlashcardRepository flashcardRepository;
	private final TimeDefinitions timeDefinitions;
	private final UserViewQueryService userViewQueryService;

	@Autowired
	public FlashcardApplicationService(final FlashcardRepository flashcardRepository,
	                                   final TimeDefinitions timeDefinitions,
	                                   final UserViewQueryService userViewQueryService) {
		this.flashcardRepository = flashcardRepository;
		this.timeDefinitions = timeDefinitions;
		this.userViewQueryService = userViewQueryService;
	}

	@PreAuthorize("hasAuthority('LEARNER')")
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public FlashcardDTO create(@NonNull final CreateFlashcardCommand command) {
		final LanguageCode domesticLanguage = LanguageCode.create(command.getDomesticLang());
		final LanguageCode translationLanguage = LanguageCode.create(command.getTranslationLang());
			final Word domesticWord = Word.create(command.getDomesticWord());
		final Word translatedWord = Word.create(command.getTranslatedWord());
		final Login ownerLogin = userViewQueryService.currentUserLogin();

		if (flashcardRepository.doesExist(domesticLanguage, translationLanguage, domesticWord, ownerLogin))
			throw new FlashcardAlreadyExistsException(domesticLanguage, translationLanguage, domesticWord);

		final Flashcard flashcard =
			FlashcardFactory.create(domesticLanguage, translationLanguage, domesticWord,
				translatedWord, ownerLogin, CreationDate.create(timeDefinitions.now()));

		return flashcardRepository.save(flashcard).mapToDTO();
	}

	@Override
	@TransactionalEventListener(value = FlashcardEvent.class)
	protected void actionWasRaised(final @NonNull DomainEvent event) {
		superActionWasRaised(event);
	}
}
