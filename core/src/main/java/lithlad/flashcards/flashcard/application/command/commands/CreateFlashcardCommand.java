package lithlad.flashcards.flashcard.application.command.commands;

public interface CreateFlashcardCommand extends FlashcardCommand {

	String getTranslatedWord();
}
