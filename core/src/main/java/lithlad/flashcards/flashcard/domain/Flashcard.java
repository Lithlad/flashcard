package lithlad.flashcards.flashcard.domain;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import lithlad.flashcards.communication.flashcard.event.FlashcardWasCreatedEvent;
import lithlad.flashcards.crossdomain.domain.LanguageCode;
import design.domain.Aggregate;
import design.domain.CreationDate;
import design.user.domain.Login;
import lithlad.flashcards.flashcard.domain.dto.FlashcardDTO;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.ToString;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "fcrd_flashcard")
@NoArgsConstructor
public class Flashcard extends Aggregate {

    @NonNull
    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "domestic_lang"))
    private LanguageCode domesticLang;

    @NonNull
    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "translation_lang"))
    private LanguageCode translationLang;

    @NonNull
    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "domestic_word"))
    private Word domesticWord;

    @NonNull
    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "translated_word"))
    private Word translatedWord;

    @NonNull
    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "owner_login"))
    private Login ownerLogin;

    @Enumerated(EnumType.STRING)
    @Column(name = "visibility", nullable = false)
    private Visibility visibility;

    Flashcard(@NonNull final LanguageCode domesticLanguage,
                     @NonNull final LanguageCode translationLanguage,
                     @NonNull final Word domesticWord,
                     @NonNull final Word translatedWord,
                     @NonNull final Login ownerLogin,
                     @NonNull final Visibility visibility,
                     @NonNull final CreationDate creationDate) {

        this.domesticLang = domesticLanguage;
        this.translationLang = translationLanguage;
        this.domesticWord = domesticWord;
        this.translatedWord = translatedWord;
        this.ownerLogin = ownerLogin;
        this.visibility = visibility;
        this.creationDate = creationDate;
        registerEvent(
            new FlashcardWasCreatedEvent(domesticLanguage.value(), translationLanguage.value(), domesticWord.value(),
                translatedWord.value(), ownerLogin.value(), visibility.name(), creationDate.value(), getConcurrencyVersion()));
    }

    public FlashcardDTO mapToDTO() {
        return new FlashcardDTO(
            domesticLang.value(),
            domesticWord.value(),
            ownerLogin.value(),
            translatedWord.value(),
            translationLang.value(),
            visibility.toString()
        );

    }
}
