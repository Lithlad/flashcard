package lithlad.flashcards.flashcard.domain;

public enum Visibility {
	PRIVATE, GROUP_FOR_USE, GROUP_FOR_EDIT
}