package lithlad.flashcards.flashcard.domain.exceptions;

import static java.lang.String.format;

import lithlad.flashcards.crossdomain.domain.LanguageCode;
import design.domain.exceptions.DomainAlreadyExistsException;
import lithlad.flashcards.flashcard.domain.Word;

public class FlashcardAlreadyExistsException extends DomainAlreadyExistsException {
	public FlashcardAlreadyExistsException(final LanguageCode domesticLanguage, final LanguageCode translationLanguage, final Word domesticWord) {
		super(format("Flashcard %s %s %s already exists for this user", domesticLanguage.value(), translationLanguage.value(), domesticWord.value()),
			"flashcard");
	}
}
