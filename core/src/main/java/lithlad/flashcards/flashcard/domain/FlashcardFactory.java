package lithlad.flashcards.flashcard.domain;

import lithlad.flashcards.crossdomain.domain.LanguageCode;
import design.domain.CreationDate;
import design.user.domain.Login;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FlashcardFactory {

	public static Flashcard create(@NonNull final LanguageCode domesticLanguage,
	                               @NonNull final LanguageCode translationLanguage,
	                               @NonNull final Word domesticWord,
	                               @NonNull final Word translatedWord,
	                               @NonNull final Login ownerLogin,
	                               @NonNull final CreationDate creationDate
	) {
		return new Flashcard(domesticLanguage, translationLanguage, domesticWord, translatedWord, ownerLogin, Visibility.PRIVATE, creationDate);
	}
}
