package lithlad.flashcards.flashcard.domain;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;

import design.domain.StringValueObject;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Embeddable
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = true)
public class Word extends StringValueObject {

	public static Word create(@NotBlank final String word) {
		return new Word(word);
	}

	private Word(@NotBlank final String word) {
		super(word, 255);
	}
}
