package lithlad.flashcards.flashcard.domain;

import lithlad.flashcards.crossdomain.domain.LanguageCode;
import design.user.domain.Login;
import lombok.NonNull;

public interface FlashcardRepository {
	boolean doesExist(
		@NonNull final LanguageCode domesticLanguage,
		@NonNull final LanguageCode translationLanguage,
		@NonNull final Word domesticWord,
		@NonNull final Login ownerLogin);

	Flashcard save(@NonNull final Flashcard any);
}
