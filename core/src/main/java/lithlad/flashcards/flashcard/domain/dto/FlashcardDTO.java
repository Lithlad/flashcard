package lithlad.flashcards.flashcard.domain.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
public class FlashcardDTO {
	private final String domesticLanguage;
	private final String domesticWord;
	private final String ownerLogin;
	private final String translatedWord;
	private final String translationLang;
	private final String visibility;

	public FlashcardDTO(@NonNull  final String domesticLanguage,
	                    @NonNull  final String domesticWord,
	                    @NonNull  final String ownerLogin,
	                    @NonNull  final String translatedWord,
	                    @NonNull  final String translationLang,
	                    @NonNull  final String visibility) {

		this.domesticLanguage = domesticLanguage;
		this.domesticWord = domesticWord;
		this.ownerLogin = ownerLogin;
		this.translatedWord = translatedWord;
		this.translationLang = translationLang;
		this.visibility = visibility;
	}
}
