package lithlad.flashcards.flashcard.infrastructure.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import lithlad.flashcards.crossdomain.domain.LanguageCode;
import design.user.domain.Login;
import lithlad.flashcards.flashcard.domain.Flashcard;
import lithlad.flashcards.flashcard.domain.Word;
import lombok.NonNull;

public interface JpaFlashcardRepository extends JpaRepository<Flashcard, Long> {

	boolean existsByDomesticLangAndTranslationLangAndDomesticWordAndOwnerLogin(
		@NonNull LanguageCode domesticLanguage, @NonNull LanguageCode translationLanguage,
		@NonNull Word domesticWord, @NonNull Login ownerLogin);
}
