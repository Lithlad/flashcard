package lithlad.flashcards.flashcard.infrastructure.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import lithlad.flashcards.crossdomain.domain.LanguageCode;
import design.user.domain.Login;
import lithlad.flashcards.flashcard.domain.Flashcard;
import lithlad.flashcards.flashcard.domain.FlashcardRepository;
import lithlad.flashcards.flashcard.domain.Word;
import lombok.NonNull;

@Repository
public class FlashcardRepositoryFacade implements FlashcardRepository {

	private final JpaFlashcardRepository jpaFlashcardRepository;

	@Autowired
	public FlashcardRepositoryFacade(final JpaFlashcardRepository jpaFlashcardRepository) {
		this.jpaFlashcardRepository = jpaFlashcardRepository;
	}

	@Override
	public boolean doesExist(final @NonNull LanguageCode domesticLanguage, final @NonNull LanguageCode translationLanguage,
	                         final @NonNull Word domesticWord, final @NonNull Login ownerLogin) {
		return jpaFlashcardRepository.existsByDomesticLangAndTranslationLangAndDomesticWordAndOwnerLogin(
			domesticLanguage, translationLanguage, domesticWord, ownerLogin);
	}

	@Override
	public Flashcard save(final @NonNull Flashcard flashcard) {
		return jpaFlashcardRepository.save(flashcard);
	}
}
