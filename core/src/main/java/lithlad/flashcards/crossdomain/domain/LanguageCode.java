package lithlad.flashcards.crossdomain.domain;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;

import design.domain.StringValueObject;
import lithlad.flashcards.crossdomain.domain.exceptions.InappropriateSignsInLanguageCodeException;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Embeddable
@NoArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class LanguageCode extends StringValueObject {

	public static LanguageCode create(@NotBlank final String code) {
		validate(code);
		return new LanguageCode(code);
	}

	private LanguageCode(@NotBlank final String code) {
		super(code, 2);
	}

	private static void validate(final String code) {
		if (!code.matches("[A-Z]{2}"))
			throw new InappropriateSignsInLanguageCodeException(code);
	}
}
