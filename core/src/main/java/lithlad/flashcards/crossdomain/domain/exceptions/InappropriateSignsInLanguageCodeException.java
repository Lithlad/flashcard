package lithlad.flashcards.crossdomain.domain.exceptions;

import static java.lang.String.format;

import design.domain.exceptions.DomainRequirementsNotFulfilledException;

public class InappropriateSignsInLanguageCodeException extends DomainRequirementsNotFulfilledException {
	public InappropriateSignsInLanguageCodeException(final String code) {
		super(format("Language code must contain exactly two uppercase letters. Code %s doesnt fulfill this requirement", code),
			"inappropriate.signs.in.code");
	}
}
