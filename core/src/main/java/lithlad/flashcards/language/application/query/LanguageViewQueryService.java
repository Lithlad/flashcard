package lithlad.flashcards.language.application.query;

import javax.validation.constraints.NotBlank;

public interface LanguageViewQueryService {

	boolean doesExist(@NotBlank final String code);
}
