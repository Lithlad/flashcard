package lithlad.flashcards.language.application.command.commands;

public interface LanguageCommand {

	String getCode();
}
