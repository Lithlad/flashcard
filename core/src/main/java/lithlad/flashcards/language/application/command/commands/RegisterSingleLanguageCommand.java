package lithlad.flashcards.language.application.command.commands;

public interface RegisterSingleLanguageCommand extends LanguageCommand {
	String getCountry();
}
