package lithlad.flashcards.language.application.command;

import static java.lang.String.format;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toSet;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionalEventListener;

import lithlad.flashcards.communication.language.event.LanguageEvent;
import lithlad.flashcards.crossdomain.domain.LanguageCode;
import design.application.command.ApplicationService;
import design.application.command.commands.Information;
import design.domain.CreationDate;
import design.domain.DomainEvent;
import design.infrastructure.TimeDefinitions;
import lithlad.flashcards.language.application.command.commands.RegisterLanguagesCommand;
import lithlad.flashcards.language.domain.Country;
import lithlad.flashcards.language.domain.Language;
import lithlad.flashcards.language.domain.LanguageFactory;
import lithlad.flashcards.language.domain.LanguageRepository;
import lombok.NonNull;

@Service
public class LanguageApplicationService extends ApplicationService {

	private static final String EXISTING_CODES = "existingCodes";
	private static final String EXISTING_COUNTRIES = "existingCountries";
	private static final String TO_SAVE = "toSave";

	private final LanguageRepository languageRepository;

	private final TimeDefinitions timeDefinitions;

	@Autowired
	public LanguageApplicationService(final LanguageRepository languageRepository, final TimeDefinitions timeDefinitions) {
		this.languageRepository = languageRepository;
		this.timeDefinitions = timeDefinitions;
	}

	@PreAuthorize("hasAuthority('ADMIN')")
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Information> registerLanguages(final RegisterLanguagesCommand registerLanguagesCommand) {
		final Set<Pair<LanguageCode, Country>> codeAndCountryList = registerLanguagesCommand.getLanguages().values().stream().map(
			singleCommand -> Pair.of(LanguageCode.create(singleCommand.getCode()), Country.create(singleCommand.getCountry()))).collect(Collectors.toSet());
		final Set<LanguageCode> alreadyExistingCodes = languageRepository.getExistingByCodes(codeAndCountryList.stream().map(Pair::getFirst).collect(Collectors.toSet()));
		final Set<LanguageCode> alreadyExistingCountries = languageRepository.getExistingByCountries(codeAndCountryList.stream().map(Pair::getSecond).collect(Collectors.toSet()));

		final Map<String, Set<Pair<LanguageCode, Country>>> groupedCommands = codeAndCountryList.stream().collect(Collectors.groupingBy(cc -> {
			if (alreadyExistingCodes.contains(cc.getFirst())) return EXISTING_CODES;
			if (alreadyExistingCountries.contains(cc.getFirst())) return EXISTING_COUNTRIES;
			return TO_SAVE;
		}, mapping(cc -> cc, toSet())));

		final List<Information> notInsertedLanguagesInformation = Stream.concat(
			maybe(groupedCommands.get(EXISTING_CODES)).stream().map(cc ->
				new Information(cc.getFirst().value(), format("Language with code %s already exists", cc.getFirst().value()))),
			maybe(groupedCommands.get(EXISTING_COUNTRIES)).stream().map(cc ->
				new Information(cc.getFirst().value(), format("Nationality %s already exists", cc.getSecond().value())))).collect(Collectors.toList());

		final Set<Language> languagesToSave = maybe(groupedCommands.get(TO_SAVE)).stream().map(cc ->
			LanguageFactory.create(cc.getFirst(), cc.getSecond(), CreationDate.create(timeDefinitions.now())))
			.collect(toSet());
		languageRepository.save(languagesToSave);

		return notInsertedLanguagesInformation;
	}

	private Set<Pair<LanguageCode, Country>> maybe(final Set<Pair<LanguageCode, Country>> pairs) {
		return Optional.ofNullable(pairs).orElse(Collections.emptySet());
	}

	@Override
	@TransactionalEventListener(value = LanguageEvent.class)
	protected void actionWasRaised(final @NonNull DomainEvent event) {
		superActionWasRaised(event);
	}
}
