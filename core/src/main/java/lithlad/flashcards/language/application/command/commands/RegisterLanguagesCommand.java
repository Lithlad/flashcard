package lithlad.flashcards.language.application.command.commands;

import java.util.Map;

public interface RegisterLanguagesCommand {
	Map<String, ? extends RegisterSingleLanguageCommand> getLanguages();
}
