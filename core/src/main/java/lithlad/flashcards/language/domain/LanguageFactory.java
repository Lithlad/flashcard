package lithlad.flashcards.language.domain;

import lithlad.flashcards.crossdomain.domain.LanguageCode;
import design.domain.CreationDate;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class LanguageFactory {

	public static Language create(@NonNull final LanguageCode code, @NonNull final Country country, @NonNull final CreationDate creationDate) {
		return new Language(code, country, creationDate);
	}
}
