package lithlad.flashcards.language.domain;

import java.util.regex.Pattern;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;

import design.domain.StringValueObject;
import lithlad.flashcards.language.domain.exceptions.CountryNameMustContainOnlyLettersException;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Embeddable
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = true)
public class Country extends StringValueObject {

	private static final Pattern ONLY_LETTERS_PATTERN = Pattern.compile("^[a-zA-Z]*$");

	public static Country create(@NotBlank final String country) {
		validateThatCountryNameContainsOnlyLetter(country);
		return new Country(country.toUpperCase());
	}

	private static void validateThatCountryNameContainsOnlyLetter(final String countryName) {
		if (!ONLY_LETTERS_PATTERN.matcher(countryName).matches())
			throw new CountryNameMustContainOnlyLettersException(countryName);
	}

	private Country(@NotBlank final String code) {
		super(code, 20);
	}

}
