package lithlad.flashcards.language.domain.exceptions;

import static java.lang.String.format;

import javax.validation.constraints.NotBlank;

import design.domain.exceptions.DomainRequirementsNotFulfilledException;

public class CountryNameMustContainOnlyLettersException extends DomainRequirementsNotFulfilledException {

	public CountryNameMustContainOnlyLettersException(final @NotBlank String countryName) {
		super(format("Country name %s contains forbidden signs", countryName), "country.name.doesnt.fulfill.pattern");
	}
}
