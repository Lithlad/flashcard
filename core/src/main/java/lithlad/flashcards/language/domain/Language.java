package lithlad.flashcards.language.domain;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Table;

import lithlad.flashcards.communication.language.event.LanguageWasCreatedEvent;
import lithlad.flashcards.crossdomain.domain.LanguageCode;
import design.domain.Aggregate;
import design.domain.CreationDate;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.ToString;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "lngg_language")
@NoArgsConstructor
public class Language extends Aggregate {

	@Embedded
	@AttributeOverride(name = "value", column = @Column(name = "code", unique = true))
	private LanguageCode code;

	@Embedded
	@AttributeOverride(name = "value", column = @Column(name = "name"))
	private Country country;

	Language(@NonNull final LanguageCode code, @NonNull final Country country, @NonNull final CreationDate creationDate) {
		super(creationDate);
		this.code = code;
		this.country = country;
		registerEvent(new LanguageWasCreatedEvent(code.value(), country.value(), creationDate.epoch(), getConcurrencyVersion()));
	}
}
