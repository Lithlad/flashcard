package lithlad.flashcards.language.domain;

import java.util.Set;

import javax.validation.constraints.NotEmpty;

import lithlad.flashcards.crossdomain.domain.LanguageCode;
import lombok.NonNull;

public interface LanguageRepository {

	boolean doesExist(@NonNull final LanguageCode code);

	void save(@NotEmpty final Set<Language> language);

	Set<LanguageCode> getExistingByCodes(@NotEmpty final Set<LanguageCode> codes);

	Set<LanguageCode> getExistingByCountries(@NotEmpty final Set<Country> countries);
}
