package lithlad.flashcards.language.infrastructure.repository;

import javax.validation.constraints.NotBlank;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import lithlad.flashcards.language.application.query.LanguageViewQueryService;
import lithlad.flashcards.crossdomain.domain.LanguageCode;

@Repository
public class JpaLanguageViewQueryService implements LanguageViewQueryService {

	private final LanguageRepositoryFacade languageRepositoryFacade;

	@Autowired
	public JpaLanguageViewQueryService(final LanguageRepositoryFacade languageRepositoryFacade) {
		this.languageRepositoryFacade = languageRepositoryFacade;
	}

	@Override
	public boolean doesExist(final @NotBlank String code) {
		final LanguageCode codeVO = LanguageCode.create(code);
		return languageRepositoryFacade.doesExist(codeVO);
	}
}
