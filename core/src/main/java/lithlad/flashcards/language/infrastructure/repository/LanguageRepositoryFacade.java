package lithlad.flashcards.language.infrastructure.repository;

import java.util.Set;

import javax.validation.constraints.NotEmpty;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import lithlad.flashcards.crossdomain.domain.LanguageCode;
import lithlad.flashcards.language.domain.Country;
import lithlad.flashcards.language.domain.Language;
import lithlad.flashcards.language.domain.LanguageRepository;
import lombok.NonNull;

@Repository
public class LanguageRepositoryFacade implements LanguageRepository {

	private final JpaLanguageRepository jpaLanguageRepository;

	@Autowired
	public LanguageRepositoryFacade(final JpaLanguageRepository jpaLanguageRepository) {
		this.jpaLanguageRepository = jpaLanguageRepository;
	}

	@Override
	public boolean doesExist(@NonNull final LanguageCode code) {
		return jpaLanguageRepository.existsByCode(code);
	}

	@Override
	public void save(final Set<Language> languages) {
		jpaLanguageRepository.saveAll(languages);
	}

	@Override
	public Set<LanguageCode> getExistingByCodes(@NotEmpty final Set<LanguageCode> codes) {
		return Set.copyOf(jpaLanguageRepository.findByCodeIn(codes));
	}

	@Override
	public Set<LanguageCode> getExistingByCountries(@NotEmpty final Set<Country> countries) {
		return Set.copyOf(jpaLanguageRepository.findByCountryIn(countries));
	}
}
