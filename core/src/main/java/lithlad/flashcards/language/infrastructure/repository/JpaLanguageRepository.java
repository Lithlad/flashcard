package lithlad.flashcards.language.infrastructure.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import lithlad.flashcards.crossdomain.domain.LanguageCode;
import lithlad.flashcards.language.domain.Country;
import lithlad.flashcards.language.domain.Language;
import lombok.NonNull;

//https://www.baeldung.com/hibernate-entitymanager

//https://www.baeldung.com/hibernate-save-persist-update-merge-saveorupdate

@Repository
public interface JpaLanguageRepository extends JpaRepository<Language, Long> {

	boolean existsByCode(@NonNull final LanguageCode code);

	List<LanguageCode> findByCodeIn(Set<LanguageCode> codes);

	List<LanguageCode> findByCountryIn(Set<Country> countries);
}
