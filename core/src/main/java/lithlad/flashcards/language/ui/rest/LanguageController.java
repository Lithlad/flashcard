package lithlad.flashcards.language.ui.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import design.application.command.commands.Information;
import lithlad.flashcards.language.application.command.LanguageApplicationService;
import lithlad.flashcards.language.ui.rest.form.RegisterLanguagesForm;
import lombok.NonNull;

@Api
@RestController
@RequestMapping("/api/language")
public class LanguageController {

	private final LanguageApplicationService languageApplicationService;

	@Autowired
	public LanguageController(final LanguageApplicationService languageApplicationService) {
		this.languageApplicationService = languageApplicationService;
	}

	@ApiOperation("Language registration")
	@PostMapping("/register")
	protected ResponseEntity<List<Information>> registerLearner(@ApiParam(required = true)
	                                         @NonNull @Validated @RequestBody final RegisterLanguagesForm form) {
		final List<Information> notAddedLanguagesMessage = languageApplicationService.registerLanguages(form);
		return ResponseEntity.ok(notAddedLanguagesMessage);
	}
}
