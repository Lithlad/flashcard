package lithlad.flashcards.language.ui.rest.form;

import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lithlad.flashcards.language.application.command.commands.RegisterSingleLanguageCommand;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@ApiModel
@EqualsAndHashCode
public class RegisterSingleLanguageForm implements RegisterSingleLanguageCommand {

	@NotBlank
	@ApiModelProperty(value = "code", required = true, example="PL")
	private final String code;

	@NotBlank
	@ApiModelProperty(value = "country", required = true, example="polish")
	private final String country;

	public RegisterSingleLanguageForm(final @NotBlank String code, final @NotBlank String country) {
		this.code = code;
		this.country = country;
	}
}
