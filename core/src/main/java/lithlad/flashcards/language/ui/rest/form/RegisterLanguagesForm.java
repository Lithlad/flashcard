package lithlad.flashcards.language.ui.rest.form;

import java.util.Map;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lithlad.flashcards.language.application.command.commands.RegisterLanguagesCommand;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@ApiModel
@EqualsAndHashCode
@NoArgsConstructor
public class RegisterLanguagesForm implements RegisterLanguagesCommand {

	// TODO fix example
	@ApiModelProperty(value = "languages",
		required = true,
		dataType = "Map[String,RegisterSingleLanguageForm]")
//		example="{\"PL\": {\"code\" : \"PL\", \"country\" : \"polish\" }")
//		example="{'PL': { 'code' : 'PL', 'country' : 'polish' }")
	private Map<String, RegisterSingleLanguageForm> languages;

	public RegisterLanguagesForm(final Map<String, RegisterSingleLanguageForm> getLanguages) {
		this.languages = getLanguages;
	}
}
