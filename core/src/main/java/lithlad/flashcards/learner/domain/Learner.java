package lithlad.flashcards.learner.domain;

import static java.util.Objects.isNull;

import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Table;

import lithlad.flashcards.communication.learner.event.DefaultLanguageWasSetEvent;
import lithlad.flashcards.communication.learner.event.LearnerWasCreatedEvent;
import lithlad.flashcards.communication.learner.event.MothertongueWasSetEvent;
import lithlad.flashcards.crossdomain.domain.LanguageCode;
import design.domain.CreationDate;
import design.user.domain.Email;
import design.user.domain.Login;
import design.user.domain.Pass;
import design.user.domain.User;
import lithlad.flashcards.learner.domain.dto.LearnerDTO;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.ToString;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "lrnr_learner")
@NoArgsConstructor
public class Learner extends User {

    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "mothertongue"))
    private LanguageCode mothertongue;

    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "defaultForeignLanguage"))
    private LanguageCode defaultForeignLanguage;

    @Column(name = "createdFlashcards")
    private int createdFlashcards;

    Learner(@NonNull final Login login, @NonNull final Pass pass, @NonNull final Email email, @NonNull final LanguageCode mothertongue,
            final LanguageCode defaultForeignLanguage, @NonNull final CreationDate creationDate) {
        super(creationDate, login, pass, email);
        this.mothertongue = mothertongue;
        this.defaultForeignLanguage = defaultForeignLanguage;
        registerEvent(new LearnerWasCreatedEvent(login.value(), email.value(), mothertongue.value(), isNull(defaultForeignLanguage) ? null : defaultForeignLanguage.value(), creationDate.epoch(), getConcurrencyVersion()));
    }

    public void setMotherTongue(final LanguageCode language) {
        this.mothertongue = language;
        registerEvent(new MothertongueWasSetEvent(login.value(), language.value(), getConcurrencyVersion()));
    }

    public void setDefaultForeignLanguage(final LanguageCode language) {
        this.defaultForeignLanguage = language;
        registerEvent(new DefaultLanguageWasSetEvent(login.value(), language.value(), getConcurrencyVersion()));
    }

    public LearnerDTO mapToDTO() {
        return new LearnerDTO(
            login.value(),
            email.value(),
            mothertongue.value(),
            defaultForeignLanguage.value(),
            locked.isLocked(),
            enabled.isEnabled(),
            creationDate.value(),
            getConcurrencyVersion(),
            createdFlashcards);
    }

    @Override
    protected List<String> getRoles() {
        return List.of("LEARNER");
    }

    // !! View methods !!
    public void incrementNumberOfCreatedFlashcards() {
        createdFlashcards += 1;
    }
}
