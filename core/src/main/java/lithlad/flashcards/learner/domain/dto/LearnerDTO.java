package lithlad.flashcards.learner.domain.dto;

import java.time.LocalDateTime;

import org.springframework.lang.Nullable;

import design.user.domain.dto.UserDTO;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;

/**
 * In proper CQRS DTO would be implemented in application.query.
 * But, to not to add getters to aggregate it is moved to domain.
 * DTO must be moved, when view table will be implemented
 */
@Getter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class LearnerDTO extends UserDTO {

	@NonNull
	private String motherTongue;

	@Nullable
	private String defaultForeignLanguage;

	private int createdFlashcards;

	public LearnerDTO(@NonNull final String login,
	                  @NonNull final String email,
	                  @NonNull final String motherTongue,
	                  @Nullable final String defaultForeignLanguage,
	                  final boolean locked,
	                  final boolean enabled,
	                  @NonNull final LocalDateTime creationDate,
	                  final long concurrencyVersion,
	                  final int createdFlashcards
	) {
		super(login, email, locked, enabled, creationDate, concurrencyVersion);
		this.motherTongue = motherTongue;
		this.defaultForeignLanguage = defaultForeignLanguage;
		this.createdFlashcards = createdFlashcards;
	}
}
