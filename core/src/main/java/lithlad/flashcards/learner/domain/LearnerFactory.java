package lithlad.flashcards.learner.domain;

import lithlad.flashcards.crossdomain.domain.LanguageCode;
import design.domain.CreationDate;
import design.user.domain.Email;
import design.user.domain.Login;
import design.user.domain.Pass;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class LearnerFactory {

    public static Learner create(@NonNull final Login login, @NonNull final Pass pass, @NonNull final Email email,
                                 @NonNull final LanguageCode mothertongue, @NonNull final CreationDate creationDate) {
        return new Learner(login, pass, email, mothertongue, null, creationDate);
    }

    public static Learner create(@NonNull final Login login, @NonNull final Pass pass, @NonNull final Email email,
                                 @NonNull final LanguageCode mothertongue, @NonNull final LanguageCode defaultForeignLanguage, @NonNull final CreationDate creationDate) {
        return new Learner(login, pass, email, mothertongue, defaultForeignLanguage, creationDate);
    }
}
