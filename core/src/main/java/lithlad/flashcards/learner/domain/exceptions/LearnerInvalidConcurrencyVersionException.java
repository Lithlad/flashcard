package lithlad.flashcards.learner.domain.exceptions;

import static java.lang.String.format;

import design.domain.exceptions.DomainInvalidConcurrencyVersionException;
import design.user.domain.Login;
import lombok.NonNull;

public class LearnerInvalidConcurrencyVersionException extends DomainInvalidConcurrencyVersionException {
	public LearnerInvalidConcurrencyVersionException(final @NonNull Login login) {
		super(format("Learner %s concurrency version is invalid", login.value()), "learner");
	}
}
