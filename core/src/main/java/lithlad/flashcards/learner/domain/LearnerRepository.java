package lithlad.flashcards.learner.domain;

import design.user.domain.Login;
import lombok.NonNull;

public interface LearnerRepository {
    Learner getByLoginAndConcurrencyVersion(@NonNull final Login login, final Integer concurrencyVersion);

    boolean doesExist(@NonNull final Login login);

    void save(@NonNull final Learner learner);
}
