package lithlad.flashcards.learner.infrastructure.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

import lithlad.flashcards.crossdomain.domain.LanguageCode;
import design.application.query.AggregateSpecificationCreator;
import design.application.query.AuthenticationInfo;
import design.user.domain.Email;
import design.user.domain.Enabled;
import design.user.domain.Locked;
import design.user.domain.Login;
import lithlad.flashcards.learner.application.query.LearnerViewQueryService;
import lithlad.flashcards.learner.domain.dto.LearnerDTO;
import lithlad.flashcards.learner.domain.Learner;
import lithlad.flashcards.learner.infrastructure.repository.exceptions.UnauthorizedLearnerDTORequestException;
import lithlad.flashcards.learner.ui.rest.LearnerCriteria;
import lombok.NonNull;

@Repository
public class JpaAggregateLearnerViewQueryService implements LearnerViewQueryService {

	private final JpaLearnerRepositoryFacade jpaLearnerRepositoryFacade;

	private final AuthenticationInfo authenticationInfo;

	@Autowired
	public JpaAggregateLearnerViewQueryService(final JpaLearnerRepositoryFacade jpaLearnerRepositoryFacade,
	                                           final AuthenticationInfo authenticationInfo) {
		this.jpaLearnerRepositoryFacade = jpaLearnerRepositoryFacade;
		this.authenticationInfo = authenticationInfo;
	}

	@Override
	public @NonNull LearnerDTO find(final String login) {
		final Login loginVO = Login.create(login);
		if (!loginVO.isEqual(Login.create(authenticationInfo.loggedUserLogin()))) {
			throw new UnauthorizedLearnerDTORequestException();
		}
		final Learner learner = jpaLearnerRepositoryFacade.getByLogin(loginVO);
		return learner.mapToDTO();
	}

	@Override
	public Page<LearnerDTO> findByCriteria(@Nullable final LearnerCriteria criteria, final @NonNull Pageable page) {
		Specification<Learner> specification = createSpecification(criteria);
		return jpaLearnerRepositoryFacade.findAll(specification, page).map(Learner::mapToDTO);
	}

	private Specification<Learner> createSpecification(@Nullable final LearnerCriteria criteria) {
		final AggregateSpecificationCreator specificationCreator = AggregateSpecificationCreator.INSTANCE;
		return (Root<Learner> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) -> {
			final List<Predicate> predicates = new ArrayList<>();
			specificationCreator.maybePredicate(
				root, criteriaBuilder, criteria.getLogin(), "login", Login::create).ifPresent(predicates::add);
			specificationCreator.maybePredicate(
				root, criteriaBuilder, criteria.getEmail(), "email", Email::create).ifPresent(predicates::add);
			specificationCreator.maybePredicate(
				root, criteriaBuilder, criteria.getEnabled(), "enabled", Enabled::create).ifPresent(predicates::add);
			specificationCreator.maybePredicate(
				root, criteriaBuilder, criteria.getLocked(), "locked", Locked::create).ifPresent(predicates::add);
			specificationCreator.maybePredicate(
				root, criteriaBuilder, criteria.getMothertongue(), "mothertongue", LanguageCode::create).ifPresent(predicates::add);
			specificationCreator.maybePredicate(
				root, criteriaBuilder, criteria.getDefaultForeignLanguage(), "defaultForeignLanguage", LanguageCode::create).ifPresent(predicates::add);
			return criteriaBuilder.and(predicates.toArray(Predicate[]::new));
		};
	}
}
