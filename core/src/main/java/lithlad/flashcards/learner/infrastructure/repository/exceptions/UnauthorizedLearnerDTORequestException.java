package lithlad.flashcards.learner.infrastructure.repository.exceptions;

import design.domain.exceptions.DomainUnauthorizedException;

public class UnauthorizedLearnerDTORequestException extends DomainUnauthorizedException {
	public UnauthorizedLearnerDTORequestException() {
		super("Currently logged user is not authorized to get requested data", "learner");
	}
}
