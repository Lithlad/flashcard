package lithlad.flashcards.learner.infrastructure.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Repository;

import design.user.domain.Login;
import lithlad.flashcards.learner.domain.Learner;
import lithlad.flashcards.learner.domain.LearnerRepository;
import lithlad.flashcards.learner.domain.exceptions.LearnerInvalidConcurrencyVersionException;
import lithlad.flashcards.learner.infrastructure.repository.exceptions.LearnerNotFoundException;
import lombok.NonNull;

@Repository
public class JpaLearnerRepositoryFacade implements LearnerRepository {

    private final JpaLearnerRepository jpaLearnerRepository;

    public JpaLearnerRepositoryFacade(final JpaLearnerRepository jpaLearnerRepository) {
        this.jpaLearnerRepository = jpaLearnerRepository;
    }

    @Override
    public Learner getByLoginAndConcurrencyVersion(@NonNull final Login login, final Integer concurrencyVersion) {
        final Optional<Learner> learner = Optional.of(getByLogin(login));
        return learner.filter(l -> l.getConcurrencyVersion() == concurrencyVersion).orElseThrow(() -> new LearnerInvalidConcurrencyVersionException(login));
    }

    @Override
    public boolean doesExist(@NonNull final Login login) {
        return jpaLearnerRepository.doesExist(login);
    }

    @Override
    public void save(@NonNull final Learner learner) {
        jpaLearnerRepository.save(learner);
    }

    protected Learner getByLogin(@NonNull final Login login) {
        return jpaLearnerRepository.findByLogin(login).orElseThrow(() -> new LearnerNotFoundException(login));
    }

    public Page<Learner> findAll(@NonNull final Specification<Learner> specification, @NonNull final Pageable page) {
        return jpaLearnerRepository.findAll(specification, page);
    }
}
