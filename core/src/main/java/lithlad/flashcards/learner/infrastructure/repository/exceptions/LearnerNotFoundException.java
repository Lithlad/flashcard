package lithlad.flashcards.learner.infrastructure.repository.exceptions;

import static java.lang.String.format;

import design.domain.exceptions.DomainNotFoundException;
import design.user.domain.Login;
import lombok.NonNull;

public class LearnerNotFoundException extends DomainNotFoundException {

    public LearnerNotFoundException(final @NonNull Login login) {
        super(format("Learner with login %s does not exist", login.value()), "learner");
    }
}
