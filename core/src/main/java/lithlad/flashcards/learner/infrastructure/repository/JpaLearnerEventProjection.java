package lithlad.flashcards.learner.infrastructure.repository;

import static org.springframework.transaction.annotation.Isolation.REPEATABLE_READ;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import design.user.domain.Login;
import lithlad.flashcards.learner.application.query.LearnerEventProjection;
import lithlad.flashcards.learner.domain.Learner;
import lombok.NonNull;

/**
 * As a final version class must operate on view object - not Aggregate
 */
@Repository
public class JpaLearnerEventProjection implements LearnerEventProjection {

	private final JpaLearnerRepositoryFacade jpaLearnerFacade;

	@Autowired
	public JpaLearnerEventProjection(final JpaLearnerRepositoryFacade jpaLearnerFacade) {
		this.jpaLearnerFacade = jpaLearnerFacade;
	}

	@Override
	@PreAuthorize("hasAuthority('LEARNER_VIEW')")
	@Transactional(isolation = REPEATABLE_READ)
	public void increaseNumberOfLearnerFlashcards(@NonNull final String ownerLogin) {
		final Learner learner = jpaLearnerFacade.getByLogin(Login.create(ownerLogin));
		learner.incrementNumberOfCreatedFlashcards();
		jpaLearnerFacade.save(learner);
	}
}
