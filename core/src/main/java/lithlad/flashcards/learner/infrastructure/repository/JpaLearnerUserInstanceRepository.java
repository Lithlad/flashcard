package lithlad.flashcards.learner.infrastructure.repository;

import static java.util.Optional.empty;
import static java.util.Optional.of;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import design.user.application.query.UserViewInstanceQueryService;
import design.user.domain.Login;
import design.user.domain.User;
import design.user.domain.UserInstanceRepository;
import lithlad.flashcards.learner.domain.Learner;
import lombok.NonNull;

@Repository
public class JpaLearnerUserInstanceRepository implements UserInstanceRepository, UserViewInstanceQueryService {

	private final JpaLearnerRepository jpaLearnerRepository;

	@Autowired
	public JpaLearnerUserInstanceRepository(final JpaLearnerRepository jpaLearnerRepository) {
		this.jpaLearnerRepository = jpaLearnerRepository;
	}

	@Override
	public Optional<? extends User> findByLogin(@NonNull final Login login) {
		return jpaLearnerRepository.findByLogin(login);
	}

	@Override
	public Optional<User> saveIfSuitable(@NonNull final User user) {
		if (!(user instanceof Learner))
			return empty();

		return of(jpaLearnerRepository.save((Learner) user));
	}

	@Override
	public boolean doesExist(@NonNull final Login login) {
		return jpaLearnerRepository.doesExist(login);
	}
}
