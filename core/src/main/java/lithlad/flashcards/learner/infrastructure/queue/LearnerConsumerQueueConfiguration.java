package lithlad.flashcards.learner.infrastructure.queue;

import java.util.Set;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;

import lithlad.flashcards.communication.flashcard.event.infrastructure.FlashcardInfrastructureConstants;
import lithlad.flashcards.communication.learner.event.LearnerEvent;
import lithlad.flashcards.communication.learner.event.infrastructure.LearnerInfrastructureConstants;
import design.infrastructure.queue.configuration.ConsumerQueueConfiguration;

@Configuration("LearnerConsumerQueueConfiguration")
public class LearnerConsumerQueueConfiguration extends ConsumerQueueConfiguration<LearnerEvent> {

	public static final String GROUP_ID = "learner-group";

	public static final String CONTAINER_FACTORY_ID = "learnerListenerContainerFactory";

	private final String bootstrapAddress;

	protected LearnerConsumerQueueConfiguration(@Value("${kafka.bootstrapAddress}") final String bootstrapAddress) {
		this.bootstrapAddress = bootstrapAddress;
	}

	@Bean("learnerConsumerFactory")
	@Override
	public ConsumerFactory<String, LearnerEvent> consumerFactoryBean() {
		return consumerFactory(bootstrapAddress);
	}

	@Bean("learnerListenerContainerFactory")
	@Override
	public ConcurrentKafkaListenerContainerFactory<String, LearnerEvent> listenerContainerFactoryBean(
		@Qualifier("learnerConsumerFactory") final ConsumerFactory<String, LearnerEvent> consumerFactory) {
		return listenerContainerFactory(consumerFactory);
	}

	@Override
	protected Set<String> trustedPackages() {
		return Set.of(
			LearnerInfrastructureConstants.PACKAGE,
			FlashcardInfrastructureConstants.PACKAGE);
	}

	@Override
	protected String groupId() {
		return GROUP_ID;
	}
}
