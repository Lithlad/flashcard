package lithlad.flashcards.learner.infrastructure.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import design.user.domain.Login;
import lithlad.flashcards.learner.domain.Learner;
import lombok.NonNull;

@Repository
public interface JpaLearnerRepository extends JpaRepository<Learner, Long>, JpaSpecificationExecutor<Learner> {

    Optional<Learner> findByLogin(@NonNull final Login login);

    @Query("SELECT CASE WHEN(COUNT(l) > 0) THEN true ELSE false END FROM Learner l WHERE l.login = :login")
    boolean doesExist(@NonNull final Login login);
}
