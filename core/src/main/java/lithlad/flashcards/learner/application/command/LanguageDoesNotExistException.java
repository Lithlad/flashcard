package lithlad.flashcards.learner.application.command;

import static java.lang.String.format;

import javax.validation.constraints.NotNull;

import design.domain.exceptions.DomainRequirementsNotFulfilledException;

public class LanguageDoesNotExistException extends DomainRequirementsNotFulfilledException {
	public LanguageDoesNotExistException(final @NotNull String language) {
		super(format("Language %s does not exist", language), "language.does.not.exist");
	}
}
