package lithlad.flashcards.learner.application.command.commands;

import javax.validation.constraints.NotEmpty;

public interface LearnerCommand {

	@NotEmpty
	String getLogin();
}
