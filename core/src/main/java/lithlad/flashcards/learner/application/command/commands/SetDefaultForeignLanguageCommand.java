package lithlad.flashcards.learner.application.command.commands;

import design.application.command.commands.ModificationCommand;

public interface SetDefaultForeignLanguageCommand extends ModificationCommand, LearnerCommand {

    String getDefaultForeignLanguage();
}
