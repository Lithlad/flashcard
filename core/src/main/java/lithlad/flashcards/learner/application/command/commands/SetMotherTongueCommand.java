package lithlad.flashcards.learner.application.command.commands;

import design.application.command.commands.ModificationCommand;

public interface SetMotherTongueCommand extends ModificationCommand, LearnerCommand {

    String getMotherTongue();
}
