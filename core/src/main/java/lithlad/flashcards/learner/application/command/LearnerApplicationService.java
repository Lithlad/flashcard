package lithlad.flashcards.learner.application.command;

import static java.util.Optional.ofNullable;
import static org.springframework.util.StringUtils.isEmpty;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionalEventListener;

import lithlad.flashcards.communication.learner.event.LearnerEvent;
import lithlad.flashcards.crossdomain.domain.LanguageCode;
import design.domain.CreationDate;
import design.domain.DomainEvent;
import design.infrastructure.TimeDefinitions;
import design.user.application.command.UserInstanceApplicationService;
import design.user.domain.Email;
import design.user.domain.Login;
import design.user.domain.Pass;
import design.user.domain.UserRepository;
import lithlad.flashcards.language.application.query.LanguageViewQueryService;
import lithlad.flashcards.learner.application.command.commands.RegisterLearnerCommand;
import lithlad.flashcards.learner.application.command.commands.SetDefaultForeignLanguageCommand;
import lithlad.flashcards.learner.application.command.commands.SetMotherTongueCommand;
import lithlad.flashcards.learner.domain.Learner;
import lithlad.flashcards.learner.domain.LearnerFactory;
import lithlad.flashcards.learner.domain.LearnerRepository;
import lithlad.flashcards.learner.infrastructure.repository.JpaLearnerRepositoryFacade;
import lombok.NonNull;

@Service
public class LearnerApplicationService extends UserInstanceApplicationService<Learner, RegisterLearnerCommand> {

    private final LearnerRepository jpaLearnerRepository;

    private final LanguageViewQueryService languageViewQueryService;

    private final TimeDefinitions timeDefinitions;

    @Autowired
    public LearnerApplicationService(final UserRepository userRepository,
                                     final JpaLearnerRepositoryFacade jpaLearnerRepository,
                                     final LanguageViewQueryService languageViewQueryService,
                                     final TimeDefinitions timeDefinitions) {
        super(userRepository);
        this.jpaLearnerRepository = jpaLearnerRepository;
        this.languageViewQueryService = languageViewQueryService;
        this.timeDefinitions = timeDefinitions;
    }

    @PreAuthorize("hasAuthority('LEARNER')")
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void setMotherTongue(final SetMotherTongueCommand command) {
        final Login login = Login.create(command.getLogin());
        final LanguageCode mothertongue = LanguageCode.create(command.getMotherTongue());

        final Learner learner = jpaLearnerRepository.getByLoginAndConcurrencyVersion(login, command.getConcurrencyVersion());

        learner.setMotherTongue(mothertongue);
        jpaLearnerRepository.save(learner);
    }

    @PreAuthorize("hasAuthority('LEARNER')")
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void setDefaultForeignLanguage(final SetDefaultForeignLanguageCommand command) {
        final Login login = Login.create(command.getLogin());
        final LanguageCode defaultForeignLanguage = LanguageCode.create(command.getDefaultForeignLanguage());

        final Learner learner = jpaLearnerRepository.getByLoginAndConcurrencyVersion(login, command.getConcurrencyVersion());

        learner.setDefaultForeignLanguage(defaultForeignLanguage);
        jpaLearnerRepository.save(learner);
    }

    @Override
    @TransactionalEventListener(value = LearnerEvent.class)
    protected void actionWasRaised(final @NonNull DomainEvent event) {
        superActionWasRaised(event);
    }

    @Override
    protected void saveUser(final Learner user) {
        jpaLearnerRepository.save(user);
    }

    @Override
    protected Learner createUserInstance(final RegisterLearnerCommand command) {
        final Login login = Login.create(command.getLogin());
        final Pass pass = Pass.create(command.getPass());
        final Email email = Email.create(command.getEmail());
        final LanguageCode mothertongue = LanguageCode.create(command.getMotherTongue());

        if (!languageViewQueryService.doesExist(mothertongue.value()))
            throw new LanguageDoesNotExistException(mothertongue.value());

        if (!isEmpty(command.getDefaultForeignLanguage()) && !languageViewQueryService.doesExist(command.getDefaultForeignLanguage()))
            throw new LanguageDoesNotExistException(command.getDefaultForeignLanguage());

        return ofNullable(command.getDefaultForeignLanguage()).map(
            defaultForeignLanguage ->
                LearnerFactory.create(login, pass, email, mothertongue, LanguageCode.create(defaultForeignLanguage), CreationDate.create(timeDefinitions.now())))
            .orElse(LearnerFactory.create(login, pass, email, mothertongue, CreationDate.create(timeDefinitions.now())));
    }

    @Override
    protected boolean isAppropriate(final @NonNull Login login) {
        return jpaLearnerRepository.doesExist(login);
    }
}
