package lithlad.flashcards.learner.application.command.commands;

import javax.validation.constraints.NotNull;

import org.springframework.lang.Nullable;

import design.user.application.command.commands.RegisterUserCommand;

public interface RegisterLearnerCommand extends LearnerCommand, RegisterUserCommand {

    @NotNull
    String getMotherTongue();

    @Nullable
    String getDefaultForeignLanguage();
}
