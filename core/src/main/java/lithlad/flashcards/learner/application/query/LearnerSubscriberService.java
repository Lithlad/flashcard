package lithlad.flashcards.learner.application.query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lithlad.flashcards.communication.flashcard.event.FlashcardWasCreatedEvent;

@Service
public class LearnerSubscriberService {

	private final LearnerEventProjection learnerEventProjection;

	@Autowired
	public LearnerSubscriberService(final LearnerEventProjection learnerEventProjection) {
		this.learnerEventProjection = learnerEventProjection;
	}

	public void flashcardWasCreated(final FlashcardWasCreatedEvent event) {
		learnerEventProjection.increaseNumberOfLearnerFlashcards(event.getOwnerLogin());
	}
}
