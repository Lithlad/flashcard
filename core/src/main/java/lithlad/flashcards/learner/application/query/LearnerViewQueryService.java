package lithlad.flashcards.learner.application.query;

import javax.validation.constraints.NotBlank;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.lang.Nullable;

import lithlad.flashcards.learner.domain.dto.LearnerDTO;
import lithlad.flashcards.learner.ui.rest.LearnerCriteria;
import lombok.NonNull;

public interface LearnerViewQueryService {

	@NonNull
	LearnerDTO find(@NotBlank final String login);

	Page<LearnerDTO> findByCriteria(@Nullable final LearnerCriteria criteria, @NonNull final Pageable pageable);
}
