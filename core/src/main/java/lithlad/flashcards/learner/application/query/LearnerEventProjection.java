package lithlad.flashcards.learner.application.query;

public interface LearnerEventProjection {
	void increaseNumberOfLearnerFlashcards(final String ownerLogin);
}
