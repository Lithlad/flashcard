package lithlad.flashcards.learner.ui.rest.form;

import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lithlad.flashcards.learner.application.command.commands.SetMotherTongueCommand;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@ApiModel
@EqualsAndHashCode
public class UpdateMotherTongueForm extends LearnerModificationForm implements SetMotherTongueCommand {

    @NotBlank
    @ApiModelProperty(value = "Learner mothertongue", required = true)
    private final String motherTongue;

    public UpdateMotherTongueForm(final @NotBlank String login, final @NotBlank String motherTongue, final int concurrencyVersion) {
        super(login, concurrencyVersion);
        this.motherTongue = motherTongue;
    }
}
