package lithlad.flashcards.learner.ui.rest.form;

import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import design.ui.rest.form.ModificationForm;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@ApiModel
@EqualsAndHashCode
public class LearnerModificationForm extends ModificationForm {

    @NotBlank
    @ApiModelProperty(value = "login", required = true)
    private final String login;

    public LearnerModificationForm(@NotBlank final String login, final int concurrencyVersion) {
        super(concurrencyVersion);
        this.login = login;
    }
}
