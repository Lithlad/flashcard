package lithlad.flashcards.learner.ui.rest.form;

import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lithlad.flashcards.learner.application.command.commands.SetDefaultForeignLanguageCommand;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@ApiModel
@EqualsAndHashCode
public class UpdateDefaultForeignLanguageForm extends LearnerModificationForm implements SetDefaultForeignLanguageCommand {

    @NotBlank
    @ApiModelProperty(value = "Default foreign language", required = true)
    private final String defaultForeignLanguage;

    public UpdateDefaultForeignLanguageForm(final @NotBlank String login, final @NotBlank String defaultForeignLanguage, final int concurrencyVersion) {
        super(login, concurrencyVersion);
        this.defaultForeignLanguage = defaultForeignLanguage;
    }
}
