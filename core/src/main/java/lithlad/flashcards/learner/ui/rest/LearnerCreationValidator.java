package lithlad.flashcards.learner.ui.rest;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import lithlad.flashcards.learner.ui.rest.form.RegisterLearnerForm;

/**
 * Validator is implemented only because of desire to demonstrate such possibility.
 * In application aim is to validate data during value objects creation and in application services.
 * It is why checks like login unique are not done
 */
@Component
public class LearnerCreationValidator extends LearnerValidator<RegisterLearnerForm> {

	@Override
	protected boolean isAppropriateForForm(final Object toValidate) {
		return toValidate instanceof RegisterLearnerForm;
	}

	@Override
	protected void validate(final RegisterLearnerForm form, final Errors errors) {
		validatePassword(form.getPass(), errors);
		validateEmail(form.getEmail(), errors);
		validateLanguage("mothertongue", form.getMotherTongue(), errors);
		if (form.getDefaultForeignLanguage() != null)
			validateLanguage("foreign language", form.getDefaultForeignLanguage(), errors);
	}
}
