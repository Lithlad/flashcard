package lithlad.flashcards.learner.ui.rest;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import lithlad.flashcards.learner.ui.rest.form.UpdateMotherTongueForm;

/**
 * Validator is implemented only because of desire to demonstrate such possibility.
 * In application aim is to validate data during value objects creation and in application services.
 */
@Component
public class LearnerUpdateMotherTongueValidator extends LearnerValidator<UpdateMotherTongueForm> {

	@Override
	protected boolean isAppropriateForForm(final Object toValidate) {
		return toValidate instanceof UpdateMotherTongueForm;
	}

	@Override
	protected void validate(final UpdateMotherTongueForm form, final Errors errors) {
		validateLanguage("mothertongue", form.getMotherTongue(), errors);
	}
}
