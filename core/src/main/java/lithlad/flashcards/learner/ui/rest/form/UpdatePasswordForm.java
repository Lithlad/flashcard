package lithlad.flashcards.learner.ui.rest.form;

import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@ApiModel
@EqualsAndHashCode
public class UpdatePasswordForm extends LearnerModificationForm {

	@NotBlank
	@ApiModelProperty(value = "pass", required = true, example = "pass")
	private final String password;

	public UpdatePasswordForm(@NotBlank final String login, @NotBlank final String password, final int concurrencyVersion) {
		super(login, concurrencyVersion);
		this.password = password;
	}
}
