package lithlad.flashcards.learner.ui.rest.form;

import javax.validation.constraints.NotBlank;

import org.springframework.lang.Nullable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import design.user.ui.form.RegisterUserForm;
import lithlad.flashcards.learner.application.command.commands.RegisterLearnerCommand;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString(callSuper = true)
@ApiModel
@EqualsAndHashCode(callSuper = true)
public class RegisterLearnerForm extends RegisterUserForm implements RegisterLearnerCommand {

    @NotBlank
    @ApiModelProperty(value = "Learner mothertongue", required = true, example = "PL")
    private final String motherTongue;

    @Nullable
    @ApiModelProperty(value = "Default foreign language, which will be learnt by learner", example = "EN", required = false)
    private final String defaultForeignLanguage;

    public RegisterLearnerForm(final @NotBlank String login, final @NotBlank String pass, final @NotBlank String email,
                               final @NotBlank String motherTongue, @Nullable final String defaultForeignLanguage) {
        super(login, pass, email);
        this.motherTongue = motherTongue;
        this.defaultForeignLanguage = defaultForeignLanguage;
    }
}
