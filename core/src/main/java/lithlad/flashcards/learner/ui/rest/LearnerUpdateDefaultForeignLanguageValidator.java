package lithlad.flashcards.learner.ui.rest;

import lithlad.flashcards.learner.ui.rest.form.UpdateDefaultForeignLanguageForm;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

/**
 * Validator is implemented only because of desire to demonstrate such possibility.
 * In application aim is to validate data during value objects creation and in application services.
 */
@Component
public class LearnerUpdateDefaultForeignLanguageValidator extends LearnerValidator<UpdateDefaultForeignLanguageForm> {
    @Override
    protected boolean isAppropriateForForm(final Object toValidate) {
        return toValidate instanceof UpdateDefaultForeignLanguageForm;
    }

    @Override
    protected void validate(final UpdateDefaultForeignLanguageForm form, final Errors errors) {
        validateLanguage("foreign language", form.getDefaultForeignLanguage(), errors);
    }
}
