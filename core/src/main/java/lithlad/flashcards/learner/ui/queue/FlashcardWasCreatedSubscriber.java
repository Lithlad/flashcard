package lithlad.flashcards.learner.ui.queue;

import static lithlad.flashcards.learner.infrastructure.queue.LearnerConsumerQueueConfiguration.CONTAINER_FACTORY_ID;
import static lithlad.flashcards.learner.infrastructure.queue.LearnerConsumerQueueConfiguration.GROUP_ID;

import java.util.Collection;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Controller;

import lithlad.flashcards.communication.flashcard.FlashcardEventsIdentifires;
import lithlad.flashcards.communication.flashcard.event.FlashcardWasCreatedEvent;
import design.application.command.DomainEventObserver;
import lithlad.flashcards.learner.application.query.LearnerSubscriberService;

@Controller
@DependsOn("LearnerConsumerQueueConfiguration")
public class FlashcardWasCreatedSubscriber extends DomainEventObserver<FlashcardWasCreatedEvent> {

	private static final Logger logger = LogManager.getLogger(FlashcardWasCreatedSubscriber.class);
	private static final String NAME = "Learner - flashcard was created subscriber";
	private static final Set<String> AUTHORITIES = Set.of("LEARNER_VIEW");

	private final LearnerSubscriberService learnerSubscriberService;

	@Autowired
	public FlashcardWasCreatedSubscriber(final LearnerSubscriberService learnerSubscriberService) {
		this.learnerSubscriberService = learnerSubscriberService;
	}

	@Override
	@KafkaListener(topics = FlashcardEventsIdentifires.FLASHCARD_WAS_CREATED, groupId = GROUP_ID, containerFactory = CONTAINER_FACTORY_ID)
	public void notify(final FlashcardWasCreatedEvent event) {
		logger.info(prepareEventLogMessage("Learner", event.eventIdentifier(), event.getCreationDate()));
		authenticate();
		learnerSubscriberService.flashcardWasCreated(event);
	}

	@Override
	protected Collection<String> getAuthorities() {
		return AUTHORITIES;
	}

	@Override
	protected String subscriberName() {
		return NAME;
	}
}
