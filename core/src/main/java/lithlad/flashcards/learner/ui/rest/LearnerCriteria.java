package lithlad.flashcards.learner.ui.rest;

import design.infrastructure.repository.criteria.BooleanSearchCriterium;
import design.infrastructure.repository.criteria.SetStringSearchCriterium;
import design.infrastructure.repository.criteria.StringSearchCriterium;
import lombok.Data;

// Requires getter and setter - no contructor allowed
@Data
public class LearnerCriteria {

	private StringSearchCriterium login;

	private StringSearchCriterium email;

	private BooleanSearchCriterium enabled;

	private BooleanSearchCriterium locked;

	private SetStringSearchCriterium mothertongue;

	private StringSearchCriterium defaultForeignLanguage;
}
