package lithlad.flashcards.learner.ui.rest;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NOT_IMPLEMENTED;
import static org.springframework.http.HttpStatus.OK;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lithlad.flashcards.learner.application.command.LearnerApplicationService;
import lithlad.flashcards.learner.application.query.LearnerViewQueryService;
import lithlad.flashcards.learner.domain.dto.LearnerDTO;
import lithlad.flashcards.learner.ui.rest.form.RegisterLearnerForm;
import lithlad.flashcards.learner.ui.rest.form.UpdateDefaultForeignLanguageForm;
import lithlad.flashcards.learner.ui.rest.form.UpdateMotherTongueForm;
import lithlad.flashcards.learner.ui.rest.form.UpdatePasswordForm;
import lombok.NonNull;
import springfox.documentation.annotations.ApiIgnore;

@Api
@RestController
@RequestMapping("/api/learner")
public class LearnerController {

    private final LearnerApplicationService applicationService;

    private final LearnerViewQueryService queryService;

    private final LearnerUpdateMotherTongueValidator learnerUpdateMotherTongueValidator;

    private final LearnerUpdateDefaultForeignLanguageValidator learnerUpdateDefaultForeignLanguageValidator;

    private final LearnerCreationValidator learnerCreationValidator;

    @Autowired
    public LearnerController(final LearnerApplicationService applicationService,
                             final LearnerViewQueryService queryService,
                             final LearnerUpdateMotherTongueValidator learnerUpdateMotherTongueValidator,
                             final LearnerUpdateDefaultForeignLanguageValidator learnerUpdateDefaultForeignLanguageValidator,
                             final LearnerCreationValidator learnerCreationValidator
    ) {
        this.applicationService = applicationService;
        this.queryService = queryService;
        this.learnerUpdateMotherTongueValidator = learnerUpdateMotherTongueValidator;
        this.learnerCreationValidator = learnerCreationValidator;
        this.learnerUpdateDefaultForeignLanguageValidator = learnerUpdateDefaultForeignLanguageValidator;
    }

    /**
     * Binder is implemented only because of desire to demonstrate such possibility.
     * In application aim is to validate data during value objects creation and in application services.
     */
    @InitBinder
    public void initBinder(final WebDataBinder binder) {
//        binder.addValidators(learnerCreationValidator, learnerUpdateMotherTongueValidator, learnerUpdateDefaultForeignLanguageValidator);
    }

//    http://dolszewski.com/spring/how-to-bind-requestparam-to-object/
//    https://www.baeldung.com/rest-api-search-language-spring-data-specifications
    @ApiOperation("Get learner")
    @GetMapping("/list")
    protected ResponseEntity<Page<LearnerDTO>> list(final LearnerCriteria criteria, @ApiIgnore Pageable pageable) {
        final Page<LearnerDTO> page = queryService.findByCriteria(criteria, pageable);
        return ResponseEntity.ok(page);
    }

    @ApiOperation("Get learner")
    @GetMapping("{login}")
    protected ResponseEntity<LearnerDTO> getLearner(@Valid @PathVariable @ApiParam(required = true) @NonNull String login) {
        @NonNull final LearnerDTO body = queryService.find(login);
        return ResponseEntity.ok(body);
    }

    @ApiOperation("Learner registration")
    @PostMapping("/register")
    protected ResponseEntity registerLearner(@Valid @ApiParam(required = true)
                                                 @NonNull @Validated @RequestBody RegisterLearnerForm form) {
        applicationService.register(form);
        return new ResponseEntity(CREATED);
    }

    @ApiOperation("Learner motherTongue update")
    @PostMapping("/updateMotherTongue")
    protected ResponseEntity updateMotherTongue(@Valid @ApiParam(required = true)
                                                @NonNull @Validated @RequestBody UpdateMotherTongueForm form) {
        applicationService.setMotherTongue(form);
        return new ResponseEntity(OK);
    }

    @ApiOperation("Learner default foreign language update")
    @PostMapping("/updateDefaultForeignLanguage")
    protected ResponseEntity updateDefaultForeignLanguage(@Valid @ApiParam(required = true)
                                                @NonNull @Validated @RequestBody UpdateDefaultForeignLanguageForm form) {
        applicationService.setDefaultForeignLanguage(form);
        return new ResponseEntity(OK);
    }

    // TODO implement when SSL will be configured
    @ApiOperation("Update password - to implement")
    @PostMapping("/updatePassword")
    protected ResponseEntity updatePassword(@Valid @ApiParam(required = true)
                                                @NonNull @Validated @RequestBody UpdatePasswordForm form) {
        return new ResponseEntity(NOT_IMPLEMENTED);
    }
}
