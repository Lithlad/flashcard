package lithlad.flashcards.learner.ui.rest;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import lithlad.flashcards.learner.application.command.commands.LearnerCommand;

public abstract class LearnerValidator<F extends LearnerCommand> implements Validator {

	private static final int MINIMAL_PASSWORD_LENGTH = 6;

	@Override
	public boolean supports(final Class<?> aClass) {
		return LearnerCommand.class.isAssignableFrom(aClass);
	}

	@Override
	public void validate(final Object toValidate, final Errors errors) {
		if (!isAppropriateForForm(toValidate))
			return;
		F form = (F) toValidate;
		validate(form, errors);
	}

	protected abstract boolean isAppropriateForForm(final Object toValidate);

	protected abstract void validate(final F form, final Errors errors);

	protected void validateEmail(final String email, final Errors errors) {
		if (!EmailValidator.getInstance().isValid(email))
			errors.rejectValue("email", "learner.email.is.invalid");
	}

	protected void validatePassword(final String pass, final Errors errors) {
		if (pass.length() < MINIMAL_PASSWORD_LENGTH)
			errors.rejectValue("pass", "learner.password.must.contain.at.least.6.signs");
	}

	protected void validateLanguage(final String fieldName, final String language, final Errors errors) {
		if (!language.matches("[A-Z]{2}"))
			errors.rejectValue(fieldName, "language.is.invalid");
	}
}
