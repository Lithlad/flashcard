package lithlad.flashcards.learner.ui.queue;

import static lithlad.flashcards.learner.infrastructure.queue.LearnerConsumerQueueConfiguration.CONTAINER_FACTORY_ID;
import static lithlad.flashcards.learner.infrastructure.queue.LearnerConsumerQueueConfiguration.GROUP_ID;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import org.springframework.context.annotation.DependsOn;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Controller;

import lithlad.flashcards.communication.learner.LearnerEventsIdentifires;
import lithlad.flashcards.communication.learner.event.LearnerWasCreatedEvent;
import design.application.command.DomainEventObserver;

@Controller
@DependsOn("LearnerConsumerQueueConfiguration")
public class LearnerWasCreatedSubscriber extends DomainEventObserver<LearnerWasCreatedEvent> {

	private static final Set<String> AUTHORITIES = Collections.emptySet();
	private static final String NAME = "Learner - learner was created subscriber";

	@Override
	@KafkaListener(topics = LearnerEventsIdentifires.LEARNER_WAS_CREATED, groupId = GROUP_ID, containerFactory = CONTAINER_FACTORY_ID)
	public void notify(final LearnerWasCreatedEvent event) {
		System. out.println(event.toString());
	}

	@Override
	protected Collection<String> getAuthorities() {
		return AUTHORITIES;
	}

	@Override
	protected String subscriberName() {
		return NAME;
	}
}
