package lithlad.flashcards.learner.integration;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.TestPropertySource;

import configurations.ServiceDomainTestConfiguration;
import design.user.infrastructure.UserRepositoryImplementation;
import lithlad.flashcards.communication.learner.event.LearnerWasCreatedEvent;
import design.infrastructure.TimeDefinitions;
import design.user.domain.Login;
import design.user.domain.UserRepository;
import lithlad.flashcards.language.application.query.LanguageViewQueryService;
import lithlad.flashcards.learner.application.command.LearnerApplicationService;
import lithlad.flashcards.learner.application.command.commands.RegisterLearnerCommand;
import lithlad.flashcards.learner.infrastructure.repository.JpaLearnerRepository;
import lithlad.flashcards.learner.infrastructure.repository.JpaLearnerRepositoryFacade;

@Tag("it")
@DataJpaTest(includeFilters = {
    @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = JpaLearnerRepository.class),
    @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = LearnerApplicationService.class),
    @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = UserRepository.class),
    @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = JpaLearnerRepositoryFacade.class),
    @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = TimeDefinitions.class)
})
@Import({ ServiceDomainTestConfiguration.class, LearnerApplicationService.class,
    UserRepositoryImplementation.class, TimeDefinitions.class })
@TestPropertySource(locations="classpath:application-test.properties")
public class LearnerServiceDomainRepositoryIT {

    private final LearnerApplicationService learnerApplicationService;

    private final JpaLearnerRepository learnerRepository;

    @MockBean
    private LanguageViewQueryService languageViewQueryService;

    @Autowired
    public LearnerServiceDomainRepositoryIT(final LearnerApplicationService learnerApplicationService, final JpaLearnerRepository learnerRepository) {
        this.learnerApplicationService = learnerApplicationService;
        this.learnerRepository = learnerRepository;
    }

    @AfterEach
    void cleanDB() {
        learnerRepository.deleteAll();
    }

    @Test
    void shouldBroadcastEventAfterCommandAndSaveAggregate() {
        // given
        final RegisterLearnerCommand registerCommand = mock(RegisterLearnerCommand.class);
        final String planeLogin = "login";
        when(registerCommand.getLogin()).thenReturn(planeLogin);
        when(registerCommand.getPass()).thenReturn("pass");
        when(registerCommand.getEmail()).thenReturn("login@mail.com");
        when(registerCommand.getMotherTongue()).thenReturn("PL");
        when(registerCommand.getDefaultForeignLanguage()).thenReturn("EN");
        assertFalse(learnerRepository.doesExist(Login.create(planeLogin)));
        when(languageViewQueryService.doesExist("PL")).thenReturn(true);
        when(languageViewQueryService.doesExist("EN")).thenReturn(true);

        // when
        learnerApplicationService.register(registerCommand);

        // then
        assertTrue(learnerRepository.doesExist(Login.create(planeLogin)));
        verify(ServiceDomainTestConfiguration.EVENT_BROADCASTER).broadcast(any(LearnerWasCreatedEvent.class));
    }
}
