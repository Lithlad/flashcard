package lithlad.flashcards.learner.domain;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

import design.domain.CreationDate;

@Tag("unit")
class CreationDateTest {

    @Test
    void shouldReturnProperEpoch() {
        // given
        final CreationDate creationDate = CreationDate.create(LocalDateTime.of(2020, 5, 9, 7, 8, 5, 0));

        // when
        final long epoch = creationDate.epoch();

        // then
        assertThat(epoch).isEqualTo(1589008085L);
    }
}
