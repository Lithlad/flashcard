package lithlad.flashcards.learner.domain;

import static design.utils.TestUtils.assertEventIsRegistered;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lithlad.flashcards.communication.learner.event.DefaultLanguageWasSetEvent;
import lithlad.flashcards.communication.learner.event.LearnerWasCreatedEvent;
import lithlad.flashcards.communication.learner.event.MothertongueWasSetEvent;
import lithlad.flashcards.crossdomain.domain.LanguageCode;
import design.domain.CreationDate;
import design.user.domain.Email;
import design.user.domain.Login;
import design.user.domain.Pass;
import lithlad.flashcards.learner.domain.dto.LearnerDTO;

@Tag("unit")
class LearnerTest {

    @Test
    void shouldCreateNewInstance() {
        // given
        final Login login = Login.create(UUID.randomUUID().toString());
        final Pass pass = Pass.create(UUID.randomUUID().toString());
        final Email email = Email.create("email@mail.com");
        final LanguageCode motherTongue = LanguageCode.create("PL");
        final LanguageCode defaultForeignLanguage = LanguageCode.create("EN");
        final CreationDate creationDate = CreationDate.create(LocalDateTime.now());

        // when
        final Learner learner = new Learner(login, pass, email, motherTongue, defaultForeignLanguage, creationDate);

        // then
        assertThat(learner)
            .extracting("login", "pass", "email", "mothertongue", "defaultForeignLanguage", "creationDate")
            .containsExactly(login, pass, email, motherTongue, defaultForeignLanguage, creationDate);
        assertEventIsRegistered(learner,
                new LearnerWasCreatedEvent(login.value(), email.value(), motherTongue.value(), defaultForeignLanguage.value(), creationDate.epoch(), -1));
    }

    @Test
    void shouldSetMothertongue() {
        // given
        final Login login = Login.create(UUID.randomUUID().toString());
        final Pass pass = Pass.create(UUID.randomUUID().toString());
        final Email email = Email.create("email@mail.com");
        final LanguageCode motherTongue = LanguageCode.create("PL");
        final LanguageCode defaultForeignLanguage = LanguageCode.create("EN");
        final CreationDate creationDate = CreationDate.create(LocalDateTime.now());
        final Learner learner = new Learner(login, pass, email, motherTongue, defaultForeignLanguage, creationDate);

        final LanguageCode newMothertongue = LanguageCode.create("DE");

        // when
        learner.setMotherTongue(newMothertongue);

        // then
        assertThat(learner).extracting("mothertongue").isEqualTo(newMothertongue);
        assertEventIsRegistered(learner, new MothertongueWasSetEvent(login.value(), newMothertongue.value(), -1));
    }

    @Test
    void shouldSetDefaultForeignLanguage() {
        // given
        final Login login = Login.create(UUID.randomUUID().toString());
        final Pass pass = Pass.create(UUID.randomUUID().toString());
        final Email email = Email.create("email@mail.com");
        final LanguageCode motherTongue = LanguageCode.create("PL");
        final LanguageCode defaultForeignLanguage = LanguageCode.create("EN");
        final CreationDate creationDate = CreationDate.create(LocalDateTime.now());
        final Learner learner = new Learner(login, pass, email, motherTongue, defaultForeignLanguage, creationDate);

        final LanguageCode newDefaultForeignLanguage = LanguageCode.create("DE");

        // when
        learner.setDefaultForeignLanguage(newDefaultForeignLanguage);

        // then
        assertThat(learner).extracting("defaultForeignLanguage").isEqualTo(newDefaultForeignLanguage);
        assertEventIsRegistered(learner, new DefaultLanguageWasSetEvent(login.value(), newDefaultForeignLanguage.value(), -1));
    }

    @Test
    void shouldMapToUserDetails() {
        // given
        final Login login = Login.create(UUID.randomUUID().toString());
        final Pass pass = Pass.create(UUID.randomUUID().toString());
        final Email email = Email.create("email@mail.com");
        final LanguageCode motherTongue = LanguageCode.create("PL");
        final LanguageCode defaultForeignLanguage = LanguageCode.create("EN");
        final CreationDate creationDate = CreationDate.create(LocalDateTime.now());
        final Learner learner = new Learner(login, pass, email, motherTongue, defaultForeignLanguage, creationDate);

        // when
        final UserDetails userDetails = learner.mapToUserDetails();

        // then
        final List<SimpleGrantedAuthority> authorities = (List<SimpleGrantedAuthority>) userDetails.getAuthorities();
        assertThat(authorities).containsExactly(new SimpleGrantedAuthority("LEARNER"));
        assertEquals(login.value(), userDetails.getUsername());
        assertEquals(pass.value(), userDetails.getPassword());
        assertTrue(userDetails.isAccountNonExpired());
        assertTrue(userDetails.isAccountNonLocked());
        assertTrue(userDetails.isAccountNonExpired());
        assertTrue(userDetails.isEnabled());
    }

    @Test
    void shouldMapToDTO() {
        // given
        final Login login = Login.create(UUID.randomUUID().toString());
        final Pass pass = Pass.create(UUID.randomUUID().toString());
        final Email email = Email.create("email@mail.com");
        final LanguageCode motherTongue = LanguageCode.create("PL");
        final LanguageCode defaultForeignLanguage = LanguageCode.create("EN");
        final CreationDate creationDate = CreationDate.create(LocalDateTime.now());
        final Learner learner = new Learner(login, pass, email, motherTongue, defaultForeignLanguage, creationDate);

        // when
        final LearnerDTO dto = learner.mapToDTO();

        // then
        assertThat(dto.getLogin()).isEqualTo(login.value());
        assertThat(dto.getEmail()).isEqualTo(email.value());
        assertThat(dto.getMotherTongue()).isEqualTo(motherTongue.value());
        assertThat(dto.getDefaultForeignLanguage()).isEqualTo(defaultForeignLanguage.value());
        assertThat(dto.getCreationDate()).isEqualTo(creationDate.value());
        assertThat(dto.getConcurrencyVersion()).isEqualTo(0);
        assertThat(dto.isEnabled()).isEqualTo(true);
        assertThat(dto.isLocked()).isEqualTo(false);
    }

    // View method
    @Test
    void shouldIncrementNumberOfCreatedFlashcards() {
        // given
        final Login login = Login.create(UUID.randomUUID().toString());
        final Pass pass = Pass.create(UUID.randomUUID().toString());
        final Email email = Email.create("email@mail.com");
        final LanguageCode motherTongue = LanguageCode.create("PL");
        final LanguageCode defaultForeignLanguage = LanguageCode.create("EN");
        final CreationDate creationDate = CreationDate.create(LocalDateTime.now());
        final Learner learner = new Learner(login, pass, email, motherTongue, defaultForeignLanguage, creationDate);
        final int createdFlashcardsBeforeIncrementation = learner.mapToDTO().getCreatedFlashcards();

        // when
        learner.incrementNumberOfCreatedFlashcards();

        // then
        final int createdFlashcardsAfterIncrementation = learner.mapToDTO().getCreatedFlashcards();
        assertThat(createdFlashcardsAfterIncrementation).isEqualTo(createdFlashcardsBeforeIncrementation + 1);
    }
}
