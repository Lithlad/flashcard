package lithlad.flashcards.learner.domain;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import design.utils.TestUtils;
import lithlad.flashcards.crossdomain.domain.LanguageCode;
import design.domain.CreationDate;
import design.user.domain.Email;
import design.user.domain.Login;
import design.user.domain.Pass;

@Tag("unit")
class LearnerFactoryTest {

    @Test
    void shouldCreateLearner() {
        // given
        final Login login = mock(Login.class);
        final Pass pass = mock(Pass.class);
        final Email email = mock(Email.class);
        final LanguageCode defaultLanguage = mock(LanguageCode.class);
        final CreationDate creationDate = mock(CreationDate.class);
        when(login.value()).thenReturn(TestUtils.randomString());
        when(email.value()).thenReturn(TestUtils.randomString());
        when(defaultLanguage.value()).thenReturn(TestUtils.randomString());

        // when
        final Learner learner = LearnerFactory.create(login, pass, email, defaultLanguage, creationDate);

        // then
        assertNotNull(learner);
    }
}
