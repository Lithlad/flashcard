package lithlad.flashcards.learner.infrastructure.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.UUID;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import lithlad.flashcards.crossdomain.domain.LanguageCode;
import design.application.query.AuthenticationInfo;
import design.domain.CreationDate;
import design.infrastructure.exception.NotImplementedException;
import design.user.domain.Email;
import design.user.domain.Login;
import design.user.domain.Pass;
import lithlad.flashcards.learner.domain.dto.LearnerDTO;
import lithlad.flashcards.learner.domain.Learner;
import lithlad.flashcards.learner.domain.LearnerFactory;
import lithlad.flashcards.learner.infrastructure.repository.exceptions.UnauthorizedLearnerDTORequestException;

@Tag("unit")
class JpaAggregateLearnerViewQueryServiceTest {

	private JpaLearnerRepositoryFacade jpaLearnerRepositoryFacade = mock(JpaLearnerRepositoryFacade.class);

	private AuthenticationInfo authenticationInfo = mock(AuthenticationInfo.class);

	private final JpaAggregateLearnerViewQueryService jpaAggregateLearnerViewQueryService = new JpaAggregateLearnerViewQueryService(jpaLearnerRepositoryFacade, authenticationInfo);

	@Test
	void shouldReturnDTOForLearnerIfUserRequestsForHimself() {
		// given
		final String login = UUID.randomUUID().toString();
		final String pass = UUID.randomUUID().toString();
		final String email = "learner@lithlad.eu";
		final String motherTongue = "EN";
		final String defaultForeignLanguage = "PL";
		final LocalDateTime creationDate = LocalDateTime.now();
		final Learner learner = sampleLearner(login, pass, email, motherTongue, defaultForeignLanguage, creationDate);

		when(jpaLearnerRepositoryFacade.getByLogin(Login.create(login))).thenReturn(learner);
		when(authenticationInfo.loggedUserLogin()).thenReturn(login);

		// when
		final LearnerDTO learnerDTO = jpaAggregateLearnerViewQueryService.find(login);

		// then
		assertThat(learnerDTO).isEqualTo(expectedLearnerDTO(login, email, motherTongue, defaultForeignLanguage, false, true, creationDate, 0));
	}

	// TODO implement roles
	@Test
	@Disabled
	void shouldReturnDTOForLearnerIfUserIsAdmin() {

	}

	// TODO implement roles
	@Test
	void shouldThrowExceptionIfRequestsNotForHimselfAndIsNotAnAdmin() {
		// given
		final String login = UUID.randomUUID().toString();
		final String pass = UUID.randomUUID().toString();
		final String email = "learner@lithlad.eu";
		final String motherTongue = "EN";
		final String defaultForeignLanguage = "PL";
		final LocalDateTime creationDate = LocalDateTime.now();
		final Learner learner = sampleLearner(login, pass, email, motherTongue, defaultForeignLanguage, creationDate);

		when(jpaLearnerRepositoryFacade.getByLogin(Login.create(login))).thenReturn(learner);
		when(authenticationInfo.loggedUserLogin()).thenReturn("otherLogin");

		// when
		final UnauthorizedLearnerDTORequestException exception = assertThrows(UnauthorizedLearnerDTORequestException.class, () -> jpaAggregateLearnerViewQueryService.find(login));

		// then
		assertThat(exception).hasMessage("Currently logged user is not authorized to get requested data");
	}

	@Disabled
	@Test
	void shouldCreateSpecification() {
		throw new NotImplementedException();
	}

	private Learner sampleLearner(final String login, final String pass, final String email, final String motherTongue, final String defaultForeignLanguage, final LocalDateTime creationDate) {
		return LearnerFactory.create(
			Login.create(login),
			Pass.create(pass),
			Email.create(email),
			LanguageCode.create(motherTongue),
			LanguageCode.create(defaultForeignLanguage),
			CreationDate.create(creationDate));
	}

	private LearnerDTO expectedLearnerDTO(final String login,
	                                      final String email,
	                                      final String motherTongue,
	                                      final String defaultForeignLanguage,
	                                      final boolean locked,
	                                      final boolean enabled,
	                                      final LocalDateTime creationDate,
	                                      long concurrencyVersion) {
		return new LearnerDTO(login, email, motherTongue, defaultForeignLanguage, locked, enabled, creationDate, concurrencyVersion, 0);
	}
}
