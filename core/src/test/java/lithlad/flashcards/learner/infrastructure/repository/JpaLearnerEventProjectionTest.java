package lithlad.flashcards.learner.infrastructure.repository;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import design.user.domain.Login;
import design.utils.TestUtils;
import lithlad.flashcards.learner.domain.Learner;

@Tag("unit")
class JpaLearnerEventProjectionTest {

	private final JpaLearnerRepositoryFacade jpaLearnerFacade = mock(JpaLearnerRepositoryFacade.class);

	private final JpaLearnerEventProjection jpaLearnerEventProjection = new JpaLearnerEventProjection(jpaLearnerFacade);

	@Test
	void shouldIncreaseNumberOfLearnerFlashcards() {
		// given
		final String login = TestUtils.randomString();
		final Learner learner = mock(Learner.class);
		when(jpaLearnerFacade.getByLogin(Login.create(login))).thenReturn(learner);

		// when
		jpaLearnerEventProjection.increaseNumberOfLearnerFlashcards(login);

		// then
		verify(jpaLearnerFacade).getByLogin(Login.create(login));
		verify(learner).incrementNumberOfCreatedFlashcards();
		verify(jpaLearnerFacade).save(learner);
	}
}
