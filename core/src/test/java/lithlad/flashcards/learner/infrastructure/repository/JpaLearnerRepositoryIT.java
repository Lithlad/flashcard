package lithlad.flashcards.learner.infrastructure.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDateTime;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.TestPropertySource;

import configurations.DataJpaTestConfiguration;
import lithlad.flashcards.crossdomain.domain.LanguageCode;
import design.domain.CreationDate;
import design.user.domain.Email;
import design.user.domain.Login;
import design.user.domain.Pass;
import lithlad.flashcards.learner.domain.Learner;
import lithlad.flashcards.learner.domain.LearnerFactory;

@Tag("it")
// something with DataJpaTest. Fix
@DataJpaTest
@Import(DataJpaTestConfiguration.class)
@TestPropertySource(locations="classpath:application-test.properties")
class JpaLearnerRepositoryIT {

    private final JpaLearnerRepository jpaLearnerRepository;
    private final Login login = Login.create("username");
    private final Pass pass = Pass.create("pass");
    private final Email email = Email.create("learner@lithlad.eu");
    private final LanguageCode defaultLanguage = LanguageCode.create("PL");
    private final CreationDate creationDate = CreationDate.create(LocalDateTime.now());
    private final Learner learner = LearnerFactory.create(login, pass, email, defaultLanguage, creationDate);

    @Autowired
    JpaLearnerRepositoryIT(final JpaLearnerRepository jpaLearnerRepository) {
        this.jpaLearnerRepository = jpaLearnerRepository;
    }

    @BeforeEach
    public void fillDb() {
        jpaLearnerRepository.save(learner);
    }

    @Test
    void shouldFindByLogin() {
        // given

        // when
        Optional<Learner> maybeLearner = jpaLearnerRepository.findByLogin(login);

        // then
        assertThat(maybeLearner).hasValue(learner);
    }

    @Test
    void shouldReturnTrueIfLearnerExists() {
        // given
        Login existingLogin = this.login;

        // when
        boolean doesExist = jpaLearnerRepository.doesExist(existingLogin);

        // then
        assertTrue(doesExist);
    }

    @Test
    void shouldReturnFalseIfLearnerDoesntExist() {
        // given
        Login nonExistingLogin = Login.create("nonExisting");

        // when
        boolean doesExist = jpaLearnerRepository.doesExist(nonExistingLogin);

        // then
        assertFalse(doesExist);

    }
}
