package lithlad.flashcards.learner.infrastructure.repository;

import static java.lang.String.format;
import static java.util.Optional.empty;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import lithlad.flashcards.learner.domain.Learner;
import design.user.domain.Login;
import lithlad.flashcards.learner.domain.exceptions.LearnerInvalidConcurrencyVersionException;
import lithlad.flashcards.learner.infrastructure.repository.exceptions.LearnerNotFoundException;

@Tag("unit")
class JpaLearnerRepositoryFacadeTest {

    private final JpaLearnerRepository jpaLearnerRepository = mock(JpaLearnerRepository.class);

    private final JpaLearnerRepositoryFacade jpaLearnerRepositoryFacade = new JpaLearnerRepositoryFacade(jpaLearnerRepository);

    @Test
    void shouldGetByLoginAndConcurrencyVersion() {
        // given
        final Login login = mock(Login.class);
        final Integer concurrencyVersion = 4;
        final Learner learner = mock(Learner.class);
        when(learner.getConcurrencyVersion()).thenReturn(concurrencyVersion);
        when(jpaLearnerRepository.findByLogin(login)).thenReturn(Optional.of(learner));

        // when
        final Learner obtainedLearner = jpaLearnerRepositoryFacade.getByLoginAndConcurrencyVersion(login, concurrencyVersion);

        // then
        verify(jpaLearnerRepository).findByLogin(login);
        assertNotNull(obtainedLearner);
    }

    @Test
    void shouldGetByLogin() {
        // given
        final Login login = mock(Login.class);
        when(jpaLearnerRepository.findByLogin(login)).thenReturn(Optional.of(mock(Learner.class)));

        // when
        final Learner learner = jpaLearnerRepositoryFacade.getByLogin(login);

        // then
        verify(jpaLearnerRepository).findByLogin(login);
        assertNotNull(learner);
    }

    @Test
    void shouldThrowExceptionIfLoginIsNotFound() {
        // given
        final Login login = Login.create(UUID.randomUUID().toString());
        final Integer concurrencyVersion = 4;
        when(jpaLearnerRepository.findByLogin(login)).thenReturn(empty());

        // when
        final LearnerNotFoundException exception = assertThrows(LearnerNotFoundException.class,
                () -> jpaLearnerRepositoryFacade.getByLoginAndConcurrencyVersion(login, concurrencyVersion));

        // then
        assertThat(exception).hasMessage(format("Learner with login %s does not exist", login.value()));
    }

    @Test
    void shouldThrowExceptionIfConcurrencyVersionIsInvalid() {
        // given
        final Login login = mock(Login.class);
        final Integer concurrencyVersion = 4;
        final Learner learner = mock(Learner.class);
        when(jpaLearnerRepository.findByLogin(login)).thenReturn(Optional.of(mock(Learner.class)));
        when(learner.getConcurrencyVersion()).thenReturn(concurrencyVersion + 1);

        // when
        final LearnerInvalidConcurrencyVersionException exception = assertThrows(LearnerInvalidConcurrencyVersionException.class,
            () -> jpaLearnerRepositoryFacade.getByLoginAndConcurrencyVersion(login, concurrencyVersion));

        // then
        assertThat(exception).hasMessage(format("Learner %s concurrency version is invalid", login.value()));
    }

    @Test
    void shouldReturnTrueIfLearnerExists() {
        // given
        final Login login = Login.create(UUID.randomUUID().toString());
        when(jpaLearnerRepository.doesExist(login)).thenReturn(true);

        // when
        boolean doesExist = jpaLearnerRepositoryFacade.doesExist(login);

        // then
        assertTrue(doesExist);
    }

    @Test
    void shouldReturnFalseIfLearnerDoesntExist() {
        // given
        final Login login = Login.create(UUID.randomUUID().toString());
        when(jpaLearnerRepository.doesExist(login)).thenReturn(false);

        // when
        boolean doesExist = jpaLearnerRepositoryFacade.doesExist(login);

        // then
        assertFalse(doesExist);
    }

    @Test
    void shouldSaveLearner() {
        // given
        final Learner learner = mock(Learner.class);

        // when
        jpaLearnerRepositoryFacade.save(learner);

        // then
        verify(jpaLearnerRepository).save(learner);
    }
}
