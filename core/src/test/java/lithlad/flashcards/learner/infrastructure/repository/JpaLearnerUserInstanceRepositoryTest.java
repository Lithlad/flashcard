package lithlad.flashcards.learner.infrastructure.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import design.user.domain.Login;
import design.user.domain.User;
import design.user.domain.dto.UserDTO;
import lithlad.flashcards.learner.domain.Learner;
import lombok.NonNull;

@Tag("unit")
class JpaLearnerUserInstanceRepositoryTest {

	private final JpaLearnerRepository jpaLearnerRepository = mock(JpaLearnerRepository.class);

	private final JpaLearnerUserInstanceRepository jpaLearnerUserInstanceRepository = new JpaLearnerUserInstanceRepository(jpaLearnerRepository);

	@Test
	void shouldFindByLogin() {
		// given
		final @NonNull Login login = Login.create("login");

		final Optional<Learner> learner = Optional.of(mock(Learner.class));
		when(jpaLearnerRepository.findByLogin(login)).thenReturn(learner);

		// when
		final Optional<? extends User> byLogin = jpaLearnerUserInstanceRepository.findByLogin(login);

		// then
		verify(jpaLearnerRepository).findByLogin(login);
		assertThat(byLogin).isEqualTo(learner);
	}

	@Test
	void shouldSaveIfSuitable() {
		// given
		final Learner learner = mock(Learner.class);
		when(jpaLearnerRepository.save(learner)).thenReturn(learner);

		// when
		final Optional<User> maybeUser = jpaLearnerUserInstanceRepository.saveIfSuitable(learner);

		// then
		verify(jpaLearnerRepository).save(learner);
		assertThat(maybeUser).isNotEmpty();
	}

	@Test
	void shouldNotSaveIfNotSuitable() {
		// given
		final User user = mock(ConcreteUser.class);

		// when
		final Optional<User> maybeUser = jpaLearnerUserInstanceRepository.saveIfSuitable(user);

		// then
		verify(jpaLearnerRepository, never()).save(any(Learner.class));
		assertThat(maybeUser).isEmpty();
	}

	static class ConcreteUser extends User {

		@Override
		public UserDTO mapToDTO() {
			return mock(UserDTO.class);
		}

		@Override
		protected List<String> getRoles() {
			return null;
		}
	}

}
