package lithlad.flashcards.learner.infrastructure;

import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.TestPropertySource;

import configurations.DataJpaTestConfiguration;
import design.admin.infrastructure.JpaAdminUserInstanceRepository;
import design.infrastructure.AuthenticationInfoDefaultImplementation;
import design.user.domain.Login;
import design.user.infrastructure.JpaUserDetailsRepository;
import lithlad.flashcards.learner.domain.Learner;
import lithlad.flashcards.learner.infrastructure.repository.JpaAggregateLearnerViewQueryService;
import lithlad.flashcards.learner.infrastructure.repository.JpaLearnerUserInstanceRepository;
import lombok.NonNull;

@Tag("it")
// Additional info is required, because JpaUserDetailsRepository is not JPA class
@DataJpaTest(includeFilters = @ComponentScan.Filter(type = FilterType.ANNOTATION, classes = Repository.class))
@Import({DataJpaTestConfiguration.class, JpaAggregateLearnerViewQueryService.class,
	AuthenticationInfoDefaultImplementation.class, JpaUserDetailsRepository.class})
@TestPropertySource(locations="classpath:application-test.properties")
public class JpaUserDetailsRepositoryIT {

	@MockBean
	private JpaLearnerUserInstanceRepository learnerRepository;

	@MockBean
	private JpaAdminUserInstanceRepository adminRepository;

	@Autowired
	private JpaUserDetailsRepository jpaUserDetailsRepository;

	@Test
	void shouldGetLearner() {
		// given
		final @NonNull Login login = Login.create("learner");
		when(adminRepository.findByLogin(login)).thenReturn(empty());
		final Learner learner = mock(Learner.class);
		final UserDetails userDetails = mockUserDetails(login);
		when(learner.mapToUserDetails()).thenReturn(userDetails);
		doReturn(of(learner)).when(learnerRepository).findByLogin(login);

		// when
		final UserDetails userDetailsResult = jpaUserDetailsRepository.loadByUsername(login.value());

		//then
		assertThat(userDetailsResult.getUsername()).isEqualTo(login.value());
	}

	private UserDetails mockUserDetails(final Login login) {
		final UserDetails userDetails = mock(UserDetails.class);
		when(userDetails.getUsername()).thenReturn(login.value());
		return userDetails;
	}
}
