package lithlad.flashcards.learner.application.command;

import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.UUID;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import lithlad.flashcards.crossdomain.domain.LanguageCode;
import design.domain.CreationDate;
import design.infrastructure.TimeDefinitions;
import design.user.domain.Email;
import design.user.domain.Login;
import design.user.domain.Pass;
import design.user.domain.UserRepository;
import design.user.domain.exceptions.UserAlreadyExistException;
import lithlad.flashcards.language.application.query.LanguageViewQueryService;
import lithlad.flashcards.learner.application.command.commands.RegisterLearnerCommand;
import lithlad.flashcards.learner.application.command.commands.SetDefaultForeignLanguageCommand;
import lithlad.flashcards.learner.application.command.commands.SetMotherTongueCommand;
import lithlad.flashcards.learner.domain.Learner;
import lithlad.flashcards.learner.domain.LearnerFactory;
import lithlad.flashcards.learner.infrastructure.repository.JpaLearnerRepositoryFacade;
import lombok.NonNull;

@Tag("unit")
class LearnerApplicationServiceTest {

    private final JpaLearnerRepositoryFacade jpaLearnerRepository = mock(JpaLearnerRepositoryFacade.class);

    private final TimeDefinitions timeDefinitions = mock(TimeDefinitions.class);

    private LanguageViewQueryService languageViewQueryService = mock(LanguageViewQueryService.class);

    private UserRepository userRepository = mock(UserRepository.class);

    private final LearnerApplicationService learnerApplicationService = new LearnerApplicationService(userRepository, jpaLearnerRepository, languageViewQueryService, timeDefinitions);

    @Test
    void shouldRegisterNewLearner() {
        // given
        final RegisterLearnerCommand registerLearnerCommand = mock(RegisterLearnerCommand.class);
        final String planeLogin = UUID.randomUUID().toString();
        final String planePass = UUID.randomUUID().toString();
        final String planeEmail = "email@mail.com";
        final String planeMotherTongue = "PL";
        final LocalDateTime planeCreationTime = LocalDateTime.now();
        when(registerLearnerCommand.getLogin()).thenReturn(planeLogin);
        when(registerLearnerCommand.getPass()).thenReturn(planePass);
        when(registerLearnerCommand.getEmail()).thenReturn(planeEmail);
        when(registerLearnerCommand.getMotherTongue()).thenReturn(planeMotherTongue);
        when(timeDefinitions.now()).thenReturn(planeCreationTime);
        when(userRepository.doesExist(Login.create(planeLogin))).thenReturn(false);
        when(languageViewQueryService.doesExist(planeMotherTongue)).thenReturn(true);

        // when
        learnerApplicationService.register(registerLearnerCommand);

        // then
        verify(userRepository).doesExist(Login.create(planeLogin));
        verify(jpaLearnerRepository).save(LearnerFactory.create(Login.create(planeLogin), Pass.create(planePass), Email.create(planeEmail),
                LanguageCode.create(planeMotherTongue), CreationDate.create(planeCreationTime)));
    }

    @Test
    void shouldThrowExceptionIfLearnerWithLoginAlreadyExists() {
        // given
        final RegisterLearnerCommand registerLearnerCommand = mock(RegisterLearnerCommand.class);
        final String planeLogin = UUID.randomUUID().toString();
        final String planePass = UUID.randomUUID().toString();
        final String planeEmail = "email@mail.com";
        final String planeMothertongue = "PL";
        when(registerLearnerCommand.getLogin()).thenReturn(planeLogin);
        when(registerLearnerCommand.getPass()).thenReturn(planePass);
        when(registerLearnerCommand.getEmail()).thenReturn(planeEmail);
        when(registerLearnerCommand.getMotherTongue()).thenReturn(planeMothertongue);
        when(userRepository.doesExist(Login.create(planeLogin))).thenReturn(true);
        when(languageViewQueryService.doesExist(planeMothertongue)).thenReturn(true);

        // when
        final UserAlreadyExistException exception =
                assertThrows(UserAlreadyExistException.class, () -> learnerApplicationService.register(registerLearnerCommand));

        // then
        assertThat(exception).hasMessage(format("User with login %s already exists", planeLogin));
    }

    @Test
    void shouldSetLearnerMotherTongue() {
        // given
        final SetMotherTongueCommand setMothertongueCommand = mock(SetMotherTongueCommand.class);
        final String planeLogin = UUID.randomUUID().toString();
        final String planeMothertongue = "PL";
        final int planeConcurrencyVersion = 3;
        final Learner learner = mock(Learner.class);
        when(setMothertongueCommand.getLogin()).thenReturn(planeLogin);
        when(setMothertongueCommand.getMotherTongue()).thenReturn(planeMothertongue);
        when(setMothertongueCommand.getConcurrencyVersion()).thenReturn(planeConcurrencyVersion);
        when(jpaLearnerRepository.getByLoginAndConcurrencyVersion(Login.create(planeLogin), planeConcurrencyVersion)).thenReturn(learner);
        when(languageViewQueryService.doesExist(planeMothertongue)).thenReturn(true);

        // when
        learnerApplicationService.setMotherTongue(setMothertongueCommand);

        // then
        verify(jpaLearnerRepository).getByLoginAndConcurrencyVersion(Login.create(planeLogin), planeConcurrencyVersion);
        verify(learner).setMotherTongue(LanguageCode.create(planeMothertongue));
        verify(jpaLearnerRepository).save(learner);
    }

    @Test
    void shouldSetLearnerDefaultForeignLanguage() {
        // given
        final SetDefaultForeignLanguageCommand setDefaultForeignLanguageCommand = mock(SetDefaultForeignLanguageCommand.class);
        final String planeLogin = UUID.randomUUID().toString();
        final String planeDefaultForeignLanguage = "PL";
        final int planeConcurrencyVersion = 3;
        final Learner learner = mock(Learner.class);
        when(setDefaultForeignLanguageCommand.getLogin()).thenReturn(planeLogin);
        when(setDefaultForeignLanguageCommand.getDefaultForeignLanguage()).thenReturn(planeDefaultForeignLanguage);
        when(setDefaultForeignLanguageCommand.getConcurrencyVersion()).thenReturn(planeConcurrencyVersion);
        when(jpaLearnerRepository.getByLoginAndConcurrencyVersion(Login.create(planeLogin), planeConcurrencyVersion)).thenReturn(learner);
        when(languageViewQueryService.doesExist(planeDefaultForeignLanguage)).thenReturn(true);

        // when
        learnerApplicationService.setDefaultForeignLanguage(setDefaultForeignLanguageCommand);

        // then
        verify(jpaLearnerRepository).getByLoginAndConcurrencyVersion(Login.create(planeLogin), planeConcurrencyVersion);
        verify(learner).setDefaultForeignLanguage(LanguageCode.create(planeDefaultForeignLanguage));
        verify(jpaLearnerRepository).save(learner);
    }

    @Test
    void shouldThrowExceptionIfMotherTongueDoesntExist() {
        // given
        final RegisterLearnerCommand registerLearnerCommand = mock(RegisterLearnerCommand.class);
        final String planeLogin = UUID.randomUUID().toString();
        final String planePass = UUID.randomUUID().toString();
        final String planeEmail = "email@mail.com";
        final String planeMotherTongue = "PL";
        final LocalDateTime planeCreationTime = LocalDateTime.now();
        when(registerLearnerCommand.getLogin()).thenReturn(planeLogin);
        when(registerLearnerCommand.getPass()).thenReturn(planePass);
        when(registerLearnerCommand.getEmail()).thenReturn(planeEmail);
        when(registerLearnerCommand.getMotherTongue()).thenReturn(planeMotherTongue);
        when(timeDefinitions.now()).thenReturn(planeCreationTime);
        when(userRepository.doesExist(Login.create(planeLogin))).thenReturn(false);
        when(languageViewQueryService.doesExist(planeMotherTongue)).thenReturn(false);

        // when
        final LanguageDoesNotExistException exception = assertThrows(LanguageDoesNotExistException.class,
            () -> learnerApplicationService.register(registerLearnerCommand));

        // then
        verify(userRepository).doesExist(Login.create(planeLogin));
        assertThat(exception).hasMessage(format("Language %s does not exist", planeMotherTongue));
    }

    @Test
    void shouldThrowExceptionIfDefaultForeignLanguageDoesntExist() {
        // given
        final RegisterLearnerCommand registerLearnerCommand = mock(RegisterLearnerCommand.class);
        final String planeLogin = UUID.randomUUID().toString();
        final String planePass = UUID.randomUUID().toString();
        final String planeEmail = "email@mail.com";
        final String planeMotherTongue = "PL";
        final String defaultForeignLanguage = "EN";
        final LocalDateTime planeCreationTime = LocalDateTime.now();
        when(registerLearnerCommand.getLogin()).thenReturn(planeLogin);
        when(registerLearnerCommand.getPass()).thenReturn(planePass);
        when(registerLearnerCommand.getEmail()).thenReturn(planeEmail);
        when(registerLearnerCommand.getMotherTongue()).thenReturn(planeMotherTongue);
        when(registerLearnerCommand.getDefaultForeignLanguage()).thenReturn(defaultForeignLanguage);
        when(timeDefinitions.now()).thenReturn(planeCreationTime);
        when(userRepository.doesExist(Login.create(planeLogin))).thenReturn(false);
        when(languageViewQueryService.doesExist(planeMotherTongue)).thenReturn(true);
        when(languageViewQueryService.doesExist(defaultForeignLanguage)).thenReturn(false);

        // when
        final LanguageDoesNotExistException exception = assertThrows(LanguageDoesNotExistException.class,
            () -> learnerApplicationService.register(registerLearnerCommand));

        // then
        verify(userRepository).doesExist(Login.create(planeLogin));
        assertThat(exception).hasMessage(format("Language %s does not exist", defaultForeignLanguage));
    }

    @Test
    void shouldReturnTrueIfUserIsAppropriateForUser() {
        // given
        final @NonNull Login login = Login.create("login1");
        when(jpaLearnerRepository.doesExist(login)).thenReturn(true);

        // when
        final boolean isAppropriate = learnerApplicationService.isAppropriate(login);

        // then
        assertThat(isAppropriate).isTrue();
    }

    @Test
    void shouldReturnFalseIfUserIsAppropriateForUser() {
        // given
        final @NonNull Login login = Login.create("login1");
        when(jpaLearnerRepository.doesExist(login)).thenReturn(false);

        // when
        final boolean isAppropriate = learnerApplicationService.isAppropriate(login);

        // then
        assertThat(isAppropriate).isFalse();
    }
}
