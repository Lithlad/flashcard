package lithlad.flashcards.learner.application.query;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import lithlad.flashcards.communication.flashcard.event.FlashcardWasCreatedEvent;

@Tag("unit")
class LearnerSubscriberServiceTest {

	private LearnerEventProjection learnerEventProjection  = mock(LearnerEventProjection.class);

	private final LearnerSubscriberService learnerSubscriberService = new LearnerSubscriberService(learnerEventProjection);

	@Test
	void shouldIncreaseNumberOfLearnersFlashcards() {
		// given
		final String ownerLogin = "ownerLogin";
		final FlashcardWasCreatedEvent event = mock(FlashcardWasCreatedEvent.class);
		when(event.getOwnerLogin()).thenReturn(ownerLogin);

		// when
		learnerSubscriberService.flashcardWasCreated(event);

		// then
		learnerEventProjection.increaseNumberOfLearnerFlashcards(ownerLogin);
	}

}
