package lithlad.flashcards.learner.ui.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NOT_IMPLEMENTED;
import static org.springframework.http.HttpStatus.OK;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.validation.constraints.NotBlank;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MvcResult;

import design.security.infrastructure.SecurityConfiguration;
import design.security.infrastructure.UserService;
import design.utils.AbstractControllerTest;
import lithlad.flashcards.learner.application.command.LearnerApplicationService;
import lithlad.flashcards.learner.application.query.LearnerViewQueryService;
import lithlad.flashcards.learner.domain.dto.LearnerDTO;
import lithlad.flashcards.learner.ui.rest.form.RegisterLearnerForm;
import lithlad.flashcards.learner.ui.rest.form.UpdateDefaultForeignLanguageForm;
import lithlad.flashcards.learner.ui.rest.form.UpdateMotherTongueForm;
import lithlad.flashcards.learner.ui.rest.form.UpdatePasswordForm;

@Tag("it")
@Import({
	LearnerUpdateMotherTongueValidator.class,
	LearnerUpdateDefaultForeignLanguageValidator.class,
	LearnerCreationValidator.class,
	SecurityConfiguration.class
})
@WebMvcTest(LearnerController.class)
@TestPropertySource(locations="classpath:application-test.properties")
class LearnerControllerIT extends AbstractControllerTest {

	private static final String CONTROLLER_PREFIX = "/api/learner";

	@MockBean
	private LearnerApplicationService learnerApplicationService;

	@MockBean
	private LearnerViewQueryService learnerViewQueryService;

	@MockBean
	private UserService userService;

	@WithMockUser(username="learner1")
	@Test
	void shouldGetLearner() throws Exception {
		// given
		final String login = "learner1";
		final String email = "learner@lithlad.eu";
		final String motherTongue = "EN";
		final String defaultForeignLanguage = "PL";
		final LocalDateTime creationDate = LocalDateTime.now();
		final LearnerDTO learnerDTO = sampleLearnerDTO(login, email, motherTongue, defaultForeignLanguage, false, true, creationDate, 0);
		when(learnerViewQueryService.find(login)).thenReturn(learnerDTO);

		// when
		final MvcResult result = performLoggedGet(CONTROLLER_PREFIX + "/" + login);

		// then
		assertThat(result.getResponse().getStatus()).isEqualTo(OK.value());
		verify(learnerViewQueryService).find(login);
	}

	@Test
	void shouldRegisterLearner() throws Exception {
		// given
		@NotBlank final String login = UUID.randomUUID().toString();
		@NotBlank final String pass = UUID.randomUUID().toString();
		@NotBlank final String email = "example@lithlad.eu";
		@NotBlank final String motherTongue = "PL";
		final String defaultForeignLanguage = "EN";
		final RegisterLearnerForm registerLearnerForm = new RegisterLearnerForm(login, pass, email, motherTongue, defaultForeignLanguage);

		// when
		final MvcResult result = performPost(CONTROLLER_PREFIX + "/register", registerLearnerForm);

		// then
		verify(learnerApplicationService).register(registerLearnerForm);
		assertThat(result.getResponse().getStatus()).isEqualTo(CREATED.value());
	}

	@WithMockUser(username="learner1")
	@Test
	void shouldUpdateMotherTongue() throws Exception {
		// given
		@NotBlank final String login = UUID.randomUUID().toString();
		@NotBlank final String motherTongue = "PL";
		final int concurrencyVersion = 3;
		final UpdateMotherTongueForm updateMotherTongue = new UpdateMotherTongueForm(login, motherTongue, concurrencyVersion);

		// when
		final MvcResult result = performLoggedPost(CONTROLLER_PREFIX + "/updateMotherTongue", updateMotherTongue);

		// then
		verify(learnerApplicationService).setMotherTongue(updateMotherTongue);
		assertThat(result.getResponse().getStatus()).isEqualTo(OK.value());
	}

	@WithMockUser(username="learner1")
	@Test
	void shouldUpdateDefaultForeignLanguage() throws Exception {
		// given
		@NotBlank final String login = UUID.randomUUID().toString();
		final String defaultForeignLanguage = "EN";
		final int concurrencyVersion = 4;
		final UpdateDefaultForeignLanguageForm updateDefaultForeignLanguage = new UpdateDefaultForeignLanguageForm(login, defaultForeignLanguage, concurrencyVersion);

		// when
		final MvcResult result = performLoggedPost(CONTROLLER_PREFIX + "/updateDefaultForeignLanguage", updateDefaultForeignLanguage);

		// then
		verify(learnerApplicationService).setDefaultForeignLanguage(updateDefaultForeignLanguage);
		assertThat(result.getResponse().getStatus()).isEqualTo(OK.value());
	}

	@WithMockUser(username="learner1")
	@Test
	void shouldUpdatePassword() throws Exception {
		// given
		@NotBlank final String login = UUID.randomUUID().toString();
		final String password = UUID.randomUUID().toString();
		final int concurrencyVersion = 7;
		final UpdatePasswordForm updatePassword = new UpdatePasswordForm(login, password, concurrencyVersion);

		// when
		final MvcResult result = performLoggedPost(CONTROLLER_PREFIX + "/updatePassword", updatePassword);

		// then
		assertThat(result.getResponse().getStatus()).isEqualTo(NOT_IMPLEMENTED.value());
	}

	private LearnerDTO sampleLearnerDTO(final String login,
	                                      final String email,
	                                      final String motherTongue,
	                                      final String defaultForeignLanguage,
	                                      final boolean locked,
	                                      final boolean enabled,
	                                      final LocalDateTime creationDate,
	                                      long concurrencyVersion) {
		return new LearnerDTO(login, email, motherTongue, defaultForeignLanguage, locked, enabled, creationDate, concurrencyVersion, 0);
	}
}
