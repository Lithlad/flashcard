package lithlad.flashcards.learner.ui.queue;

import static design.utils.TestUtils.randomLowarcaseAlphabeticallString;
import static design.utils.TestUtils.randomString;
import static design.utils.TestUtils.randomUppercaseAlphabeticallString;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;

import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.kafka.test.EmbeddedKafkaBroker;

import design.domain.EventBroadcaster;
import design.infrastructure.AuthenticationInfoDefaultImplementation;
import design.infrastructure.TimeDefinitions;
import design.infrastructure.queue.DefaultQueueEventBroadcaster;
import design.infrastructure.queue.configuration.ProducerQueueConfiguration;
import design.user.application.query.UserViewQueryService;
import design.utils.QueueConsumerTest;
import lithlad.flashcards.communication.flashcard.event.FlashcardWasCreatedEvent;
import lithlad.flashcards.crossdomain.domain.LanguageCode;
import design.domain.CreationDate;
import design.user.domain.Login;
import lithlad.flashcards.flashcard.domain.Visibility;
import lithlad.flashcards.flashcard.domain.Word;
import lithlad.flashcards.learner.application.command.LearnerApplicationService;
import lithlad.flashcards.learner.application.query.LearnerSubscriberService;
import lithlad.flashcards.learner.infrastructure.queue.LearnerConsumerQueueConfiguration;
import lombok.NonNull;

//https://blog.mimacom.com/testing-apache-kafka-with-spring-boot/
//https://stackoverflow.com/questions/48753051/simple-embedded-kafka-test-example-with-spring-boot
//https://www.geekyhacker.com/2020/10/03/test-spring-kafka-consumer-and-producer-with-embeddedkafka/

@Import({TimeDefinitions.class,
	UserViewQueryService.class,
	DefaultQueueEventBroadcaster.class,
	AuthenticationInfoDefaultImplementation.class})
class FlashcardWasCreatedSubscriberIT extends QueueConsumerTest<FlashcardWasCreatedEvent> {

	@MockBean
	private LearnerApplicationService learnerApplicationService;

	@MockBean
	private LearnerSubscriberService learnerSubscriberService;

	@Autowired
	private FlashcardWasCreatedSubscriber flashcardWasCreatedSubscriber;

	@Autowired
	private EmbeddedKafkaBroker embeddedKafkaBroker;

	@Test
	void shouldGetMessageFromQueueAndCallService() {
	    // given
		final FlashcardWasCreatedEvent flashcardWasCreatedEvent = createDomainEvent();

		// when
		sendEvent(flashcardWasCreatedEvent);

		// then
		verify(learnerSubscriberService, timeout(EVENT_NOTIFICATION_TIMEOUT)).flashcardWasCreated(flashcardWasCreatedEvent);
	}

	@Override
	protected String testedTopicName() {
		return "flashcard_was_created_queue";
	}

	@Override
	protected FlashcardWasCreatedEvent createDomainEvent() {
		final @NonNull LanguageCode domesticLanguage = LanguageCode.create(randomUppercaseAlphabeticallString(2));
		final @NonNull LanguageCode translationLanguage = LanguageCode.create(randomUppercaseAlphabeticallString(2));
		final @NonNull Word domesticWord = Word.create(randomString());
		final @NonNull Word translatedWord = Word.create(randomString());
		final @NonNull Login ownerLogin = Login.create(randomLowarcaseAlphabeticallString(6));
		final Visibility visibility = Visibility.PRIVATE;
		final @NonNull CreationDate creationDate = CreationDate.create(LocalDateTime.now());
		final int concurencyVersion = 1;
		return new FlashcardWasCreatedEvent(domesticLanguage.value(),
			translationLanguage.value(),
			domesticWord.value(),
			translatedWord.value(),
			ownerLogin.value(),
			visibility.name(),
			creationDate.value(),
			concurencyVersion);
	}

	@Override
	protected EmbeddedKafkaBroker embeddedKafkaBroker() {
		return embeddedKafkaBroker;
	}
}
