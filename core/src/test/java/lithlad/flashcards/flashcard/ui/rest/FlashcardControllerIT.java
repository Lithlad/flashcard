package lithlad.flashcards.flashcard.ui.rest;

import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.OK;

import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MvcResult;

import design.utils.AbstractControllerTest;
import design.application.command.commands.Information;
import design.security.infrastructure.UserService;
import lithlad.flashcards.flashcard.application.command.FlashcardApplicationService;
import lithlad.flashcards.flashcard.domain.dto.FlashcardDTO;
import lithlad.flashcards.flashcard.ui.rest.form.CreateFlashcardForm;

@Tag("it")
@WebMvcTest(FlashcardController.class)
@TestPropertySource(locations="classpath:application-test.properties")
class FlashcardControllerIT extends AbstractControllerTest {

	private static final String CONTROLLER_PREFIX = "/api/flashcard";

	@MockBean
	private FlashcardApplicationService flashcardApplicationService;

	@MockBean
	private UserService userService;

	@WithMockUser(username="learner1")
	@Test
	void shouldCreateFlashcard() throws Exception {
		// given
		final String domesticLang = "PL";
		final String translationLang = "EN";
		final String domesticWord = "pies";
		final String translatedWord = "dog";
		final String owner = "owner";
		final String visibility = "visible";
		final CreateFlashcardForm registerFlashcardForm = new CreateFlashcardForm(domesticLang, translationLang, domesticWord, translatedWord);
		final List<Information> notAddedLanguages = List.of(
			new Information(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
		final FlashcardDTO flashcardDTO = sampleFlashcardDTO(domesticLang, translationLang, domesticWord, translatedWord, owner, visibility);
		when(flashcardApplicationService.create(registerFlashcardForm)).thenReturn(flashcardDTO);

		// when
		final MvcResult result = performLoggedPost(CONTROLLER_PREFIX + "/create", registerFlashcardForm);

		// then
		verify(flashcardApplicationService).create(registerFlashcardForm);
		assertThat(result.getResponse().getStatus()).isEqualTo(OK.value());

		// TODO map to json
		assertThat(format("{\"domesticLanguage\":\"%s\",\"domesticWord\":\"%s\",\"ownerLogin\":\"%s\",\"translatedWord\":\"%s\",\"translationLang\":\"%s\",\"visibility\":\"%s\"}",
			domesticLang, domesticWord, owner, translatedWord, translationLang, visibility))
			.isEqualTo(result.getResponse().getContentAsString());
	}

	private FlashcardDTO sampleFlashcardDTO(final String domesticLang, final String translationLang,
	                                        final String domesticWord, final String translatedWord,
	                                        final String owner, final String visibility
	) {
		return new FlashcardDTO(domesticLang, domesticWord, owner, translatedWord, translationLang, visibility);
	}
}
