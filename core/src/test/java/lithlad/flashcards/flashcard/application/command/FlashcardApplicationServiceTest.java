package lithlad.flashcards.flashcard.application.command;

import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.UUID;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import lithlad.flashcards.crossdomain.domain.LanguageCode;
import design.infrastructure.TimeDefinitions;
import design.user.application.query.UserViewQueryService;
import design.user.domain.Login;
import lithlad.flashcards.flashcard.application.command.commands.CreateFlashcardCommand;
import lithlad.flashcards.flashcard.domain.dto.FlashcardDTO;
import lithlad.flashcards.flashcard.domain.Flashcard;
import lithlad.flashcards.flashcard.domain.FlashcardRepository;
import lithlad.flashcards.flashcard.domain.Word;
import lithlad.flashcards.flashcard.domain.exceptions.FlashcardAlreadyExistsException;

@Tag("unit")
class FlashcardApplicationServiceTest {

	private final FlashcardRepository flashcardRepository = mock(FlashcardRepository.class);

	private final TimeDefinitions timeDefinitions = mock(TimeDefinitions.class);

	private final UserViewQueryService userViewQueryService = mock(UserViewQueryService.class);

	private final FlashcardApplicationService service = new FlashcardApplicationService(flashcardRepository, timeDefinitions, userViewQueryService);

	@Test
	void shouldCreateFlashcardIfItDoesntExist() {
		// given
		final CreateFlashcardCommand command = mock(CreateFlashcardCommand.class);
		final String domesticLanguage = "DE";
		final String translationLanguage = "PL";
		final String domesticWord = UUID.randomUUID().toString();
		final String translatedWord = UUID.randomUUID().toString();
		final String ownerLogin = "login";
		when(command.getDomesticLang()).thenReturn(domesticLanguage);
		when(command.getTranslationLang()).thenReturn(translationLanguage);
		when(command.getDomesticWord()).thenReturn(domesticWord);
		when(command.getTranslatedWord()).thenReturn(translatedWord);
		when(userViewQueryService.currentUserLogin()).thenReturn(Login.create(ownerLogin));
		final Flashcard flashcard = mock(Flashcard.class);
		final FlashcardDTO expectedDTO = mock(FlashcardDTO.class);
		when(flashcard.mapToDTO()).thenReturn(expectedDTO);
		when(flashcardRepository.doesExist(
			LanguageCode.create(domesticLanguage), LanguageCode.create(translationLanguage), Word.create(domesticWord), Login.create(ownerLogin)))
			.thenReturn(false);
		when(flashcardRepository.save(any(Flashcard.class)))
			.thenReturn(flashcard);
		when(timeDefinitions.now()).thenReturn(LocalDateTime.now());

		// when
		final FlashcardDTO flashcardDTO = service.create(command);

		// then
		verify(flashcardRepository).doesExist(
			LanguageCode.create(domesticLanguage), LanguageCode.create(translationLanguage), Word.create(domesticWord), Login.create(ownerLogin));
		verify(flashcardRepository).save(any(Flashcard.class));
		assertThat(flashcardDTO).isEqualTo(expectedDTO);
	}

	@Test
	void shouldThrowExceptionIfFlashcardAlreadyExists() {
		// given
		final CreateFlashcardCommand command = mock(CreateFlashcardCommand.class);
		final String domesticLanguage = "DE";
		final String translationLanguage = "PL";
		final String domesticWord = UUID.randomUUID().toString();
		final String translatedWord = UUID.randomUUID().toString();
		final String ownerLogin = "login";
		when(command.getDomesticLang()).thenReturn(domesticLanguage);
		when(command.getTranslationLang()).thenReturn(translationLanguage);
		when(command.getDomesticWord()).thenReturn(domesticWord);
		when(command.getTranslatedWord()).thenReturn(translatedWord);
		when(userViewQueryService.currentUserLogin()).thenReturn(Login.create(ownerLogin));
		when(flashcardRepository.doesExist(
			LanguageCode.create(domesticLanguage), LanguageCode.create(translationLanguage), Word.create(domesticWord), Login.create(ownerLogin)))
			.thenReturn(true);

		// when
		final FlashcardAlreadyExistsException exception = assertThrows(
			FlashcardAlreadyExistsException.class,
			() -> service.create(command));

		// then
		verify(flashcardRepository).doesExist(
			LanguageCode.create(domesticLanguage), LanguageCode.create(translationLanguage), Word.create(domesticWord), Login.create(ownerLogin));
		verify(flashcardRepository, never()).save(any(Flashcard.class));
		assertThat(exception).hasMessage(
			format("Flashcard %s %s %s already exists for this user", domesticLanguage, translationLanguage, domesticWord));
	}
}
