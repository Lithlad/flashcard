package lithlad.flashcards.flashcard.domain;

import static design.utils.TestUtils.assertEventIsRegistered;
import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;
import java.util.UUID;

import org.junit.jupiter.api.Test;

import design.domain.CreationDate;
import design.user.domain.Login;
import lithlad.flashcards.communication.flashcard.event.FlashcardWasCreatedEvent;
import lithlad.flashcards.crossdomain.domain.LanguageCode;

class FlashcardTest {

	@Test
	void shouldCreateFlashcard() {
		// given
		final LanguageCode domesticLanguage = LanguageCode.create("EE");
		final LanguageCode translationLanguage = LanguageCode.create("DD");
		final Word domesticWord = Word.create(UUID.randomUUID().toString());
		final Word translatedWord = Word.create(UUID.randomUUID().toString());
		final Login ownerLogin = Login.create("login");
		final Visibility visibility = Visibility.PRIVATE;
		final CreationDate creationDate = CreationDate.create(LocalDateTime.now());

		// when
		final Flashcard flashcard =
			new Flashcard(domesticLanguage, translationLanguage, domesticWord, translatedWord, ownerLogin, visibility, creationDate);

		// then
		assertThat(flashcard)
			.extracting("domesticLang", "translationLang", "domesticWord",
				"translatedWord", "ownerLogin", "visibility", "creationDate")
			.containsExactly(domesticLanguage, translationLanguage, domesticWord,
				translatedWord, ownerLogin, visibility, creationDate);

		assertEventIsRegistered(flashcard,
			new FlashcardWasCreatedEvent(domesticLanguage.value(), translationLanguage.value(), domesticWord.value(), translatedWord.value(), ownerLogin.value(), visibility.name(), creationDate.value(), -1));
	}
}
