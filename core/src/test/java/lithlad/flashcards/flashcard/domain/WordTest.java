package lithlad.flashcards.flashcard.domain;

import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import design.domain.exceptions.DomainValueLengthExceedsMaximalLengthException;

@Tag("unit")
class WordTest {

	@Test
	void shouldCreateWord() {
		// given
		final String wordValue = UUID.randomUUID().toString();

		// when
		final Word word = Word.create(wordValue);

		// then
		assertThat(word).extracting("value").isEqualTo(wordValue);
	}

	@Test
	void shouldThrowExceptionIfWordExceeds255Signs() {
		// given
		final String wordValue = IntStream.iterate(0, i -> i+1).limit(256).mapToObj(String::valueOf).collect(Collectors.joining());

		// when
		final DomainValueLengthExceedsMaximalLengthException exception =
			assertThrows(DomainValueLengthExceedsMaximalLengthException.class, () -> Word.create(wordValue));

		// then
		assertThat(exception).hasMessage(format("Value %s exceeds maximal allowed length which is %s", wordValue, 255));
	}

}
