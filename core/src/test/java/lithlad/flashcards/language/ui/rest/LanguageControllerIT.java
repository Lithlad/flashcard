package lithlad.flashcards.language.ui.rest;

import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.OK;

import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MvcResult;

import design.application.command.commands.Information;
import design.security.infrastructure.UserService;
import design.utils.AbstractControllerTest;
import lithlad.flashcards.language.application.command.LanguageApplicationService;
import lithlad.flashcards.language.ui.rest.form.RegisterLanguagesForm;
import lithlad.flashcards.language.ui.rest.form.RegisterSingleLanguageForm;

@Tag("it")
@WebMvcTest(LanguageController.class)
@TestPropertySource(locations="classpath:application-test.properties")
class LanguageControllerIT extends AbstractControllerTest {

	private static final String CONTROLLER_PREFIX = "/api/language";

	@MockBean
	private LanguageApplicationService languageApplicationService;

	@MockBean
	private UserService userService;

	@WithMockUser(username="learner1")
	@Test
	void shouldRegisterLanguages() throws Exception {
		// given
		final RegisterLanguagesForm registerLanguagesForm = new RegisterLanguagesForm(Stream.of(
			new AbstractMap.SimpleEntry<>("PL", new RegisterSingleLanguageForm("PL", "polish")),
			new AbstractMap.SimpleEntry<>("EN", new RegisterSingleLanguageForm("EN", "english"))
		).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));
		final List<Information> notAddedLanguages = List.of(
			new Information(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
		when(languageApplicationService.registerLanguages(registerLanguagesForm)).thenReturn(notAddedLanguages);

		// when
		final MvcResult result = performLoggedPost(CONTROLLER_PREFIX + "/register", registerLanguagesForm);

		// then
		verify(languageApplicationService).registerLanguages(registerLanguagesForm);
		assertThat(result.getResponse().getStatus()).isEqualTo(OK.value());
		// TODO map to json
		assertThat(format("[{\"identificator\":\"%s\",\"message\":\"%s\"}]", notAddedLanguages.get(0).getIdentificator(), notAddedLanguages.get(0).getMessage()))
			.isEqualTo(result.getResponse().getContentAsString());
	}
}
