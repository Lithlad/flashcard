package lithlad.flashcards.language.application.command.commands;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import design.application.command.commands.Information;
import design.domain.CreationDate;
import design.infrastructure.TimeDefinitions;
import lithlad.flashcards.language.application.command.LanguageApplicationService;
import lithlad.flashcards.crossdomain.domain.LanguageCode;
import lithlad.flashcards.language.domain.Country;
import lithlad.flashcards.language.domain.LanguageFactory;
import lithlad.flashcards.language.domain.LanguageRepository;

@Tag("unit")
class LanguageApplicationServiceTest {

	private LanguageRepository languageRepository = mock(LanguageRepository.class);

	private TimeDefinitions timeDefinitions = mock(TimeDefinitions.class);

	private final LanguageApplicationService languageApplicationService = new LanguageApplicationService(languageRepository, timeDefinitions);

	@Test
	void shouldAddLanguagesAndReturnNotAddedLanguages() {
		// given
		final RegisterLanguagesCommand registerLanguagesCommand = mock(RegisterLanguagesCommand.class);
		final Map<String, ? extends RegisterSingleLanguageCommand> languagesList = createLanguagesMap();
		doReturn(languagesList).when(registerLanguagesCommand).getLanguages();
		when(languageRepository.getExistingByCodes(
			Set.of(LanguageCode.create("PL"), LanguageCode.create("EN"), LanguageCode.create("DE"), LanguageCode.create("CH"), LanguageCode.create("RU"))))
			.thenReturn(Set.of(LanguageCode.create("PL")));
		when(languageRepository.getExistingByCountries(
			Set.of(Country.create("polish"), Country.create("english"), Country.create("german"), Country.create("chinese"), Country.create("russian"))))
			.thenReturn(Set.of(LanguageCode.create("DE"), LanguageCode.create("CH")));
		final LocalDateTime now = LocalDateTime.now();
		when(timeDefinitions.now()).thenReturn(now);

		// when
		List<Information> notAddedLanguages = languageApplicationService.registerLanguages(registerLanguagesCommand);

		// then
		verify(languageRepository).getExistingByCodes(
			Set.of(LanguageCode.create("PL"), LanguageCode.create("EN"), LanguageCode.create("DE"), LanguageCode.create("CH"), LanguageCode.create("RU")));
		verify(languageRepository).getExistingByCountries(
			Set.of(Country.create("polish"), Country.create("english"), Country.create("german"), Country.create("chinese"), Country.create("russian")));
		verify(languageRepository).save(Set.of(
			LanguageFactory.create(LanguageCode.create("EN"), Country.create("english"), CreationDate.create(timeDefinitions.now())),
			LanguageFactory.create(LanguageCode.create("RU"), Country.create("russian"), CreationDate.create(timeDefinitions.now()))
		));

		assertThat(notAddedLanguages).containsExactly(
			new Information("PL", "Language with code PL already exists"),
			new Information("DE", "Nationality GERMAN already exists"),
			new Information("CH", "Nationality CHINESE already exists"));
	}

	@Test
	void shouldAddLanguagesWhenNoAlreadyExitingLanguagesAreFound() {
		// given
		final RegisterLanguagesCommand registerLanguagesCommand = mock(RegisterLanguagesCommand.class);
		final Map<String, ? extends RegisterSingleLanguageCommand> languagesList = createLanguagesMap();
		doReturn(languagesList).when(registerLanguagesCommand).getLanguages();
		when(languageRepository.getExistingByCodes(
			Set.of(LanguageCode.create("PL"), LanguageCode.create("EN"), LanguageCode.create("DE"), LanguageCode.create("CH"), LanguageCode.create("RU"))))
			.thenReturn(Set.of());
		when(languageRepository.getExistingByCountries(
			Set.of(Country.create("polish"), Country.create("english"), Country.create("german"), Country.create("chinese"), Country.create("russian"))))
			.thenReturn(Set.of());
		final LocalDateTime now = LocalDateTime.now();
		when(timeDefinitions.now()).thenReturn(now);

		// when
		List<Information> notAddedLanguages = languageApplicationService.registerLanguages(registerLanguagesCommand);

		// then
		verify(languageRepository).getExistingByCodes(
			Set.of(LanguageCode.create("PL"), LanguageCode.create("EN"), LanguageCode.create("DE"), LanguageCode.create("CH"), LanguageCode.create("RU")));
		verify(languageRepository).getExistingByCountries(
			Set.of(Country.create("polish"), Country.create("english"), Country.create("german"), Country.create("chinese"), Country.create("russian")));
		verify(languageRepository).save(Set.of(
			LanguageFactory.create(LanguageCode.create("PL"), Country.create("polish"), CreationDate.create(timeDefinitions.now())),
			LanguageFactory.create(LanguageCode.create("EN"), Country.create("english"), CreationDate.create(timeDefinitions.now())),
			LanguageFactory.create(LanguageCode.create("DE"), Country.create("german"), CreationDate.create(timeDefinitions.now())),
			LanguageFactory.create(LanguageCode.create("CH"), Country.create("chinese"), CreationDate.create(timeDefinitions.now())),
			LanguageFactory.create(LanguageCode.create("RU"), Country.create("russian"), CreationDate.create(timeDefinitions.now()))
		));

		assertThat(notAddedLanguages).isEmpty();
	}

	private Map<String, RegisterSingleLanguageCommand> createLanguagesMap() {
		return Map.of(
			"PL", createSingleLanguageCommand("PL", "polish"),
			"EN", createSingleLanguageCommand("EN", "english"),
			"DE", createSingleLanguageCommand("DE", "german"),
			"CH", createSingleLanguageCommand("CH", "chinese"),
			"RU", createSingleLanguageCommand("RU", "russian"));
	}

	private RegisterSingleLanguageCommand createSingleLanguageCommand(final String code, final String country) {
		final RegisterSingleLanguageCommand registerLanguageCommand = mock(RegisterSingleLanguageCommand.class);
		when(registerLanguageCommand.getCode()).thenReturn(code);
		when(registerLanguageCommand.getCountry()).thenReturn(country);
		return registerLanguageCommand;
	}
}
