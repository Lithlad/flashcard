package lithlad.flashcards.language.infrastructure.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import javax.validation.constraints.NotBlank;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import lithlad.flashcards.crossdomain.domain.LanguageCode;

@Tag("unit")
class JpaLanguageViewQueryServiceTest {

	private LanguageRepositoryFacade languageRepositoryFacade = mock(LanguageRepositoryFacade.class);

	private final JpaLanguageViewQueryService jpaLanguageViewQueryService = new JpaLanguageViewQueryService(languageRepositoryFacade);

	@Test
	void shouldReturnTrueIfLanguageExists() {
		// given
		@NotBlank final String code = "PL";
		when(languageRepositoryFacade.doesExist(LanguageCode.create(code))).thenReturn(true);

		// when
		final boolean doesExist = jpaLanguageViewQueryService.doesExist(code);

		// then
		assertThat(doesExist).isTrue();
	}

	@Test
	void shouldReturnFalseIfLanguageDoesntExist() {
		// given
		@NotBlank final String code = "PL";
		when(languageRepositoryFacade.doesExist(LanguageCode.create(code))).thenReturn(false);

		// when
		final boolean doesExist = jpaLanguageViewQueryService.doesExist(code);

		// then
		assertThat(doesExist).isFalse();
	}
}