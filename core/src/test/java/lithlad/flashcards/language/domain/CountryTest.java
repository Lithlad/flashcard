package lithlad.flashcards.language.domain;

import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import design.domain.exceptions.DomainValueLengthExceedsMaximalLengthException;
import lithlad.flashcards.language.domain.exceptions.CountryNameMustContainOnlyLettersException;

@Tag("unit")
class CountryTest {

	@Test
	void shouldCreateCountry() {
		// given
		final String countryValue = "Poland";

		// when
		final Country country = Country.create(countryValue);

		// then
		assertThat(country.value()).isEqualTo(countryValue.toUpperCase());
	}

	@Test
	void shouldThrowExceptionIfCountryLengthExceeds20() {
		// given
		final String countryValue = "PolandPolandPolandPolandPolandPolandPoland";

		// when
		final DomainValueLengthExceedsMaximalLengthException exception = assertThrows(DomainValueLengthExceedsMaximalLengthException.class, () -> Country.create(countryValue));

		// then
		assertThat(exception).hasMessage(format("Value %s exceeds maximal allowed length which is %s", countryValue.toUpperCase(), 20));
	}

	@Test
	public void shouldThrowExceptionIfCountryNameContainsNumbers() {
		// given
		final String countryValue = "Poland2";

		// when
		final CountryNameMustContainOnlyLettersException exception = assertThrows(CountryNameMustContainOnlyLettersException.class, () -> Country.create(countryValue));

		// then
		assertThat(exception).hasMessage(format("Country name %s contains forbidden signs", countryValue));
	}
}
