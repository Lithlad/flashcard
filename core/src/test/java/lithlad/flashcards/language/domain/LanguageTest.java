package lithlad.flashcards.language.domain;

import static design.utils.TestUtils.assertEventIsRegistered;
import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import design.domain.CreationDate;
import lithlad.flashcards.communication.language.event.LanguageWasCreatedEvent;
import lithlad.flashcards.crossdomain.domain.LanguageCode;

@Tag("unit")
class LanguageTest {

	@Test
	void shouldCreateNewInstance() {
		// given
		final LanguageCode code = LanguageCode.create("EN");
		final Country country = Country.create("English");
		final CreationDate creationDate = CreationDate.create(LocalDateTime.now());

		// when
		final Language language = new Language(code, country, creationDate);

		// then
		assertThat(language)
			.extracting("code", "country", "creationDate")
			.containsExactly(code, country, creationDate);
		assertEventIsRegistered(language,
			new LanguageWasCreatedEvent(code.value(), country.value(), creationDate.epoch(), -1));
	}

}
