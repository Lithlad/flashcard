package lithlad.flashcards.crossdomain.domain;

import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import lithlad.flashcards.crossdomain.domain.exceptions.InappropriateSignsInLanguageCodeException;

@Tag("unit")
class LanguageCodeTest {

	@Test
	void shouldCreateCodeIfLengthIs2() {
		// given
		final String codeValue = "EN";

		// when
		final LanguageCode code = LanguageCode.create(codeValue);

		// then
		assertThat(code.value()).isEqualTo(codeValue.toUpperCase());
	}

	@Test
	void shouldThrowExceptionIfLengthIsLongerThen2() {
		// given
		final String codeValue = "ENA";

		// when
		final InappropriateSignsInLanguageCodeException exception = assertThrows(InappropriateSignsInLanguageCodeException.class, () -> LanguageCode.create(codeValue));

		//then
		assertThat(exception).hasMessage(format("Language code must contain exactly two uppercase letters. Code %s doesnt fulfill this requirement", codeValue));
	}

	@Test
	void shouldThrowExceptionIfLengthIsShorterThen2() {
		// given
		final String codeValue = "E";

		// when
		final InappropriateSignsInLanguageCodeException exception = assertThrows(InappropriateSignsInLanguageCodeException.class, () -> LanguageCode.create(codeValue));

		//then
		assertThat(exception).hasMessage(format("Language code must contain exactly two uppercase letters. Code %s doesnt fulfill this requirement", codeValue));
	}

	@Test
	void shouldThrowExceptionIfContainsDigits() {
		// given
		final String codeValue = "E1";

		// when
		final InappropriateSignsInLanguageCodeException exception = assertThrows(InappropriateSignsInLanguageCodeException.class, () -> LanguageCode.create(codeValue));

		//then
		assertThat(exception).hasMessage(format("Language code must contain exactly two uppercase letters. Code %s doesnt fulfill this requirement", codeValue));
	}

	@Test
	void shouldThrowExceptionIfContainsLowercase() {
		// given
		final String codeValue = "Ee";

		// when
		final InappropriateSignsInLanguageCodeException exception = assertThrows(InappropriateSignsInLanguageCodeException.class, () -> LanguageCode.create(codeValue));

		//then
		assertThat(exception).hasMessage(format("Language code must contain exactly two uppercase letters. Code %s doesnt fulfill this requirement", codeValue));
	}

}