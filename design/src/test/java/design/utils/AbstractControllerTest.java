package design.utils;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@AutoConfigureMockMvc
public abstract class AbstractControllerTest {

	@Autowired
	private MockMvc mvc;

	protected MvcResult performLoggedPost(final String url, final Object form) throws Exception {
		final MockHttpServletRequestBuilder post = post(url, form).with(csrf());
		return mvc.perform(post).andReturn();
	}

	protected MvcResult performLoggedGet(final String url) throws Exception {
		final MockHttpServletRequestBuilder get = get(url).with(csrf());
		return mvc.perform(get).andReturn();
	}

	protected MvcResult performPost(final String url, final Object form) throws Exception {
		return mvc.perform(post(url, form)).andReturn();
	}

	private MockHttpServletRequestBuilder get(final String url) {
		return MockMvcRequestBuilders.get(url);
	}

	private MockHttpServletRequestBuilder post(final String url, Object form) {
		return MockMvcRequestBuilders.post(url, form)
			.contentType(MediaType.APPLICATION_JSON)
			.content(toJson(form));
	}

	private String toJson(final Object form) {
		try {
			return new ObjectMapper().writeValueAsString(form);
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}
	}
}
