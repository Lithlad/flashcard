package design.utils.UserInstance;

import design.domain.CreationDate;
import design.user.domain.Email;
import design.user.domain.Login;
import design.user.domain.Pass;

public class UserInstanceFactory {

	private UserInstanceFactory() {}

	public static UserInstance create(final Login login, final Pass pass, final Email email, final CreationDate creationDate) {
		return new UserInstance(login, pass, email, creationDate);
	}
}
