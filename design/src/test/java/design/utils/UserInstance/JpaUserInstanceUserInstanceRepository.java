package design.utils.UserInstance;

import static java.util.Optional.empty;
import static java.util.Optional.of;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import design.user.application.query.UserViewInstanceQueryService;
import design.user.domain.Login;
import design.user.domain.User;
import design.user.domain.UserInstanceRepository;
import lombok.NonNull;

@Repository
public class JpaUserInstanceUserInstanceRepository implements UserInstanceRepository, UserViewInstanceQueryService {

	private final JpaUserInstanceRepository jpaUserInstanceRepository;

	@Autowired
	public JpaUserInstanceUserInstanceRepository(final JpaUserInstanceRepository jpaUserInstanceRepository) {
		this.jpaUserInstanceRepository = jpaUserInstanceRepository;
	}

	@Override
	public Optional<? extends User> findByLogin(final @NonNull Login login) {
		return jpaUserInstanceRepository.findByLogin(login);
	}

	@Override
	public Optional<User> saveIfSuitable(final @NonNull User user) {
		if (!(user instanceof UserInstance))
			return empty();

		return of(jpaUserInstanceRepository.save((UserInstance) user));
	}

	@Override
	public boolean doesExist(final @NonNull Login login) {
		return jpaUserInstanceRepository.doesExist(login);
	}
}
