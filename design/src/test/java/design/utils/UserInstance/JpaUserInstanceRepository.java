package design.utils.UserInstance;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import design.user.domain.Login;
import lombok.NonNull;

@Repository
public interface JpaUserInstanceRepository extends JpaRepository<UserInstance, Long> {
	Optional<UserInstance> findByLogin(@NonNull final Login login);

	@Query("SELECT CASE WHEN(COUNT(l) > 0) THEN true ELSE false END FROM UserInstance l WHERE l.login = :login")
	boolean doesExist(@NonNull final Login login);
}
