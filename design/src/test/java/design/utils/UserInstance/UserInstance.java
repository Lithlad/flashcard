package design.utils.UserInstance;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;

import design.domain.CreationDate;
import design.user.domain.Email;
import design.user.domain.Login;
import design.user.domain.Pass;
import design.user.domain.User;
import design.user.domain.dto.UserDTO;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "usr_userinst")
@NoArgsConstructor
public class UserInstance extends User {

	public UserInstance(final Login login, final Pass pass, final Email email, final CreationDate creationDate) {
		super(creationDate, login, pass, email);
	}

	@Override
	public UserDTO mapToDTO() {
		return null;
	}

	@Override
	protected List<String> getRoles() {
		return List.of("LEARNER");
	}
}
