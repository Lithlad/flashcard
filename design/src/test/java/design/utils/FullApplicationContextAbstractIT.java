package design.utils;

import org.junit.jupiter.api.Tag;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;

@Tag("it")
@SpringBootTest
@EmbeddedKafka(
	partitions = 1,
	brokerProperties = {
//		podmienic na property kafka.bootstrapAddress
		"listeners=PLAINTEXT://localhost:3333"
	},
	controlledShutdown = true
)
@DirtiesContext
@TestPropertySource(locations="classpath:application-test.properties")
public class FullApplicationContextAbstractIT {
}
