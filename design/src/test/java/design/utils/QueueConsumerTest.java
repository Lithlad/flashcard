package design.utils;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.junit.jupiter.api.Tag;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.utils.KafkaTestUtils;

import design.domain.DomainEvent;

@Tag("queue")
public abstract class QueueConsumerTest<DE extends DomainEvent> extends FullApplicationContextAbstractIT {

	protected static final int EVENT_NOTIFICATION_TIMEOUT = 5000;

	protected abstract String testedTopicName();

	protected abstract DE createDomainEvent();

	protected abstract EmbeddedKafkaBroker embeddedKafkaBroker();

	protected void sendEvent(final DE event) {
		final Producer<String, DomainEvent> producer = configureProducer();
		producer.send(new ProducerRecord<>(testedTopicName(), event));
		producer.flush();
	}

	private Producer<String, DomainEvent> configureProducer() {
		final Map<String, Object> configs = new HashMap<>(KafkaTestUtils.producerProps(embeddedKafkaBroker()));
		configs.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		configs.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);

		return new DefaultKafkaProducerFactory<String, DomainEvent>(configs).createProducer();
	}

}
