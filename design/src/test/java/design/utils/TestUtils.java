package design.utils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.util.ReflectionTestUtils.getField;

import java.util.List;
import java.util.Random;
import java.util.UUID;

import javax.validation.constraints.NotBlank;

import design.domain.Aggregate;
import design.domain.DomainEvent;
import lombok.NonNull;

public class TestUtils {

    public static void assertEventIsRegistered(final Aggregate aggregate, final DomainEvent event) {
        final List<DomainEvent> events = getFieldValue(aggregate, "events", List.class);
        assertThat(events).contains(event);
    }

    public static String randomString() {
        return UUID.randomUUID().toString();
    }

    public static String randomLowarcaseAlphabeticallString(final int length) {
        final int leftLimit = 97;   // letter 'a'
        final int rightLimit = 122; // letter 'z'

        final Random random = new Random();
        return random.ints(leftLimit, rightLimit + 1)
            .limit(length)
            .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
            .toString();
    }

    public static String randomUppercaseAlphabeticallString(final int length) {
        return randomLowarcaseAlphabeticallString(length).toUpperCase();
    }

    private static <E> E getFieldValue(@NonNull final Object sourceObject, @NotBlank final String fieldName, @NonNull final Class<E> expectedClass) {
        try {
            final Object requiredObject = getField(sourceObject, fieldName);
            return expectedClass.cast(requiredObject);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
}
