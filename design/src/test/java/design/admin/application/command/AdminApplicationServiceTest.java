package design.admin.application.command;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.UUID;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import design.admin.application.command.AdminApplicationService;
import design.admin.application.command.commands.RegisterAdminCommand;
import design.admin.domain.AdminFactory;
import design.admin.infrastructure.JpaAdminRepositoryFacade;
import design.domain.CreationDate;
import design.infrastructure.TimeDefinitions;
import design.user.domain.Email;
import design.user.domain.Login;
import design.user.domain.Pass;
import design.user.domain.UserRepository;
import lombok.NonNull;

@Tag("unit")
class AdminApplicationServiceTest {

	private final JpaAdminRepositoryFacade jpaAdminRepository = mock(JpaAdminRepositoryFacade.class);

	private final TimeDefinitions timeDefinitions = mock(TimeDefinitions.class);

	private final UserRepository userRepository = mock(UserRepository.class);

	private final AdminApplicationService adminApplicationService = new AdminApplicationService(userRepository, jpaAdminRepository, timeDefinitions);

	@Test
	void shouldRegisterNewAdmin() {
		// given
		final RegisterAdminCommand registerLearnerCommand = mock(RegisterAdminCommand.class);
		final String planeLogin = UUID.randomUUID().toString();
		final String planePass = UUID.randomUUID().toString();
		final String planeEmail = "email@mail.com";
		final LocalDateTime planeCreationTime = LocalDateTime.now();
		when(registerLearnerCommand.getLogin()).thenReturn(planeLogin);
		when(registerLearnerCommand.getPass()).thenReturn(planePass);
		when(registerLearnerCommand.getEmail()).thenReturn(planeEmail);
		when(timeDefinitions.now()).thenReturn(planeCreationTime);
		when(userRepository.doesExist(Login.create(planeLogin))).thenReturn(false);

		// when
		adminApplicationService.register(registerLearnerCommand);

		// then
		verify(userRepository).doesExist(Login.create(planeLogin));
		verify(jpaAdminRepository).save(AdminFactory.create(Login.create(planeLogin), Pass.create(planePass),
			Email.create(planeEmail), CreationDate.create(planeCreationTime)));
	}

	@Test
	void shouldReturnTrueIfUserIsAppropriateForUser() {
		// given
		final @NonNull Login login = Login.create("login1");
		when(jpaAdminRepository.doesExist(login)).thenReturn(true);

		// when
		final boolean isAppropriate = adminApplicationService.isAppropriate(login);

		// then
		assertThat(isAppropriate).isTrue();
	}

	@Test
	void shouldReturnFalseIfUserIsAppropriateForUser() {
		// given
		final @NonNull Login login = Login.create("login1");
		when(jpaAdminRepository.doesExist(login)).thenReturn(false);

		// when
		final boolean isAppropriate = adminApplicationService.isAppropriate(login);

		// then
		assertThat(isAppropriate).isFalse();
	}
}
