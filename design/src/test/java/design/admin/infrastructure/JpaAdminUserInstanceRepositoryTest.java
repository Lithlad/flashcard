package design.admin.infrastructure;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import design.admin.domain.Admin;
import design.admin.infrastructure.JpaAdminRepository;
import design.admin.infrastructure.JpaAdminUserInstanceRepository;
import design.user.domain.Login;
import design.user.domain.User;
import design.user.domain.dto.UserDTO;
import lombok.NonNull;

@Tag("unit")
class JpaAdminUserInstanceRepositoryTest {

	private final JpaAdminRepository jpaAdminRepository = mock(JpaAdminRepository.class);

	private final JpaAdminUserInstanceRepository jpaAdminUserInstanceRepository = new JpaAdminUserInstanceRepository(jpaAdminRepository);

	@Test
	void shouldFindByLogin() {
		// given
		final @NonNull Login login = Login.create("login");

		final Optional<Admin> admin = Optional.of(mock(Admin.class));
		when(jpaAdminRepository.findByLogin(login)).thenReturn(admin);

		// when
		final Optional<? extends User> byLogin = jpaAdminUserInstanceRepository.findByLogin(login);

		// then
		verify(jpaAdminRepository).findByLogin(login);
		assertThat(byLogin).isEqualTo(admin);
	}

	@Test
	void shouldSaveIfSuitable() {
		// given
		final Admin admin = mock(Admin.class);
		when(jpaAdminRepository.save(admin)).thenReturn(admin);

		// when
		final Optional<User> maybeUser = jpaAdminUserInstanceRepository.saveIfSuitable(admin);

		// then
		verify(jpaAdminRepository).save(admin);
		assertThat(maybeUser).isNotEmpty();
	}

	@Test
	void shouldNotSaveIfNotSuitable() {
		// given
		final User user = mock(ConcreteUser.class);

		// when
		final Optional<User> maybeUser = jpaAdminUserInstanceRepository.saveIfSuitable(user);

		// then
		verify(jpaAdminRepository, never()).save(any(Admin.class));
		assertThat(maybeUser).isEmpty();
	}

	static class ConcreteUser extends User {

		@Override
		public UserDTO mapToDTO() {
			return mock(UserDTO.class);
		}

		@Override
		protected List<String> getRoles() {
			return null;
		}
	}

}
