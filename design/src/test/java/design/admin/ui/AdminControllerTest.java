package design.admin.ui;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.springframework.http.HttpStatus.CREATED;

import java.util.UUID;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MvcResult;

import design.utils.AbstractControllerTest;
import design.admin.application.command.AdminApplicationService;
import design.admin.application.command.commands.RegisterAdminCommand;
import design.admin.ui.form.RegisterAdminForm;
import design.security.infrastructure.UserService;

@Tag("unit")
@WebMvcTest(AdminController.class)
@TestPropertySource(locations="classpath:application-test.properties")
class AdminControllerTest extends AbstractControllerTest {

	private static final String CONTROLLER_PREFIX = "/api/admin";

	@MockBean
	private AdminApplicationService adminApplicationService;

	@MockBean
	private UserService userService;

	@Test
	void shouldRegisterAdmin() throws Exception {
		// given
		final String login = UUID.randomUUID().toString();
		final String pass = UUID.randomUUID().toString();
		final String email = "example@lithlad.eu";
		final RegisterAdminCommand registerAdminForm = new RegisterAdminForm(login, pass, email);

		// when
		final MvcResult result = performPost(CONTROLLER_PREFIX + "/register", registerAdminForm);

		// then
		verify(adminApplicationService).register(registerAdminForm);
		assertThat(result.getResponse().getStatus()).isEqualTo(CREATED.value());
	}
}
