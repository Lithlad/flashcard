package design.admin.domain;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;
import java.util.UUID;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import design.admin.domain.dto.AdminDTO;
import design.admin.domain.communication.events.AdminWasCreatedEvent;
import design.domain.CreationDate;
import design.user.domain.Email;
import design.user.domain.Login;
import design.user.domain.Pass;
import design.utils.TestUtils;

@Tag("unit")
class AdminTest {

	@Test
	void shouldCreateNewInstance() {
		// given
		final Login login = Login.create(UUID.randomUUID().toString());
		final Pass pass = Pass.create(UUID.randomUUID().toString());
		final Email email = Email.create("email@mail.com");
		final CreationDate creationDate = CreationDate.create(LocalDateTime.now());

		// when
		final Admin admin = new Admin(login, pass, email, creationDate);

		// then
		assertThat(admin)
			.extracting("login", "pass", "email", "creationDate")
			.containsExactly(login, pass, email, creationDate);
		TestUtils.assertEventIsRegistered(admin,
			new AdminWasCreatedEvent(login, email, creationDate, -1));
	}

	@Test
	void shouldMapToDto() {
		// given
		final Login login = Login.create(UUID.randomUUID().toString());
		final Pass pass = Pass.create(UUID.randomUUID().toString());
		final Email email = Email.create("email@mail.com");
		final CreationDate creationDate = CreationDate.create(LocalDateTime.now());
		final Admin admin = new Admin(login, pass, email, creationDate);

		// when
		final AdminDTO adminDTO = admin.mapToDTO();

		// then
		assertThat(adminDTO.getLogin()).isEqualTo(login.value());
		assertThat(adminDTO.getEmail()).isEqualTo(email.value());
		assertThat(adminDTO.getCreationDate()).isEqualTo(creationDate.value());
		assertThat(adminDTO.getLogin()).isEqualTo(login.value());
	}

}
