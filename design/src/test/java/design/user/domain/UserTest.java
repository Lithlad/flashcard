package design.user.domain;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.CALLS_REAL_METHODS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.util.ReflectionTestUtils.setField;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import design.domain.DomainEvent;
import design.user.domain.communication.event.UserWasDisabledEvent;
import design.user.domain.communication.event.UserWasEnabledEvent;
import design.user.domain.communication.event.UserWasLockedEvent;
import design.user.domain.communication.event.UserWasUnlockedEvent;
import design.utils.TestUtils;

@Tag("unit")
class UserTest {

	@Test
	void shouldMapToUserDetails() {
		// given
		final Login user1 = Login.create("user1");
		final int concurrencyValue = 3;
		final User user = mockUser(user1, concurrencyValue);

		setField(user, "locked", Locked.create(false));
		setField(user, "enabled", Enabled.create(true));

		// when
		final UserDetails userDetails = user.mapToUserDetails();

		// then
		assertThat(userDetails.isAccountNonLocked()).isTrue();
		assertThat(userDetails.isEnabled()).isTrue();
		final List<SimpleGrantedAuthority> authorities = (List<SimpleGrantedAuthority>)userDetails.getAuthorities();
		assertThat(authorities).containsExactly(new SimpleGrantedAuthority("ROLE"));
	}

	@Test
	void shouldLockUser() {
		// given
		final Login user1 = Login.create("user1");
		final int concurrencyValue = 3;
		final User user = mockUser(user1, concurrencyValue);

		setField(user, "locked", Locked.create(false));

		// when
		user.lock();

		// then
		assertThat(user.mapToUserDetails().isAccountNonLocked()).isFalse();
		TestUtils.assertEventIsRegistered(user, new UserWasLockedEvent(user1, concurrencyValue));
	}

	@Test
	void shouldUnlockUser() {
		// given
		final Login user1 = Login.create("user1");
		final int concurrencyValue = 3;
		final User user = mockUser(user1, concurrencyValue);

		setField(user, "locked", Locked.create(true));

		// when
		user.unlock();

		// then
		assertThat(user.mapToUserDetails().isAccountNonLocked()).isTrue();
		TestUtils.assertEventIsRegistered(user, new UserWasUnlockedEvent(user1, concurrencyValue));
	}

	@Test
	void shouldEnableUser() {
		// given
		final Login user1 = Login.create("user1");
		final int concurrencyValue = 3;
		final User user = mockUser(user1, concurrencyValue);

		setField(user, "enabled", Enabled.create(false));

		// when
		user.enable();

		// then
		assertThat(user.mapToUserDetails().isEnabled()).isTrue();
		TestUtils.assertEventIsRegistered(user, new UserWasEnabledEvent(user1, concurrencyValue));
	}

	@Test
	void shouldDisableUser() {
		// given
		final Login user1 = Login.create("user1");
		final int concurrencyValue = 3;
		final User user = mockUser(user1, concurrencyValue);

		setField(user, "enabled", Enabled.create(true));

		// when
		user.disable();

		// then
		assertThat(user.mapToUserDetails().isEnabled()).isFalse();
		TestUtils.assertEventIsRegistered(user, new UserWasDisabledEvent(user1, concurrencyValue));
	}

	private User mockUser(final Login login, final int concurrencyValue) {
		final User user = mock(User.class, CALLS_REAL_METHODS);
		when(user.getRoles()).thenReturn(List.of("ROLE"));
		setField(user, "login", login);
		setField(user, "concurrencyVersion", concurrencyValue);
		setField(user, "events", new ArrayList<DomainEvent>());
		return user;
	}
}
