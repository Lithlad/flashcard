package design.user.domain;

import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import design.user.domain.exceptions.EmailIsInvalidException;

@Tag("unit")
class EmailTest {

	@Test
	void shouldCreateEmail() {
		// given
		final String emailValue = "mail@lithlad.com";

		// when
		final Email email = Email.create(emailValue);

		// then
		assertThat(email.value()).isEqualTo(emailValue);
	}

	@Test
	void shouldThrowExceptionIfEmailIsInvalid() {
		// given
		final String emailValue = "mail#lithlad.com";

		// when
		final EmailIsInvalidException exception = assertThrows(EmailIsInvalidException.class, () -> Email.create(emailValue));

		// then
		assertThat(exception).hasMessage(format("Email %s is invalid", emailValue));
	}

}
