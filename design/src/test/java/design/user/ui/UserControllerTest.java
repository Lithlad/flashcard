package design.user.ui;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.springframework.http.HttpStatus.OK;

import java.util.UUID;

import javax.validation.constraints.NotBlank;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MvcResult;

import design.utils.AbstractControllerTest;
import design.security.infrastructure.UserService;
import design.user.application.command.UserApplicationService;
import design.user.application.query.UserViewQueryService;
import design.user.ui.form.DisableUserForm;
import design.user.ui.form.EnableUserForm;
import design.user.ui.form.LockUserForm;
import design.user.ui.form.UnlockUserForm;

@Tag("it")
@WebMvcTest(UserController.class)
@TestPropertySource(locations="classpath:application-test.properties")
class UserControllerTest extends AbstractControllerTest {

	private static final String CONTROLLER_PREFIX = "/api/user";

	@MockBean
	private UserApplicationService userApplicationService;

	@MockBean
	private UserService userService;

	@MockBean
	private UserViewQueryService queryService;

	@WithMockUser(username="admin1")
	@Test
	void shouldLockUser() throws Exception {
		// given
		@NotBlank final String login = UUID.randomUUID().toString();
		final int concurrencyVersion = 3;
		final LockUserForm lock = new LockUserForm(login, concurrencyVersion);

		// when
		final MvcResult result = performLoggedPost(CONTROLLER_PREFIX + "/lock", lock);

		// then
		verify(userApplicationService).lock(lock);
		assertThat(result.getResponse().getStatus()).isEqualTo(OK.value());
	}

	@WithMockUser(username="admin1")
	@Test
	void shouldUnlockUser() throws Exception {
		// given
		@NotBlank final String login = UUID.randomUUID().toString();
		final int concurrencyVersion = 3;
		final UnlockUserForm unlock = new UnlockUserForm(login, concurrencyVersion);

		// when
		final MvcResult result = performLoggedPost(CONTROLLER_PREFIX + "/unlock", unlock);

		// then
		verify(userApplicationService).unlock(unlock);
		assertThat(result.getResponse().getStatus()).isEqualTo(OK.value());
	}

	@WithMockUser(username="admin1")
	@Test
	void shouldEnableUser() throws Exception {
		// given
		@NotBlank final String login = UUID.randomUUID().toString();
		final int concurrencyVersion = 3;
		final EnableUserForm form = new EnableUserForm(login, concurrencyVersion);

		// when
		final MvcResult result = performLoggedPost(CONTROLLER_PREFIX + "/enable", form);

		// then
		verify(userApplicationService).enable(form);
		assertThat(result.getResponse().getStatus()).isEqualTo(OK.value());
	}

	@WithMockUser(username="admin1")
	@Test
	void shouldDisableUser() throws Exception {
		// given
		@NotBlank final String login = UUID.randomUUID().toString();
		final int concurrencyVersion = 3;
		final DisableUserForm form = new DisableUserForm(login, concurrencyVersion);

		// when
		final MvcResult result = performLoggedPost(CONTROLLER_PREFIX + "/disable", form);

		// then
		verify(userApplicationService).disable(form);
		assertThat(result.getResponse().getStatus()).isEqualTo(OK.value());
	}
}
