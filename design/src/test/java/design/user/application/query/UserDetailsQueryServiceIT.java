package design.user.application.query;

import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.test.context.support.WithMockUser;

import design.utils.FullApplicationContextAbstractIT;
import design.user.application.query.exception.LoggedUserIsOfUnrecognizedTypeException;
import design.user.application.query.exception.NoUserIsLoggedException;
import design.user.domain.Login;
import design.user.domain.User;

@Tag("it")
public class UserDetailsQueryServiceIT extends FullApplicationContextAbstractIT {

	@Autowired
	private UserViewQueryService userViewQueryService;

	@MockBean
	SecurityContext securityContext;

	@Test
	@WithMockUser(username="learner1")
	void shouldReturnUserLogin() {
		// given
		final String username = "learner1";

		// when
		final Login login = userViewQueryService.currentUserLogin();

		// then
		assertThat(login).isEqualTo(Login.create(username));
	}

	@Test
	@Disabled
	void shouldThrowExceptionIfCanNotCastToUser() {
		// given
		final Object user = mock(User.class);
		final Authentication authentication = mock(Authentication.class);
		when(authentication.getPrincipal()).thenReturn(user);
		when(securityContext.getAuthentication()).thenReturn(authentication);

		// when
		final LoggedUserIsOfUnrecognizedTypeException exception = assertThrows(
			LoggedUserIsOfUnrecognizedTypeException.class, () -> userViewQueryService.currentUserLogin());

		// then
		assertThat(exception).hasMessage(format("Logged user type is not recognized - %s", user.toString()));
	}

	@Test
	void shouldThrowExceptionIfNoUserIsCurrentlyLogged() {
		// given

		// when
		final NoUserIsLoggedException exception =
			assertThrows(NoUserIsLoggedException.class, () -> userViewQueryService.currentUserLogin());

		// then
		assertThat(exception).hasMessage("No user is currently logged");
	}
}
