package design.user.application.query;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.UUID;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.userdetails.UserDetails;

import design.user.application.query.UserDetailsService;
import design.user.domain.User;
import design.user.domain.UserDetailsRepository;

@Tag("unit")
class UserDetailsServiceTest {

	private final UserDetailsRepository userDetailsRepository = mock(UserDetailsRepository.class);

	private final UserDetailsService userDetailsService = new UserDetailsService(userDetailsRepository);

	@Test
	void shouldReturnUserDetails() {
		// given
		final String username = UUID.randomUUID().toString();
		final User user = mock(User.class);
		when(user.mapToUserDetails()).thenReturn(mock(UserDetails.class));
		when(userDetailsRepository.loadByUsername(username)).thenReturn(mock(UserDetails.class));

		// when
		userDetailsService.loadByUsername(username);

		// then
		verify(userDetailsRepository).loadByUsername(username);
	}
}
