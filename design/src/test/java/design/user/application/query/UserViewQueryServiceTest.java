package design.user.application.query;

import static java.lang.String.format;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import design.user.application.query.UserViewInstanceQueryService;
import design.user.application.query.UserViewQueryService;
import design.user.domain.dto.UserDTO;
import design.user.domain.Login;
import design.user.domain.User;
import design.user.domain.exceptions.UserDoesNotExistException;

@Tag("unit")
class UserViewQueryServiceTest {

	private UserViewInstanceQueryService service1 = mock(UserViewInstanceQueryService.class);
	private UserViewInstanceQueryService service2 = mock(UserViewInstanceQueryService.class);
	private List<UserViewInstanceQueryService> services = Arrays.asList(service1, service2);
	private final UserViewQueryService userViewQueryService = new UserViewQueryService(services);

	@Test
	void shouldGetByLogin() {
		// given
		final Login login = Login.create("user1");
		when(service1.findByLogin(login)).thenReturn(empty());
		final User user = mock(User.class);
		final UserDTO userDTO = mock(UserDTO.class);
		when(user.mapToDTO()).thenReturn(userDTO);
		doReturn(of(user)).when(service2).findByLogin(login);

		// when
		final UserDTO userDTOResult = userViewQueryService.find(login.value());

		// then
		verify(service1).findByLogin(login);
		verify(service2).findByLogin(login);
		assertThat(userDTOResult).isEqualTo(userDTO);
	}

	@Test
	void shouldStopSearchingAfterFirstFound() {
		// given
		final Login login = Login.create("user1");
		final User user = mock(User.class);
		final UserDTO userDTO = mock(UserDTO.class);
		when(user.mapToDTO()).thenReturn(userDTO);
		doReturn(of(user)).when(service1).findByLogin(login);

		// when
		final UserDTO userDTOResult = userViewQueryService.find(login.value());

		// then
		verify(service1).findByLogin(login);
		verify(service2, never()).findByLogin(login);
		assertThat(userDTOResult).isEqualTo(userDTO);
	}

	@Test
	void shouldThrowExceptionIfUserDoesNotExist() {
		// given
		final Login login = Login.create("user1");
		when(service1.findByLogin(login)).thenReturn(empty());
		when(service2.findByLogin(login)).thenReturn(empty());

		// when
		final UserDoesNotExistException exception =
			assertThrows(UserDoesNotExistException.class, () -> userViewQueryService.find(login.value()));

		// then
		verify(service1).findByLogin(login);
		verify(service2).findByLogin(login);
		assertThat(exception).hasMessage(format("User %s does not exist", login.value()));
	}
}
