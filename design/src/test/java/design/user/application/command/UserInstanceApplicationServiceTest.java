package design.user.application.command;

import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.CALLS_REAL_METHODS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.withSettings;

import java.util.UUID;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import design.user.application.command.UserInstanceApplicationService;
import design.user.application.command.commands.DisableUserCommand;
import design.user.application.command.commands.EnableUserCommand;
import design.user.application.command.commands.LockUserCommand;
import design.user.application.command.commands.RegisterUserCommand;
import design.user.application.command.commands.UnlockUserCommand;
import design.user.domain.Login;
import design.user.domain.User;
import design.user.domain.UserRepository;
import design.user.domain.exceptions.UserAlreadyExistException;

@Tag("unit")
class UserInstanceApplicationServiceTest {

	private final UserRepository userRepository = mock(UserRepository.class);

	private final UserInstanceApplicationService userApplicationService =
		mock(UserInstanceApplicationService.class, withSettings()
			.useConstructor(userRepository)
			.defaultAnswer(CALLS_REAL_METHODS));

	@Test
	void shouldCreateUser() {
		// given
		final RegisterUserCommand registerUserCommand = mock(RegisterUserCommand.class);
		final String login = UUID.randomUUID().toString();
		final String pass = UUID.randomUUID().toString();
		final String email = UUID.randomUUID().toString();
		when(registerUserCommand.getLogin()).thenReturn(login);
		when(registerUserCommand.getPass()).thenReturn(pass);
		when(registerUserCommand.getEmail()).thenReturn(email);
		when(userRepository.doesExist(Login.create(login))).thenReturn(false);

		final User user = mock(User.class);
		when(userApplicationService.createUserInstance(registerUserCommand)).thenReturn(user);

		// when
		userApplicationService.register(registerUserCommand);

		// then
		verify(userRepository).doesExist(Login.create(login));
		verify(userApplicationService).createUserInstance(registerUserCommand);
		verify(userApplicationService).saveUser(user);
	}

	@Test
	void shouldThrowExceptionIfUserAlreadyExists() {
		// given
		final RegisterUserCommand registerUserCommand = mock(RegisterUserCommand.class);
		final String login = UUID.randomUUID().toString();
		when(registerUserCommand.getLogin()).thenReturn(login);
		when(userRepository.doesExist(Login.create(login))).thenReturn(true);

		// when
		final UserAlreadyExistException exception =
			assertThrows(UserAlreadyExistException.class, () -> userApplicationService.register(registerUserCommand));

		// then
		assertThat(exception).hasMessage(format("User with login %s already exists", login));
		verify(userRepository).doesExist(Login.create(login));
		verify(userApplicationService, never()).createUserInstance(registerUserCommand);
		verify(userApplicationService, never()).saveUser(any(User.class));
	}

	@Test
	void shouldLockUser() {
		// given
		final LockUserCommand command = mock(LockUserCommand.class);
		final String login = UUID.randomUUID().toString();
		final Integer concurrencyVersion = 9;
		when(command.getLogin()).thenReturn(login);
		when(command.getConcurrencyVersion()).thenReturn(concurrencyVersion);
		final User user = mock(User.class);
		when(userRepository.getByLoginAndConcurrencyVersion(Login.create(login), concurrencyVersion)).thenReturn(user);

		// when
		userApplicationService.lock(command);

		// then
		verify(userRepository).getByLoginAndConcurrencyVersion(Login.create(login), concurrencyVersion);
		verify(user).lock();
		verify(userApplicationService).saveUser(user);
	}

	@Test
	void shouldUnlockUser() {
		// given
		final UnlockUserCommand command = mock(UnlockUserCommand.class);
		final String login = UUID.randomUUID().toString();
		final Integer concurrencyVersion = 9;
		when(command.getLogin()).thenReturn(login);
		when(command.getConcurrencyVersion()).thenReturn(concurrencyVersion);
		final User user = mock(User.class);
		when(userRepository.getByLoginAndConcurrencyVersion(Login.create(login), concurrencyVersion)).thenReturn(user);

		// when
		userApplicationService.unlock(command);

		// then
		verify(userRepository).getByLoginAndConcurrencyVersion(Login.create(login), concurrencyVersion);
		verify(user).unlock();
		verify(userApplicationService).saveUser(user);
	}

	@Test
	void shouldEnableUser() {
		// given
		final EnableUserCommand command = mock(EnableUserCommand.class);
		final String login = UUID.randomUUID().toString();
		final Integer concurrencyVersion = 9;
		when(command.getLogin()).thenReturn(login);
		when(command.getConcurrencyVersion()).thenReturn(concurrencyVersion);
		final User user = mock(User.class);
		when(userRepository.getByLoginAndConcurrencyVersion(Login.create(login), concurrencyVersion)).thenReturn(user);

		// when
		userApplicationService.enable(command);

		// then
		verify(userRepository).getByLoginAndConcurrencyVersion(Login.create(login), concurrencyVersion);
		verify(user).enable();
		verify(userApplicationService).saveUser(user);
	}

	@Test
	void shouldDisableUser() {
		// given
		final DisableUserCommand command = mock(DisableUserCommand.class);
		final String login = UUID.randomUUID().toString();
		final Integer concurrencyVersion = 9;
		when(command.getLogin()).thenReturn(login);
		when(command.getConcurrencyVersion()).thenReturn(concurrencyVersion);
		final User user = mock(User.class);
		when(userRepository.getByLoginAndConcurrencyVersion(Login.create(login), concurrencyVersion)).thenReturn(user);

		// when
		userApplicationService.disable(command);

		// then
		verify(userRepository).getByLoginAndConcurrencyVersion(Login.create(login), concurrencyVersion);
		verify(user).disable();
		verify(userApplicationService).saveUser(user);
	}
}
