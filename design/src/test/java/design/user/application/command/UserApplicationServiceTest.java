package design.user.application.command;

import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import design.user.application.command.UserApplicationService;
import design.user.application.command.UserInstanceApplicationService;
import design.user.application.command.commands.DisableUserCommand;
import design.user.application.command.commands.EnableUserCommand;
import design.user.application.command.commands.LockUserCommand;
import design.user.application.command.commands.UnlockUserCommand;
import design.user.domain.Login;
import design.user.domain.exceptions.UserDoesNotExistException;

@Tag("unit")
class UserApplicationServiceTest {

	private final UserInstanceApplicationService service1 = mock(UserInstanceApplicationService.class);
	private final UserInstanceApplicationService service2 = mock(UserInstanceApplicationService.class);
	private final List<UserInstanceApplicationService> userServices = List.of(service1, service2);
	private final UserApplicationService userApplicationService = new UserApplicationService(userServices);

	@Test
	void shouldLockUser() {
		// given
		final LockUserCommand command = mock(LockUserCommand.class);
		final String login = UUID.randomUUID().toString();
		final Login loginVO = Login.create(login);
		when(command.getLogin()).thenReturn(login);
		when(service1.isAppropriate(loginVO)).thenReturn(false);
		when(service2.isAppropriate(loginVO)).thenReturn(true);

		// when
		userApplicationService.lock(command);

		// then
		verify(service1, never()).lock(command);
		verify(service2).lock(command);
	}

	@Test
	void shouldThrowExceptionIfThereIsNoAppropriateServiceDuringUserLocking() {
		// given
		final LockUserCommand command = mock(LockUserCommand.class);
		final String login = UUID.randomUUID().toString();
		final Login loginVO = Login.create(login);
		when(command.getLogin()).thenReturn(login);
		when(service1.isAppropriate(loginVO)).thenReturn(false);
		when(service2.isAppropriate(loginVO)).thenReturn(false);

		// when
		final UserDoesNotExistException exception = assertThrows(UserDoesNotExistException.class, () -> userApplicationService.lock(command));

		// then
		verify(service1, never()).lock(command);
		verify(service2, never()).lock(command);
		assertThat(exception).hasMessage(format("User %s does not exist", login));
	}

	@Test
	void shouldUnlockUser() {
		// given
		final UnlockUserCommand command = mock(UnlockUserCommand.class);
		final String login = UUID.randomUUID().toString();
		final Login loginVO = Login.create(login);
		when(command.getLogin()).thenReturn(login);
		when(service1.isAppropriate(loginVO)).thenReturn(false);
		when(service2.isAppropriate(loginVO)).thenReturn(true);

		// when
		userApplicationService.unlock(command);

		// then
		verify(service1, never()).unlock(command);
		verify(service2).unlock(command);
	}

	@Test
	void shouldThrowExceptionIfThereIsNoAppropriateServiceDuringUserUnlocking() {
		// given
		final UnlockUserCommand command = mock(UnlockUserCommand.class);
		final String login = UUID.randomUUID().toString();
		final Login loginVO = Login.create(login);
		when(command.getLogin()).thenReturn(login);
		when(service1.isAppropriate(loginVO)).thenReturn(false);
		when(service2.isAppropriate(loginVO)).thenReturn(false);

		// when
		final UserDoesNotExistException exception = assertThrows(UserDoesNotExistException.class, () -> userApplicationService.unlock(command));

		// then
		verify(service1, never()).unlock(command);
		verify(service2, never()).unlock(command);
		assertThat(exception).hasMessage(format("User %s does not exist", login));
	}

	@Test
	void shouldDisableUser() {
		// given
		final DisableUserCommand command = mock(DisableUserCommand.class);
		final String login = UUID.randomUUID().toString();
		final Login loginVO = Login.create(login);
		when(command.getLogin()).thenReturn(login);
		when(service1.isAppropriate(loginVO)).thenReturn(false);
		when(service2.isAppropriate(loginVO)).thenReturn(true);

		// when
		userApplicationService.disable(command);

		// then
		verify(service1, never()).disable(command);
		verify(service2).disable(command);
	}

	@Test
	void shouldThrowExceptionIfThereIsNoAppropriateServiceDuringUserDisabling() {
		// given
		final DisableUserCommand command = mock(DisableUserCommand.class);
		final String login = UUID.randomUUID().toString();
		final Login loginVO = Login.create(login);
		when(command.getLogin()).thenReturn(login);
		when(service1.isAppropriate(loginVO)).thenReturn(false);
		when(service2.isAppropriate(loginVO)).thenReturn(false);

		// when
		final UserDoesNotExistException exception = assertThrows(UserDoesNotExistException.class, () -> userApplicationService.disable(command));

		// then
		verify(service1, never()).disable(command);
		verify(service2, never()).disable(command);
		assertThat(exception).hasMessage(format("User %s does not exist", login));
	}

	@Test
	void shouldEnableUser() {
		// given
		final EnableUserCommand command = mock(EnableUserCommand.class);
		final String login = UUID.randomUUID().toString();
		final Login loginVO = Login.create(login);
		when(command.getLogin()).thenReturn(login);
		when(service1.isAppropriate(loginVO)).thenReturn(false);
		when(service2.isAppropriate(loginVO)).thenReturn(true);

		// when
		userApplicationService.enable(command);

		// then
		verify(service1, never()).enable(command);
		verify(service2).enable(command);
	}

	@Test
	void shouldThrowExceptionIfThereIsNoAppropriateServiceDuringUserEnabling() {
		// given
		final EnableUserCommand command = mock(EnableUserCommand.class);
		final String login = UUID.randomUUID().toString();
		final Login loginVO = Login.create(login);
		when(command.getLogin()).thenReturn(login);
		when(service1.isAppropriate(loginVO)).thenReturn(false);
		when(service2.isAppropriate(loginVO)).thenReturn(false);

		// when
		final UserDoesNotExistException exception = assertThrows(UserDoesNotExistException.class, () -> userApplicationService.enable(command));

		// then
		verify(service1, never()).enable(command);
		verify(service2, never()).enable(command);
		assertThat(exception).hasMessage(format("User %s does not exist", login));
	}
}
