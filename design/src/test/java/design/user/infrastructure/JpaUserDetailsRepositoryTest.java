package design.user.infrastructure;

import static java.lang.String.format;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.userdetails.UserDetails;

import design.user.domain.Login;
import design.user.domain.User;
import design.user.domain.UserInstanceRepository;
import design.user.domain.exceptions.UserDoesNotExistException;
import design.user.infrastructure.JpaUserDetailsRepository;
import lombok.NonNull;

@Tag("unit")
class JpaUserDetailsRepositoryTest {

	private final UserInstanceRepository repository1 = mock(UserInstanceRepository.class);
	private final UserInstanceRepository repository2 = mock(UserInstanceRepository.class);
	private final List<UserInstanceRepository> repositoriesList = List.of(repository1, repository2);

	private final JpaUserDetailsRepository jpaUserDetailsRepository = new JpaUserDetailsRepository(repositoriesList);

	@Test
	void shouldGetByLogin() {
		// given
		final Login login = Login.create("user1");
		when(repository1.findByLogin(login)).thenReturn(empty());
		final User user = mock(User.class);
		doReturn(of(user)).when(repository2).findByLogin(login);
		final UserDetails expectedUserDetails = mock(UserDetails.class);
		when(user.mapToUserDetails()).thenReturn(expectedUserDetails);

		// when
		final UserDetails obtainedUserDetails = jpaUserDetailsRepository.loadByUsername(login.value());

		// then
		verify(repository1).findByLogin(login);
		verify(repository2).findByLogin(login);
		assertThat(obtainedUserDetails).isEqualTo(expectedUserDetails);
	}

	@Test
	void shouldStopSearchingAfterRepositoryReturnsFirstUser() {
		// given
		final User user = mock(User.class);
		final @NonNull Login login = Login.create("user1");
		doReturn(of(user)).when(repository1).findByLogin(login);
		when(repository2.findByLogin(login)).thenReturn(empty());
		final UserDetails expectedUserDetails = mock(UserDetails.class);
		when(user.mapToUserDetails()).thenReturn(expectedUserDetails);

		// when
		final UserDetails obtainedUserDetails = jpaUserDetailsRepository.loadByUsername(login.value());

		// then
		verify(repository1).findByLogin(login);
		verify(repository2, never()).findByLogin(login);
		assertThat(obtainedUserDetails).isEqualTo(expectedUserDetails);
	}

	@Test
	void shouldThrowExceptionIfNoUserWasFound() {
		// given
		final User user = mock(User.class);
		final @NonNull Login login = Login.create("user1");
		when(repository1.findByLogin(login)).thenReturn(empty());
		when(repository2.findByLogin(login)).thenReturn(empty());
		final UserDetails expectedUserDetails = mock(UserDetails.class);
		when(user.mapToUserDetails()).thenReturn(expectedUserDetails);

		// when
		final UserDoesNotExistException exception = assertThrows(UserDoesNotExistException.class, () -> jpaUserDetailsRepository.loadByUsername(login.value()));

		// then
		verify(repository1).findByLogin(login);
		verify(repository2).findByLogin(login);
		assertThat(exception).hasMessage(format("User %s does not exist", login.value()));
	}
}
