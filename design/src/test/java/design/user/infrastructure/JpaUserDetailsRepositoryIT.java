package design.user.infrastructure;

import static java.util.Optional.of;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.TestPropertySource;

import configurations.DataJpaTestConfiguration;
import design.admin.domain.Admin;
import design.admin.infrastructure.JpaAdminUserInstanceRepository;
import design.application.query.AuthenticationInfo;
import design.user.domain.Login;
import lombok.NonNull;

@Tag("it")
// Additional info is required, because JpaUserDetailsRepository is not JPA class
@DataJpaTest(includeFilters = @ComponentScan.Filter(type = FilterType.ANNOTATION, classes = Repository.class))
@Import(DataJpaTestConfiguration.class)
@TestPropertySource(locations="classpath:application-test.properties")
class JpaUserDetailsRepositoryIT {

	@MockBean
	private JpaAdminUserInstanceRepository adminRepository;

	@MockBean
	private AuthenticationInfo authenticationInfo;

	@Autowired
	private JpaUserDetailsRepository jpaUserDetailsRepository;

	@Test
	void shouldGetAdmin() {
		// given
		final @NonNull Login login = Login.create("admin");
//		when(learnerRepository.findByLogin(login)).thenReturn(empty());
		final Admin admin = mock(Admin.class);
		final UserDetails userDetails = mockUserDetails(login);
		when(admin.mapToUserDetails()).thenReturn(userDetails);
		doReturn(of(admin)).when(adminRepository).findByLogin(login);

		// when
		final UserDetails userDetailsResult = jpaUserDetailsRepository.loadByUsername(login.value());

		//then
		assertThat(userDetailsResult.getUsername()).isEqualTo(login.value());
	}

	private UserDetails mockUserDetails(final Login login) {
		final UserDetails userDetails = mock(UserDetails.class);
		when(userDetails.getUsername()).thenReturn(login.value());
		return userDetails;
	}
}
