package design.user.infrastructure;

import static java.lang.String.format;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import design.user.domain.Login;
import design.user.domain.User;
import design.user.domain.UserInstanceRepository;
import design.user.domain.dto.UserDTO;
import design.user.domain.exceptions.NoSuitableRepositoryForUserException;
import design.user.domain.exceptions.UserDoesNotExistException;
import design.user.domain.exceptions.UserInvalidConcurrencyVersionException;
import design.user.infrastructure.UserRepositoryImplementation;

@Tag("unit")
class UserRepositoryImplementationTest {

	private final UserInstanceRepository repository1 = mock(UserInstanceRepository.class);
	private final UserInstanceRepository repository2 = mock(UserInstanceRepository.class);
	private final List<UserInstanceRepository> userInstanceRepositories = List.of(repository1, repository2);

	private final UserRepositoryImplementation userRepositoryImplementation = new UserRepositoryImplementation(userInstanceRepositories);

	@Test
	void shouldGetByLoginAndConcurrencyVersion() {
		// given
		final Login login = mock(Login.class);
		final Integer concurrencyVersion = 4;
		when(repository1.findByLogin(login)).thenReturn(empty());
		final User user = mock(User.class);
		doReturn(of(user)).when(repository2).findByLogin(login);
		when(user.getConcurrencyVersion()).thenReturn(concurrencyVersion);

		// when
		final User result = userRepositoryImplementation.getByLoginAndConcurrencyVersion(login, concurrencyVersion);

		// then
		verify(user).getConcurrencyVersion();
		verify(repository1).findByLogin(login);
		verify(repository2).findByLogin(login);
		assertThat(result).isEqualTo(user);
	}

	@Test
	void shouldStopSearchingAfterRepositoryReturnsFirstUser() {
		// given
		final Login login = mock(Login.class);
		final Integer concurrencyVersion = 4;
		final User user = mock(User.class);
		doReturn(of(user)).when(repository1).findByLogin(login);
		when(user.getConcurrencyVersion()).thenReturn(concurrencyVersion);

		// when
		final User result = userRepositoryImplementation.getByLoginAndConcurrencyVersion(login, concurrencyVersion);

		// then
		verify(user).getConcurrencyVersion();
		verify(repository1).findByLogin(login);
		verify(repository2, never()).findByLogin(login);
		assertThat(result).isEqualTo(user);
	}

	@Test
	void shouldThrowExceptionIfUserIsNotFound() {
		// given
		final Login login = Login.create("name");
		final Integer concurrencyVersion = 4;
		when(repository1.findByLogin(login)).thenReturn(empty());
		when(repository2.findByLogin(login)).thenReturn(empty());

		// when
		final UserDoesNotExistException exception = assertThrows(UserDoesNotExistException.class,
			() -> userRepositoryImplementation.getByLoginAndConcurrencyVersion(login, concurrencyVersion));

		// then
		verify(repository1).findByLogin(login);
		verify(repository2).findByLogin(login);
		assertThat(exception).hasMessage(format("User %s does not exist", login.value()));
	}

	@Test
	void shouldThrowExceptionIfConcurrencyVersionIsInvalid() {
		// given
		final Login login = mock(Login.class);
		final Integer concurrencyVersion = 4;
		when(repository1.findByLogin(login)).thenReturn(empty());
		final User user = mock(User.class);
		doReturn(of(user)).when(repository2).findByLogin(login);
		when(user.getConcurrencyVersion()).thenReturn(concurrencyVersion + 1);

		// when
		final UserInvalidConcurrencyVersionException exception = assertThrows(UserInvalidConcurrencyVersionException.class,
			() -> userRepositoryImplementation.getByLoginAndConcurrencyVersion(login, concurrencyVersion));

		// then
		verify(user).getConcurrencyVersion();
		verify(repository1).findByLogin(login);
		verify(repository2).findByLogin(login);
		assertThat(exception).hasMessage(format("User %s concurrency version is invalid", login.value()));
	}

	@Test
	void shouldSaveUser() {
		// given
		final UserDTO expectedUserDTO = mock(UserDTO.class);
		final User user = mock(User.class);
		when(user.mapToDTO()).thenReturn(expectedUserDTO);
		when(repository1.saveIfSuitable(user)).thenReturn(Optional.empty());
		when(repository2.saveIfSuitable(user)).thenReturn(Optional.of(user));

		// when
		final UserDTO userDTO = userRepositoryImplementation.save(user);

		// then
		verify(repository2).saveIfSuitable(user);
		verify(repository1).saveIfSuitable(user);
		assertThat(userDTO).isEqualTo(expectedUserDTO);
	}

	@Test
	void shouldBreakAfterUserWasSaved() {
		// given
		final UserDTO expectedUserDTO = mock(UserDTO.class);
		final User user = mock(User.class);
		when(user.mapToDTO()).thenReturn(expectedUserDTO);
		when(repository1.saveIfSuitable(user)).thenReturn(Optional.of(user));
		when(repository2.saveIfSuitable(user)).thenReturn(Optional.empty());

		// when
		final UserDTO userDTO = userRepositoryImplementation.save(user);

		// then
		verify(repository1).saveIfSuitable(user);
		verify(repository2, never()).saveIfSuitable(user);
		assertThat(userDTO).isEqualTo(expectedUserDTO);
	}

	@Test
	void shouldThrowExceptionIfNoRepositorySavedUser() {
		// given
		final User user = mock(User.class);
		final String userToString = UUID.randomUUID().toString();
		when(user.toString()).thenReturn(userToString);
		when(repository1.saveIfSuitable(user)).thenReturn(Optional.empty());
		when(repository2.saveIfSuitable(user)).thenReturn(Optional.empty());

		// when
		final NoSuitableRepositoryForUserException exception = assertThrows(NoSuitableRepositoryForUserException.class, () -> userRepositoryImplementation.save(user));

		// then
		verify(repository2).saveIfSuitable(user);
		verify(repository1).saveIfSuitable(user);
		assertThat(exception).hasMessage(format("User %s was not saved - no suitable repository was found", userToString));
	}

	@Test
	void shouldReturnTrueIfUserAlreadyExist() {
		// given
		final Login login = Login.create("login1");
		when(repository1.doesExist(login)).thenReturn(false);
		when(repository2.doesExist(login)).thenReturn(true);

		// when
		final boolean doesExist = userRepositoryImplementation.doesExist(login);

		// then
		assertThat(doesExist).isTrue();
		verify(repository1).doesExist(login);
		verify(repository2).doesExist(login);
	}

	@Test
	void shouldReturnFalseIfUserDoesntExist() {
		// given
		final Login login = Login.create("login1");
		when(repository1.doesExist(login)).thenReturn(false);
		when(repository2.doesExist(login)).thenReturn(false);

		// when
		final boolean doesExist = userRepositoryImplementation.doesExist(login);

		// then
		assertThat(doesExist).isFalse();
		verify(repository1).doesExist(login);
		verify(repository2).doesExist(login);
	}

	@Test
	void shouldStopSearchingAfterFirstPositiveResult() {
		// given
		final Login login = Login.create("login1");
		when(repository1.doesExist(login)).thenReturn(true);
		when(repository2.doesExist(login)).thenReturn(false);

		// when
		final boolean doesExist = userRepositoryImplementation.doesExist(login);

		// then
		assertThat(doesExist).isTrue();
		verify(repository1).doesExist(login);
		verify(repository2, never()).doesExist(any(Login.class));
	}
}
