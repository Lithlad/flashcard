package design.infrastructure.queue;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.security.test.context.support.WithMockUser;

import design.infrastructure.queue.configuration.business.ActionService;
import design.infrastructure.queue.configuration.business.ActionWasRaisedRegisteredEvent;
import design.infrastructure.queue.configuration.db.ActionRepository;
import design.infrastructure.queue.configuration.queue.ActionWasRaisedEventService;
import design.utils.QueueConsumerTest;

public class QueueMessagingIT extends QueueConsumerTest<ActionWasRaisedRegisteredEvent> {

    @Autowired
    private ActionService actionService;

    @Autowired
    private ActionRepository actionRepository;

    @Autowired
    private ActionWasRaisedEventService eventService;

    @Autowired
    private EmbeddedKafkaBroker embeddedKafkaBroker;

    private final long timeForQueueToReact = 1000;

    private final String defaultValue = "";

    @BeforeEach
    void setReceiverValue() {
        eventService.setValue(defaultValue);
        actionRepository.deleteAll();
    }

    @Test
    @WithMockUser("user1")
    //dont know how to fix test
    @Disabled
    void shouldAfterObjectPersistenceSendAndReceiveEvent() {
        // given
        final String givenValue = "newValue";
        assertThat(actionRepository.count()).isZero();
        assertThat(eventService.getValue()).isEqualTo(defaultValue);

        // when
        actionService.doActionAndPersist(givenValue);
        sleep();

        // then
        assertThat(actionRepository.count()).isOne();
        assertThat(eventService.getValue()).isEqualTo(givenValue);
    }

    @Test
    @WithMockUser("user1")
    void shouldReceiveNoEventIfObjectWasNotPersisted() {
        // given
        final String givenValue = "newValue";
        assertThat(actionRepository.count()).isZero();
        assertThat(eventService.getValue()).isEqualTo(defaultValue);

        // when
        actionService.doActionWithoutPersisting(givenValue);
        sleep();

        // then
        assertThat(actionRepository.count()).isZero();
        assertThat(eventService.getValue()).isEqualTo(defaultValue);
    }

    @Test
    @WithMockUser("user1")
    void shouldReceiveNoEventIfObjectWasPersistedButWithNotSupportedEvent() {
        // given
        final String givenValue = "newValue";
        assertThat(actionRepository.count()).isZero();
        assertThat(eventService.getValue()).isEqualTo(defaultValue);

        // when
        actionService.doActionAndPersistWithNotSupportedEvent(givenValue);
        sleep();

        // then
        assertThat(actionRepository.count()).isOne();
        assertThat(eventService.getValue()).isEqualTo(defaultValue);
    }

    // to give time for kafka to react
    private void sleep() {
        try {
            Thread.sleep(timeForQueueToReact);
        } catch (InterruptedException e) { }
    }

    @Override
    protected String testedTopicName() {
        return "action_was_raised_queue";
    }

    // TODO unneeded - remove
    @Override
    protected ActionWasRaisedRegisteredEvent createDomainEvent() {
        return null;
    }

    @Override
    protected EmbeddedKafkaBroker embeddedKafkaBroker() {
        return embeddedKafkaBroker;
    }
}
