package design.infrastructure.queue.configuration.queue;

import java.util.Collection;
import java.util.Collections;

import org.springframework.context.annotation.DependsOn;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import design.application.command.DomainEventObserver;
import design.infrastructure.queue.configuration.business.ActionWasRaisedRegisteredEvent;
import lombok.Getter;
import lombok.Setter;

@Service
@DependsOn("ActionWasRaisedConsumerQueueConfiguration")
public class ActionWasRaisedEventService extends DomainEventObserver<ActionWasRaisedRegisteredEvent> {

	@Getter
	@Setter
	private String value = "";

	@Override
	@KafkaListener(topics = ActionWasRaisedRegisteredEvent.IDENTIFIER, groupId = ActionWasRaisedConsumerQueueConfiguration.GROUP_ID, containerFactory = ActionWasRaisedConsumerQueueConfiguration.CONTAINER_FACTORY_ID)
	public void notify(final ActionWasRaisedRegisteredEvent event) {
		value = event.getValue();
	}

	@Override
	protected Collection<String> getAuthorities() {
		return Collections.emptyList();
	}

	@Override
	protected String subscriberName() {
		return "Subscriber";
	}
}
