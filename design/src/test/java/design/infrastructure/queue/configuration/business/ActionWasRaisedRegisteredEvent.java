package design.infrastructure.queue.configuration.business;

import design.domain.DomainEvent;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(exclude = { "concurrencyVersion", "eventDate" })
public class ActionWasRaisedRegisteredEvent extends DomainEvent {

    public static final String IDENTIFIER = "action_was_raised_identifier";

    public static final String DOMAIN = "action_was_raised_domain";

    private String value;

    public ActionWasRaisedRegisteredEvent(final int concurrencyVersion, final String value) {
        super(concurrencyVersion);
        this.value = value;
    }

    @Override
    public String eventIdentifier() {
        return IDENTIFIER;
    }

    @Override
    public String eventDomain() {
        return DOMAIN;
    }
}
