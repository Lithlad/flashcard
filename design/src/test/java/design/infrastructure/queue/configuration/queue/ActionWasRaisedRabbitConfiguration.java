package design.infrastructure.queue.configuration.queue;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import design.infrastructure.queue.configuration.business.ActionWasRaisedRegisteredEvent;
import design.infrastructure.queue.configuration.QueueConfiguration;

@Configuration
public class ActionWasRaisedRabbitConfiguration extends QueueConfiguration {

    @Bean(name = "actionWasRaisedTopic")
    public NewTopic actionWasRaisedTopic() {
        return generateTopic(ActionWasRaisedRegisteredEvent.IDENTIFIER);
    }
}
