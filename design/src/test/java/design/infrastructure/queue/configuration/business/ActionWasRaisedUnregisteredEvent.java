package design.infrastructure.queue.configuration.business;

import design.domain.DomainEvent;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode(exclude = { "concurrencyVersion", "eventDate" })
public class ActionWasRaisedUnregisteredEvent extends DomainEvent {

    public static final String IDENTIFIER = "action_was_raised_identifier";

    public static final String DOMAIN = "action_was_raised_domain";

    private final String value;

    public ActionWasRaisedUnregisteredEvent(final int concurrencyVersion, final String value) {
        super(concurrencyVersion);
        this.value = value;
    }

    @Override
    public String eventIdentifier() {
        return IDENTIFIER;
    }

    @Override
    public String eventDomain() {
        return DOMAIN;
    }
}
