package design.infrastructure.queue.configuration.business;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import design.domain.Aggregate;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "aggregate")
@NoArgsConstructor
public class ActionAggregate extends Aggregate {

    @Column
    private String value;

    public void changeStateAndRaiseRegisteredEvent(final String value) {
        this.value = value;
        registerEvent(new ActionWasRaisedRegisteredEvent(3, value));
    }

    public void changeStateAndRaiseUnregisteredEvent(final String value) {
        this.value = value;
        registerEvent(new ActionWasRaisedUnregisteredEvent(3, value));
    }
}
