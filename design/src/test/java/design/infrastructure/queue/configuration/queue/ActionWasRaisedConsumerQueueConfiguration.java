package design.infrastructure.queue.configuration.queue;

import java.util.Collections;
import java.util.Set;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;

import design.infrastructure.queue.configuration.ConsumerQueueConfiguration;
import design.infrastructure.queue.configuration.business.ActionWasRaisedRegisteredEvent;

@Configuration("ActionWasRaisedConsumerQueueConfiguration")
public class ActionWasRaisedConsumerQueueConfiguration extends ConsumerQueueConfiguration<ActionWasRaisedRegisteredEvent> {

	public final static String GROUP_ID = "action-group";

	public final static String CONTAINER_FACTORY_ID = "actionWasRaisedListenerContainerFactory";

	private final String bootstrapAddress;

	public ActionWasRaisedConsumerQueueConfiguration(@Value("${kafka.bootstrapAddress}") final String bootstrapAddress) {
		this.bootstrapAddress = bootstrapAddress;
	}

	@Bean("actionWasRaisedConsumerFactory")
	@Override
	public ConsumerFactory<String, ActionWasRaisedRegisteredEvent> consumerFactoryBean() {
		return consumerFactory(bootstrapAddress);
	}

	@Bean("actionWasRaisedListenerContainerFactory")
	@Override
	public ConcurrentKafkaListenerContainerFactory<String, ActionWasRaisedRegisteredEvent> listenerContainerFactoryBean(
		@Qualifier("actionWasRaisedConsumerFactory") final ConsumerFactory<String, ActionWasRaisedRegisteredEvent> consumerFactory) {
		return listenerContainerFactory(consumerFactory);
	}

	@Override
	protected Set<String> trustedPackages() {
		return Collections.singleton("lithlad.flashcards.design.infrastructure.queue.configuration.business");
	}

	@Override
	protected String groupId() {
		return GROUP_ID;
	}
}
