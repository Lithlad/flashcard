package design.infrastructure.queue.configuration.db;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import design.infrastructure.queue.configuration.business.ActionAggregate;

@Repository
public interface ActionRepository extends JpaRepository<ActionAggregate, Long> { }
