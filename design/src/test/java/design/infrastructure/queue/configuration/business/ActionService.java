package design.infrastructure.queue.configuration.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionalEventListener;

import design.application.command.ApplicationService;
import design.domain.DomainEvent;
import design.infrastructure.queue.configuration.db.ActionRepository;
import lombok.NonNull;

@Service
public class ActionService extends ApplicationService {

    @Autowired
    private ActionRepository actionRepository;

    @Transactional
    public void doActionAndPersist(final String value) {
        final ActionAggregate actionAggregate = new ActionAggregate();
        actionAggregate.changeStateAndRaiseRegisteredEvent(value);
        actionRepository.save(actionAggregate);
    }

    @Transactional
    public void doActionWithoutPersisting(final String value) {
        final ActionAggregate actionAggregate = new ActionAggregate();
        actionAggregate.changeStateAndRaiseRegisteredEvent(value);
    }

    @Transactional
    public void doActionAndPersistWithNotSupportedEvent(final String value) {
        final ActionAggregate actionAggregate = new ActionAggregate();
        actionAggregate.changeStateAndRaiseUnregisteredEvent(value);
        actionRepository.save(actionAggregate);
    }

    @Override
    @TransactionalEventListener(value = ActionWasRaisedRegisteredEvent.class)
    protected void actionWasRaised(final @NonNull DomainEvent event) {
        superActionWasRaised(event);
    }
}
