package design.infrastructure;

import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.Property;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.layout.PatternLayout;
import org.apache.logging.log4j.message.Message;

@Plugin(name="TestAppender", category="Core", elementType="appender", printObject=true)
public class TestAppender extends AbstractAppender {

	private static final Set<String> events = new LinkedHashSet<>();

	protected TestAppender(String name,
	                       Filter filter,
	                       Layout<? extends Serializable> layout,
	                       boolean ignoreExceptions) {
		super(name, filter, layout, ignoreExceptions, new Property[0]);
	}

	@Override
	public void append(final LogEvent logEvent) {
		final Message message = logEvent.getMessage();
		events.add(message.getFormattedMessage());
	}


	@PluginFactory
	public static TestAppender createAppender(
		@PluginAttribute("name") String name,
		@PluginElement("Layout") Layout<? extends Serializable> layout,
		@PluginElement("Filter") Filter filter) {
		if (name == null) {
			LOGGER.error("No name provided for TestLoggerAppender");
			return null;
		}

		if (layout == null) layout = PatternLayout.createDefaultLayout();

		return new TestAppender(name, filter, layout, true);
	}

	public static void clear() {
		events.clear();
	}

	public static Set<String> getEvents() {
		return Collections.unmodifiableSet(events);
	}
}
