package design.infrastructure;

import static design.infrastructure.CustomExceptionHandler.INTERNAL_SYSTEM_EXCEPTION_ERROR_CODE;
import static design.infrastructure.CustomExceptionHandler.OPTIMISTIC_LOCK_EXCEPTION_ERROR_CODE;
import static design.infrastructure.CustomExceptionHandler.PERSISTENCE_EXCEPTION_ERROR_CODE;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.util.Set;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.google.gson.Gson;

import design.utils.FullApplicationContextAbstractIT;
import design.domain.exceptions.DomainAlreadyExistsException;
import design.domain.exceptions.DomainException;
import design.domain.exceptions.DomainInvalidConcurrencyVersionException;
import design.domain.exceptions.DomainNotFoundException;
import design.domain.exceptions.DomainRequirementsNotFulfilledException;
import design.domain.exceptions.DomainUnauthorizedException;
import lombok.NonNull;

@AutoConfigureMockMvc
@Import(ExceptionHandlerTestController.class)
class CustomExceptionHandlerIT extends FullApplicationContextAbstractIT {

	@Autowired
	private MockMvc mvc;

	private final static String CONTROLLER_PREFIX = "/test";

	@BeforeEach
	void cleanLog() {
		TestAppender.clear();
	}

	@WithMockUser(username="user")
	@Test
	void shouldHandleDomainAlreadyExistsException() throws Exception {
		// given
		final String message = UUID.randomUUID().toString();
		final String errorCode = UUID.randomUUID().toString();

		// when
		final MvcResult result = performGet(CONTROLLER_PREFIX + "/throwDomainAlreadyExistsException/" + message + "/" + errorCode);

		// then
		assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.PRECONDITION_FAILED.value());
		assertResponse(message, errorCode, result, DomainAlreadyExistsException.class);
		assertThrownException(message, errorCode, result, DomainAlreadyExistsException.class);
		assertThat(TestAppender.getEvents()).contains("design.domain.exceptions.DomainAlreadyExistsException: " + message);
	}

	@WithMockUser(username="user")
	@Test
	void shouldHandleDomainInvalidConcurrencyVersionException() throws Exception {
		// given
		final String message = UUID.randomUUID().toString();
		final String errorCode = UUID.randomUUID().toString();

		// when
		final MvcResult result = performGet(CONTROLLER_PREFIX + "/throwDomainInvalidConcurrencyVersionException/" + message + "/" + errorCode);

		// then
		assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.UNAUTHORIZED.value());
		assertResponse(message, errorCode, result, DomainInvalidConcurrencyVersionException.class);
		assertThrownException(message, errorCode, result, DomainInvalidConcurrencyVersionException.class);
		assertThat(TestAppender.getEvents()).contains("design.domain.exceptions.DomainInvalidConcurrencyVersionException: " + message);
	}

	@WithMockUser(username="user")
	@Test
	void shouldHandleDomainNotFoundException() throws Exception {
		// given
		final String message = UUID.randomUUID().toString();
		final String errorCode = UUID.randomUUID().toString();

		// when
		final MvcResult result = performGet(CONTROLLER_PREFIX + "/throwDomainNotFoundException/" + message + "/" + errorCode);

		// then
		assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
		assertResponse(message, errorCode, result, DomainNotFoundException.class);
		assertThrownException(message, errorCode, result, DomainNotFoundException.class);
		assertThat(TestAppender.getEvents()).contains("design.domain.exceptions.DomainNotFoundException: " + message);
	}

	@WithMockUser(username="user")
	@Test
	void shouldHandleDomainRequirementsNotFulfilledException() throws Exception {
		// given
		final String message = UUID.randomUUID().toString();
		final String errorCode = UUID.randomUUID().toString();

		// when
		final MvcResult result = performGet(CONTROLLER_PREFIX + "/throwDomainRequirementsNotFulfilledException/" + message + "/" + errorCode);

		// then
		assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.PRECONDITION_FAILED.value());
		assertResponse(message, errorCode, result, DomainRequirementsNotFulfilledException.class);
		assertThrownException(message, errorCode, result, DomainRequirementsNotFulfilledException.class);
		assertThat(TestAppender.getEvents()).contains("design.domain.exceptions.DomainRequirementsNotFulfilledException: " + message);
	}

	@WithMockUser(username="user")
	@Test
	void shouldHandleDomainUnauthorizedException() throws Exception {
		// given
		final String message = UUID.randomUUID().toString();
		final String errorCode = UUID.randomUUID().toString();

		// when
		final MvcResult result = performGet(CONTROLLER_PREFIX + "/throwDomainUnauthorizedException/" + message + "/" + errorCode);

		// then
		assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.UNAUTHORIZED.value());
		assertResponse(message, errorCode, result, DomainUnauthorizedException.class);
		assertThrownException(message, errorCode, result, DomainUnauthorizedException.class);
		assertThat(TestAppender.getEvents()).contains("design.domain.exceptions.DomainUnauthorizedException: " + message);
	}

	@WithMockUser(username="user")
	@Test
	void shouldHandleOptimisticLockException() throws Exception {
		// given
		final String message = UUID.randomUUID().toString();

		// when
		final MvcResult result = performGet(CONTROLLER_PREFIX + "/throwOptimisticLockException/" + message);

		// then
		assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR.value());

		final ExceptionResponse expectedResponse = new ExceptionResponse(OPTIMISTIC_LOCK_EXCEPTION_ERROR_CODE, message);
		assertThat(result.getResponse().getContentAsString()).isEqualTo(toJson(expectedResponse));
		assertThat(TestAppender.getEvents()).contains("javax.persistence.OptimisticLockException: " + message);
	}

	@WithMockUser(username="user")
	@Test
	void shouldHandlePersistenceException() throws Exception {
		// given
		final String message = UUID.randomUUID().toString();

		// when
		final MvcResult result = performGet(CONTROLLER_PREFIX + "/throwPersistenceException/" + message);

		// then
		assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR.value());

		final ExceptionResponse expectedResponse = new ExceptionResponse(PERSISTENCE_EXCEPTION_ERROR_CODE, message);
		assertThat(result.getResponse().getContentAsString()).isEqualTo(toJson(expectedResponse));
		assertThat(TestAppender.getEvents()).contains("javax.persistence.PersistenceException: " + message);
	}

	@WithMockUser(username="user")
	@Test
	void shouldHandleInternalSystemException() throws Exception {
		// given
		final String message = UUID.randomUUID().toString();

		// when
		final MvcResult result = performGet(CONTROLLER_PREFIX + "/throwInternalSystemException/" + message);

		// then
		final Set<String> eventsList = TestAppender.getEvents();
		assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR.value());

		final ExceptionResponse expectedResponse = new ExceptionResponse(INTERNAL_SYSTEM_EXCEPTION_ERROR_CODE, message);
		assertThat(result.getResponse().getContentAsString()).isEqualTo(toJson(expectedResponse));
		assertThat(TestAppender.getEvents()).contains("java.lang.ArithmeticException: " + message);
	}

	private void assertResponse(
		final String expectedMessage,
		final String expectedErrorCode,
		final MvcResult result,
		final Class<? extends DomainException> domainExceptionClass) throws UnsupportedEncodingException, NoSuchFieldException, IllegalAccessException {

		assertThat(result.getResponse().getContentAsString()).isEqualTo(expectedDomainErrorResponseToJson(expectedErrorCode, expectedMessage, domainExceptionClass));
	}

	private void assertThrownException(
		final String expectedMessage,
		final String expectedErrorCode,
		final MvcResult result,
		final Class<? extends DomainException> domainExceptionClass) throws NoSuchFieldException, IllegalAccessException {

		final Exception resolvedException = result.getResolvedException();
		assertThat(resolvedException).isInstanceOf(domainExceptionClass);
		final DomainException requiredException = (DomainException) resolvedException;
		final String errorCodePrefix = getErrorCodePrefix(domainExceptionClass);
		assertThat(requiredException.getErrorCode()).isEqualTo(errorCodePrefix + expectedErrorCode);
		assertThat(requiredException.getMessage()).isEqualTo(expectedMessage);
	}

	private String expectedDomainErrorResponseToJson(
		@NonNull final String errorCode,
		@NonNull final String message,
		final Class<? extends DomainException> domainExceptionClass) throws NoSuchFieldException, IllegalAccessException {

		final String errorCodePrefix = getErrorCodePrefix(domainExceptionClass);
		final ExceptionResponse expectedResponse = new ExceptionResponse(errorCodePrefix + errorCode, message);
		return toJson(expectedResponse);
	}

	private String toJson(final ExceptionResponse expectedResponse) {
		return new Gson().toJson(expectedResponse);
	}

	private String getErrorCodePrefix(final Class<? extends DomainException> exceptionClass) throws NoSuchFieldException, IllegalAccessException {
		final Field errorCodePrefixField = exceptionClass.getDeclaredField("ERROR_CODE_PREFIX");
		errorCodePrefixField.setAccessible(true);
		return (String) errorCodePrefixField.get(null);
	}

	private MvcResult performGet(final String url) throws Exception {
		return mvc.perform(get(url)).andReturn();
	}

	private MockHttpServletRequestBuilder get(final String url) {
		return MockMvcRequestBuilders.get(url);
	}

	private class ExceptionResponse {
		public final String errorCode;
		public final String errorMessage;

		private ExceptionResponse(final String errorCode, final String errorMessage) {
			this.errorCode = errorCode;
			this.errorMessage = errorMessage;
		}
	}
}
