package design.infrastructure.security;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDateTime;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.test.context.support.WithMockUser;

import configurations.DataJpaTestConfiguration;
import design.domain.CreationDate;
import design.user.domain.Email;
import design.user.domain.Login;
import design.user.domain.Pass;
import design.utils.FullApplicationContextAbstractIT;
import design.utils.UserInstance.JpaUserInstanceRepository;
import design.utils.UserInstance.UserInstanceFactory;

// https://www.baeldung.com/spring-boot-security-autoconfiguration

@SpringBootTest(webEnvironment = RANDOM_PORT)
@Import(DataJpaTestConfiguration.class)
class SecurityConfigurationIT extends FullApplicationContextAbstractIT {

    @Autowired
    private TestRestTemplate restTemplate;

    private URL base;

    @LocalServerPort
    private int port;

    @Autowired
    private JpaUserInstanceRepository jpaUserInstanceRepository;

    @BeforeEach
    public void setUp() throws MalformedURLException {
        restTemplate = new TestRestTemplate("learner1", "learner1");
        base = new URL("http://localhost:" + port + "/api/status/alive");
    }

    @BeforeEach
    public void fillDb() {
        jpaUserInstanceRepository.save(
            UserInstanceFactory.create(Login.create("learner1"),
                        Pass.create("$2a$10$MLrMaWmUSCQl/lV0gyJlKuEsoSFRRmbuOiJDyJvwivBUUODWaWKF2"),
                        Email.create("email@mail.com"),
                        CreationDate.create(LocalDateTime.now())));
    }

    @AfterEach
    public void clearDb() {
        jpaUserInstanceRepository.deleteAll();
    }

    // TODO split into logged is alive and not logged is alive
    @Test
    @WithMockUser(username="learner1")
    void whenLoggedUserRequestsHomePage_ThenSuccess() throws IllegalStateException {
        ResponseEntity<String> response
                = restTemplate.getForEntity(base.toString(), String.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertTrue(response
                .getBody()
                .contains("Application is alive"));
    }

    @Test
    @WithMockUser(username="learner1")
    void whenUserWithWrongCredentials_thenUnauthorizedPage() {
        restTemplate = new TestRestTemplate("learner1", "wrongpassword");
        ResponseEntity<String> response
                = restTemplate.getForEntity(base.toString(), String.class);

        assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
    }
}
