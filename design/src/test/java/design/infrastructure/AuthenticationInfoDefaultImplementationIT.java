package design.infrastructure;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import design.utils.FullApplicationContextAbstractIT;

class AuthenticationInfoDefaultImplementationIT extends FullApplicationContextAbstractIT {

	@Autowired
	private AuthenticationInfoDefaultImplementation authenticationInfoDefaultImplementation;

	private final String login = UUID.randomUUID().toString();

	@BeforeEach
	void setUp() {
		final Authentication authentication = mock(Authentication.class);
		when(authentication.getName()).thenReturn(login);
		final SecurityContext securityContext = mock(SecurityContext.class);
		when(securityContext.getAuthentication()).thenReturn(authentication);
		SecurityContextHolder.setContext(securityContext);
	}

	@Test
	void shouldReturnLoggedUserLogin() {
		assertThat(authenticationInfoDefaultImplementation.loggedUserLogin()).isEqualTo(login);
	}

}
