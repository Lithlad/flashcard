package design.infrastructure;

import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceException;

import org.springframework.boot.test.context.TestComponent;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import design.domain.exceptions.DomainAlreadyExistsException;
import design.domain.exceptions.DomainInvalidConcurrencyVersionException;
import design.domain.exceptions.DomainNotFoundException;
import design.domain.exceptions.DomainRequirementsNotFulfilledException;
import design.domain.exceptions.DomainUnauthorizedException;

@RestController
@RequestMapping("/test")
@TestComponent
public class ExceptionHandlerTestController {

	@GetMapping("/throwDomainAlreadyExistsException/{message}/{errorCode}")
	protected ResponseEntity<Object> throwDomainAlreadyExistsException(@PathVariable final String message,
	                                                                   @PathVariable final String errorCode) {
		throw new DomainAlreadyExistsException(message, errorCode);
	}

	@GetMapping("/throwDomainInvalidConcurrencyVersionException/{message}/{errorCode}")
	protected ResponseEntity<Object> throwDomainInvalidConcurrencyVersionException(@PathVariable final String message,
	                                                                               @PathVariable final String errorCode) {
		throw new DomainInvalidConcurrencyVersionException(message, errorCode);
	}

	@GetMapping("/throwDomainNotFoundException/{message}/{errorCode}")
	protected ResponseEntity<Object> throwDomainNotFoundException(@PathVariable final String message,
	                                                              @PathVariable final String errorCode) {
		throw new DomainNotFoundException(message, errorCode);
	}

	@GetMapping("/throwDomainRequirementsNotFulfilledException/{message}/{errorCode}")
	protected ResponseEntity<Object> throwDomainRequirementsNotFulfilledException(@PathVariable final String message,
	                                                                              @PathVariable final String errorCode) {
		throw new DomainRequirementsNotFulfilledException(message, errorCode);
	}

	@GetMapping("/throwDomainUnauthorizedException/{message}/{errorCode}")
	protected ResponseEntity<Object> throwDomainUnauthorizedException(@PathVariable final String message,
	                                                                  @PathVariable final String errorCode) {
		throw new DomainUnauthorizedException(message, errorCode);
	}

	@GetMapping("/throwOptimisticLockException/{message}")
	protected ResponseEntity<Object> throwOptimisticLockException(@PathVariable final String message) {
		throw new OptimisticLockException(message);
	}

	@GetMapping("/throwPersistenceException/{message}")
	protected ResponseEntity<Object> throwPersistenceException(@PathVariable final String message) {
		throw new PersistenceException(message);
	}

	@GetMapping("/throwInternalSystemException/{message}")
	protected ResponseEntity<Object> throwInternalSystemException(@PathVariable final String message) {
		throw new ArithmeticException(message);
	}
}
