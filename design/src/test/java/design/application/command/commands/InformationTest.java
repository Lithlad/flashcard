package design.application.command.commands;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.UUID;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import design.application.command.commands.Information;

@Tag("unit")
class InformationTest {

	@Test
	void shouldCreateInformation() {
		// given
		final String identificator = UUID.randomUUID().toString();
		final String message = UUID.randomUUID().toString();

		// when
		final Information information = new Information(identificator, message);

		// then
		assertThat(information.getIdentificator()).isEqualTo(identificator);
		assertThat(information.getMessage()).isEqualTo(message);
	}

}
