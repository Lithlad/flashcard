package design.application.command;

import java.util.UUID;

import design.domain.DomainEvent;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode
class ExampleEvent extends DomainEvent {

	ExampleEvent() {
		super(1);
	}

	@Override
	public String eventIdentifier() {
		return UUID.randomUUID().toString();
	}

	@Override
	public String eventDomain() {
		return UUID.randomUUID().toString();
	}
}
