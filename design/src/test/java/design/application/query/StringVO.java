package design.application.query;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;

import design.domain.ValueObject;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Embeddable
@NoArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class StringVO extends ValueObject<String> {

	public static StringVO create(@NotBlank final String language) {
		return new StringVO(language);
	}

	private StringVO(@NotBlank final String value) {
		super(value);
	}
}
