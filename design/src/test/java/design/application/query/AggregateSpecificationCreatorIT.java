package design.application.query;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Predicate.BooleanOperator;
import javax.persistence.criteria.Root;

import org.hibernate.query.criteria.internal.predicate.ComparisonPredicate;
import org.hibernate.query.criteria.internal.predicate.ComparisonPredicate.ComparisonOperator;
import org.hibernate.query.criteria.internal.predicate.InPredicate;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import design.utils.FullApplicationContextAbstractIT;
import design.domain.ValueObject;
import design.infrastructure.exception.NotImplementedException;
import design.infrastructure.repository.criteria.SearchOperation;
import design.infrastructure.repository.criteria.SetStringSearchCriterium;
import design.infrastructure.repository.criteria.StringSearchCriterium;

class AggregateSpecificationCreatorIT extends FullApplicationContextAbstractIT {

	@PersistenceContext
	private EntityManager em;

	@Test
	void shouldReturnEqualsPredicate() {
		// given
		final CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		final Root<SampleAggregate> root = criteriaBuilder.createQuery().from(SampleAggregate.class);
		final String value = UUID.randomUUID().toString();
		final StringSearchCriterium searchCriterium = new StringSearchCriterium();
		searchCriterium.setValue(value);
		searchCriterium.setOperation(SearchOperation.SAME);
		final String fieldName = "stringVO";
		final Function<String, ValueObject<String>> voMapper = v -> StringVO.create(v);

		// when
		final Optional<Predicate> maybePredicate = AggregateSpecificationCreator.INSTANCE.maybePredicate(root, criteriaBuilder, searchCriterium, fieldName, voMapper);

		// then
		assertThat(maybePredicate).isNotEmpty();
		final Predicate predicate = maybePredicate.get();
		assertThat(predicate).isNotNull();
		assertThat(predicate.getOperator()).isEqualTo(BooleanOperator.AND);
		assertThat(predicate.isNegated()).isFalse();
		assertThat(predicate).isInstanceOf(ComparisonPredicate.class);
		final ComparisonPredicate comparisonPredicate = (ComparisonPredicate) predicate;
		assertThat(comparisonPredicate.getComparisonOperator()).isEqualTo(ComparisonOperator.EQUAL);
		assertThat(comparisonPredicate.getRightHandOperand().getJavaType()).isEqualTo(StringVO.class);
	}

	@Disabled
	@Test
	void shouldReturnLessPredicate() {
		throw new NotImplementedException();
	}

	@Disabled
	@Test
	void shouldReturnGreaterPredicate() {
		throw new NotImplementedException();
	}

	@Test
	void shouldReturnInPredicate() {
		// given
		final CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		final Root<SampleAggregate> root = criteriaBuilder.createQuery().from(SampleAggregate.class);
		final SetStringSearchCriterium searchCriterium = new SetStringSearchCriterium();
		final Set<String> values = Set.of("val1", "val2");
		searchCriterium.setValues(values);
		final String fieldName = "stringVO";
		final Function<String, ValueObject<String>> voMapper = v -> StringVO.create(v);

		// when
		final Optional<Predicate> maybePredicate = AggregateSpecificationCreator.INSTANCE.maybePredicate(root, criteriaBuilder, searchCriterium, fieldName, voMapper);

		// then
		assertThat(maybePredicate).isNotEmpty();
		final Predicate predicate = maybePredicate.get();
		assertThat(predicate).isNotNull();
		assertThat(predicate.getOperator()).isEqualTo(BooleanOperator.AND);
		assertThat(predicate.isNegated()).isFalse();
		assertThat(predicate).isInstanceOf(InPredicate.class);
		final InPredicate inPredicate = (InPredicate) predicate;
		assertThat(inPredicate.getValues()).hasSize(2);
		assertThat(inPredicate.getValues().get(0)).isInstanceOf(Expression.class);
		assertThat(inPredicate.getValues().get(1)).isInstanceOf(Expression.class);
	}

	@Test
	void shouldReturnEmptyInPredicateIfCriteriumIsNull() {
		// given
		final CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		final Root<SampleAggregate> root = criteriaBuilder.createQuery().from(SampleAggregate.class);
		final String value = UUID.randomUUID().toString();
		SetStringSearchCriterium searchCriterium = null;
		final String fieldName = "stringVO";
		final Function<String, ValueObject<String>> voMapper = v -> StringVO.create(value);

		// when
		final Optional<Predicate> maybePredicate = AggregateSpecificationCreator.INSTANCE.maybePredicate(root, criteriaBuilder, searchCriterium, fieldName, voMapper);

		// then
		assertThat(maybePredicate).isEmpty();
	}

	@Test
	void shouldReturnEmptyPredicateIfSearchCriteriumIsEmpty() {
		// given
		final CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		final Root<SampleAggregate> root = criteriaBuilder.createQuery().from(SampleAggregate.class);
		final String value = UUID.randomUUID().toString();
		final StringSearchCriterium searchCriterium = null;
		final String fieldName = "stringVO";
		final Function<String, ValueObject<String>> voMapper = v -> StringVO.create(value);

		// when
		final Optional<Predicate> maybePredicate = AggregateSpecificationCreator.INSTANCE.maybePredicate(root, criteriaBuilder, searchCriterium, fieldName, voMapper);

		// then
		assertThat(maybePredicate).isEmpty();
	}
}
