package design.application.query;

import javax.persistence.Embedded;
import javax.persistence.Entity;

import design.domain.Aggregate;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
@Entity
@NoArgsConstructor
public class SampleAggregate extends Aggregate {

	@Embedded
	private StringVO stringVO;
}
