package configurations;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpSession;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.core.userdetails.UserDetails;

class AuditingTestConfiguration {

	public static final String SAMPLE_USER_NAME = "SampleUser";

	@Bean
	@Primary
	public HttpSession httpSession() {
		final HttpSession httpSession = mock(HttpSession.class);
		final SecurityContextImpl securityContext = mock(SecurityContextImpl.class);
		final Authentication authentication = mock(Authentication.class);
		final UserDetails userDetails = mock(UserDetails.class);

		when(userDetails.getUsername()).thenReturn(SAMPLE_USER_NAME);
		when(authentication.getPrincipal()).thenReturn(userDetails);
		when(securityContext.getAuthentication()).thenReturn(authentication);
		when(httpSession.getAttribute("SPRING_SECURITY_CONTEXT")).thenReturn(securityContext);
		return httpSession;
	}
}
