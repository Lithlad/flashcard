package configurations;

import static org.mockito.Mockito.mock;
import static org.springframework.core.Ordered.HIGHEST_PRECEDENCE;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;

import design.domain.EventBroadcaster;

/*
    Moved because classes autoscan includes classes from lithlad.flashcards
 */
@TestConfiguration
public class ServiceDomainTestConfiguration extends AuditingTestConfiguration {

    public static final EventBroadcaster EVENT_BROADCASTER = mock(EventBroadcaster.class);

    @Bean
    @Order(HIGHEST_PRECEDENCE)
    public EventBroadcaster eventBroadcaster() {
        return EVENT_BROADCASTER;
    }
}
