package design.domain;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Embeddable
@NoArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class CreationDate extends ValueObject<LocalDateTime> {

    public static CreationDate create(@NotBlank final LocalDateTime dateTime) {
        return new CreationDate(dateTime);
    }

    private CreationDate(final LocalDateTime date) {
        super(date);
    }

    // TODO test
    public long epoch() {
        return value().toEpochSecond(ZoneOffset.UTC);
    }
}
