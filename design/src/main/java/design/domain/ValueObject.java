package design.domain;

import java.util.Objects;

import javax.persistence.MappedSuperclass;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.ToString;

@NoArgsConstructor
@ToString
@EqualsAndHashCode
@MappedSuperclass
public class ValueObject<T> {

    private T value;

    public ValueObject(@NonNull final T value) {
        this.value = value;
    }

    public T value() {
        return value;
    }

    public boolean isEqual(final ValueObject<T> other) {
        return value.equals(other.value);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final ValueObject<T> that = (ValueObject<T>) o;
        return Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
