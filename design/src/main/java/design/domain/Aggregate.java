package design.domain;

import static java.util.Collections.unmodifiableList;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.springframework.data.domain.AfterDomainEventPublication;
import org.springframework.data.domain.DomainEvents;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.ToString;

@ToString
@EqualsAndHashCode
@MappedSuperclass
@NoArgsConstructor
public class Aggregate extends Auditable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Getter
    @Version
    @Column(columnDefinition = "INT DEFAULT 0", nullable = false)
    private int concurrencyVersion = 0;

    @Transient
    private final List<DomainEvent> events = new LinkedList<>();

    protected Aggregate(final CreationDate creationDate) {
        this.creationDate = creationDate;
    }

    @DomainEvents
    public Collection<DomainEvent> events() {
        return unmodifiableList(events);
    }

    @AfterDomainEventPublication
    public void clearEvents() {
        events.clear();
    }

    protected void registerEvent(@NonNull final DomainEvent event) {
        events.add(event);
    }
}
