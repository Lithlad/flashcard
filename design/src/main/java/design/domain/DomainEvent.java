package design.domain;

import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
@EqualsAndHashCode(exclude = { "concurrencyVersion", "eventDate" })
public abstract class DomainEvent implements Serializable {

    private int concurrencyVersion;

    @NonNull
    private long eventDate;

    public DomainEvent(final int concurrencyVersion) {
        this.concurrencyVersion = concurrencyVersion;
        eventDate = System.currentTimeMillis();
    }

    public abstract String eventIdentifier();

    public abstract String eventDomain();
}
