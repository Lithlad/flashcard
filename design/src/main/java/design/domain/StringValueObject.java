package design.domain;

import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotBlank;

import design.domain.exceptions.DomainValueLengthExceedsMaximalLengthException;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@MappedSuperclass
public class StringValueObject extends ValueObject<String> {

	protected StringValueObject(@NotBlank final String value, final int maxLength) {
		super(value);
		validateLength(value, maxLength);
	}

	private void validateLength(@NotBlank final String value, final int maxLength) {
		if (value.length() > maxLength)
			throw new DomainValueLengthExceedsMaximalLengthException(value, maxLength);
	}
}
