package design.domain;

import lombok.NonNull;

public interface EventBroadcaster {
    void broadcast(@NonNull DomainEvent event);
}
