package design.domain.exceptions;

import javax.validation.constraints.NotBlank;

public class DomainInvalidConcurrencyVersionException extends DomainException {

	private static final String ERROR_CODE_PREFIX = "invalid.concurrency.version.";

	public DomainInvalidConcurrencyVersionException(final @NotBlank String message, @NotBlank final String errorCode) {
		super(message, ERROR_CODE_PREFIX.concat(errorCode));
	}
}
