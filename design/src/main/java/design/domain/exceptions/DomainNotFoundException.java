package design.domain.exceptions;

import javax.validation.constraints.NotBlank;

public class DomainNotFoundException extends DomainException {

    private static final String ERROR_CODE_PREFIX = "not.found.";

    public DomainNotFoundException(final @NotBlank String message, final @NotBlank String errorCode) {
        super(message, ERROR_CODE_PREFIX.concat(errorCode));
    }
}
