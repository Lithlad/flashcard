package design.domain.exceptions;

import static java.lang.String.format;

import javax.validation.constraints.NotBlank;

public class DomainValueLengthExceedsMaximalLengthException extends DomainRequirementsNotFulfilledException {

	private static final String ERROR_CODE_PREFIX = "max.length.exceeded.";

	public DomainValueLengthExceedsMaximalLengthException(final @NotBlank String value, final int maxLength) {
		super(format("Value %s exceeds maximal allowed length which is %s", value, maxLength), ERROR_CODE_PREFIX);
	}
}
