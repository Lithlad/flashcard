package design.domain.exceptions;

import javax.validation.constraints.NotBlank;

public class DomainRequirementsNotFulfilledException extends DomainException {

    private static final String ERROR_CODE_PREFIX = "requirement.not.fulfilled.";

    public DomainRequirementsNotFulfilledException(final @NotBlank String message, final @NotBlank String errorCode) {
        super(message, ERROR_CODE_PREFIX.concat(errorCode));
    }
}
