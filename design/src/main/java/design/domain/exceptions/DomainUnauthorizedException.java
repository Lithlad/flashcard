package design.domain.exceptions;

import javax.validation.constraints.NotBlank;

public class DomainUnauthorizedException extends DomainException {

	private static final String ERROR_CODE_PREFIX = "unauthorized.request.";

	public DomainUnauthorizedException(final @NotBlank String message, final @NotBlank String errorCode) {
		super(message, ERROR_CODE_PREFIX.concat(errorCode));
	}
}
