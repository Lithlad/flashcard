package design.domain.exceptions;

import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.NonNull;

public class DomainException extends RuntimeException {

    @Getter
    @NotBlank
    private final String errorCode;

    public DomainException(@NotBlank final String message, @NotBlank String errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public DomainException(@NotBlank final String message, @NonNull final Throwable cause, @NotBlank String errorCode) {
        super(message, cause);
        this.errorCode = errorCode;
    }
}
