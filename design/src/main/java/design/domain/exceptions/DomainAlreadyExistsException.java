package design.domain.exceptions;

import javax.validation.constraints.NotBlank;

public class DomainAlreadyExistsException extends DomainException {

    private static final String ERROR_CODE_PREFIX = "already.exists.";

    public DomainAlreadyExistsException(final @NotBlank String message, final @NotBlank String errorCode) {
        super(message, ERROR_CODE_PREFIX.concat(errorCode));
    }
}
