package design.security.infrastructure;

import org.springframework.security.core.userdetails.UserDetails;

import javax.validation.constraints.NotBlank;

public interface BusinessApplicationUserService {
    UserDetails loadByUsername(@NotBlank String username);
}
