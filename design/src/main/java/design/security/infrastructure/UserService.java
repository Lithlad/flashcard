package design.security.infrastructure;

import javax.validation.constraints.NotBlank;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

@Component
public class UserService implements UserDetailsService {

    private final BusinessApplicationUserService businessApplicationUserService;

    @Autowired
    public UserService(final BusinessApplicationUserService businessApplicationUserService) {
        this.businessApplicationUserService = businessApplicationUserService;
    }

    @Override
    public UserDetails loadUserByUsername(@NotBlank final String username) {
        return businessApplicationUserService.loadByUsername(username);
    }
}
