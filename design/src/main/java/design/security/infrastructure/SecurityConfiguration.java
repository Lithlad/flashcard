package design.security.infrastructure;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

/*
    TODO configure correctly
 */
//https://www.baeldung.com/spring-security-login
//https://www.baeldung.com/spring-boot-security-autoconfiguration

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final UserService userService;

    @Autowired
    public SecurityConfiguration(final UserService userService) {
        this.userService = userService;
    }

    @Override
    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
        auth
                .userDetailsService(userService)
                .passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
//         http
//                 .csrf().disable()
//                 .authorizeRequests()
//                 .antMatchers("/admin/**").hasRole("ADMIN")
//                 .antMatchers("/anonymous*").anonymous()
//                 .antMatchers("/login*").permitAll()
//                 .anyRequest().authenticated()
//                 .and()
//                 .formLogin()
//                 .loginPage("/login.html")
//                 .loginProcessingUrl("/perform_login")
//                 .defaultSuccessUrl("/homepage.html", true)
//                 //.failureUrl("/login.html?error=true")
//                 .failureHandler(authenticationFailureHandler())
//                 .and()
//                 .logout()
//                 .logoutUrl("/perform_logout")
//                 .deleteCookies("JSESSIONID")
//                 .logoutSuccessHandler(logoutSuccessHandler());

        /*
            - The client makes a GET request to Server (Spring Boot Backend), e.g. request for the main page
            - Spring sends the response for GET request along with Set-cookie header which contains securely generated XSRF Token
            - The browser sets the cookie with XSRF Token
            - While sending a state-changing request (e.g. POST) the client (might be angular) copies the cookie value to the HTTP request header
            - The request is sent with both header and cookie (browser attaches the cookie automatically)
            - Spring compares the header and the cookie values, if they are the same the request is accepted, otherwise, 403 is returned to the client

            The method withHttpOnlyFalse allows angular to read XSRF cookie. Make sure that Angular makes XHR request with withCreddentials flag set to true.
         */
        http
            .csrf()
            .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
            .and()
            .authorizeRequests()
            .anyRequest()
            .fullyAuthenticated()
            .and()
            .httpBasic();
    }

    @Override
    public void configure(final WebSecurity web) {
        web
            .ignoring()
            .antMatchers(HttpMethod.POST, "/api/learner/register")
            .antMatchers(HttpMethod.POST, "/api/admin/register")
//            .antMatchers( "/swagger-resources/**")
//            .antMatchers( "/swagger-ui.html")
//            .antMatchers( "/v2/api-docs")
//            .antMatchers( "/webjars/**")
            .antMatchers(HttpMethod.OPTIONS, "/api/**")
        ;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
