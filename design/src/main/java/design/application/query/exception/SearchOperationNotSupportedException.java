package design.application.query.exception;

import static java.text.MessageFormat.format;

import java.util.Set;
import java.util.stream.Collectors;

import design.infrastructure.repository.criteria.SearchOperation;
import design.domain.ValueObject;
import design.domain.exceptions.DomainRequirementsNotFulfilledException;

public class SearchOperationNotSupportedException extends DomainRequirementsNotFulfilledException {
	public SearchOperationNotSupportedException(final SearchOperation operation, final ValueObject vo) {
		super(format("Specification operation %s for value object  %s is not supported", operation, vo), "specification.operation.not.supported");
	}

	public SearchOperationNotSupportedException(final SearchOperation operation, final Set<ValueObject> vo) {
		super(format("Specification operation %s for value object  %s is not supported",
			operation, vo.stream().map(ValueObject::toString).collect(Collectors.joining(", "))),
			"specification.operation.not.supported");
	}
}
