package design.application.query;

public interface AuthenticationInfo {

	String loggedUserLogin();
}
