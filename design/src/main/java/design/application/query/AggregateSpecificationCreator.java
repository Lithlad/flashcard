package design.application.query;

import static java.util.Objects.nonNull;
import static org.apache.commons.collections.CollectionUtils.isNotEmpty;

import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaBuilder.In;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import design.application.query.exception.SearchOperationNotSupportedException;
import design.infrastructure.repository.criteria.SearchCriterium;
import design.infrastructure.repository.criteria.SearchCriteriumGeneralizer;
import design.infrastructure.repository.criteria.SearchOperation;
import design.infrastructure.repository.criteria.SetStringSearchCriterium;
import design.domain.Aggregate;
import design.domain.ValueObject;

// https://www.baeldung.com/rest-api-search-language-spring-data-specifications
public enum AggregateSpecificationCreator {
	INSTANCE;

	public  <A extends Aggregate> Optional<Predicate> maybePredicate(
		final Root<A> root,
		final CriteriaBuilder criteriaBuilder,
		final SetStringSearchCriterium searchCriterium,
		final String fieldName,
		final Function<String, ValueObject<String>> voMapper) {
		if (nonNull(searchCriterium) && isNotEmpty(searchCriterium.getValues())) {
			return Optional.of(AggregateSpecificationCreator.INSTANCE.toPredicate(
				root,
				criteriaBuilder,
				fieldName,
				searchCriterium.getValues().stream().map(voMapper).collect(Collectors.toSet())));
		}
		return Optional.empty();
	}

	public  <T, A extends Aggregate> Optional<Predicate> maybePredicate(
		final Root<A> root,
		final CriteriaBuilder criteriaBuilder,
		final SearchCriteriumGeneralizer<T> searchCriteriumGeneralizer,
		final String fieldName,
		final Function<T, ValueObject<T>> voMapper) {
		if (nonNull(searchCriteriumGeneralizer)) {
			final SearchCriterium<T> criterium = searchCriteriumGeneralizer.generalize();
			return Optional.of(AggregateSpecificationCreator.INSTANCE.toPredicate(
				root,
				criteriaBuilder,
				fieldName,
				voMapper.apply(criterium.getValue()),
				criterium.getOperation()));
		}
		return Optional.empty();
	}

	private Predicate toPredicate(final Root<? extends Aggregate> root,
	                              final CriteriaBuilder builder,
	                              final String fieldName,
	                              final Set<ValueObject> vo
	) {
		return in(root, builder, fieldName, vo);
	}

	private Predicate toPredicate(final Root<? extends Aggregate> root,
	                             final CriteriaBuilder builder,
	                             final String fieldName,
	                             final ValueObject vo,
	                             final SearchOperation operation
	) {
		switch (operation) {
			case SAME: return same(root, builder, fieldName, vo);
			case GREATER: return greater(root, builder, fieldName, vo);
			case LESS: return less(root, builder, fieldName, vo);
			default: throw new SearchOperationNotSupportedException(operation, vo);
		}
	}

	private Predicate same(final Root<? extends Aggregate> root, final CriteriaBuilder builder, final String fieldName, final ValueObject vo) {
		// Ugly practise. But in future query stack won't be based on value objects but java classes
		if (root.get(fieldName).getJavaType() == String.class) {
			return builder.like(root.get(fieldName), "%" + fieldName + "%");
		}
		return builder.equal(root.get(fieldName), vo);
	}

	private Predicate in(final Root<? extends Aggregate> root, final CriteriaBuilder builder, final String fieldName, final Set<ValueObject> vo) {
		final In<Object> in = builder.in(root.get(fieldName));
		vo.forEach(in::value);
		return in;
	}

	private Predicate greater(final Root<? extends Aggregate> root, final CriteriaBuilder builder, final String fieldName, final ValueObject vo) {
		final Path<String> x = root.get(fieldName);
		return builder.greaterThan(x, vo.toString());
	}


	private Predicate less(final Root<? extends Aggregate> root, final CriteriaBuilder builder, final String fieldName, final ValueObject vo) {
		return builder.lessThan(root.get(fieldName), vo.toString());
	}
}
