package design.application.command.commands;

public interface ModificationCommand {

	Integer getConcurrencyVersion();
}
