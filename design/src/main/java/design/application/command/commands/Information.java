package design.application.command.commands;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@EqualsAndHashCode
@ToString
public class Information {

	private final String identificator;
	private final String message;

	public Information(final String identificator, final String message) {

		this.identificator = identificator;
		this.message = message;
	}
}
