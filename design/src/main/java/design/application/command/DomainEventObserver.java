package design.application.command;

import static java.lang.String.format;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import design.domain.DomainEvent;

public abstract class DomainEventObserver<E extends DomainEvent> {

    public abstract void notify(final E event);
    protected abstract Collection<String> getAuthorities();
    protected abstract String subscriberName();

    protected String prepareEventLogMessage(final String domain, final String eventIdentifier, final LocalDateTime creationDate) {
        return format("%s domain - %s %s event intercepted ", domain, eventIdentifier, creationDate);
    }

    protected void authenticate() {
        final List<SimpleGrantedAuthority> authorities = getAuthorities().stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
        final String subscriberName = subscriberName();
        PreAuthenticatedAuthenticationToken authentication =
            new PreAuthenticatedAuthenticationToken(getUserDetails(subscriberName, authorities), subscriberName, authorities);
        authentication.setAuthenticated(Boolean.TRUE);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    private UserDetails getUserDetails(final String name, final Collection<SimpleGrantedAuthority> authorities) {
        return new UserDetails() {
            @Override
            public Collection<? extends GrantedAuthority> getAuthorities() {
                return authorities;
            }

            @Override
            public String getPassword() {
                return null;
            }

            @Override
            public String getUsername() {
                return name;
            }

            @Override
            public boolean isAccountNonExpired() {
                return true;
            }

            @Override
            public boolean isAccountNonLocked() {
                return true;
            }

            @Override
            public boolean isCredentialsNonExpired() {
                return true;
            }

            @Override
            public boolean isEnabled() {
                return true;
            }
        };
    }
}
