package design.application.command;

import design.domain.DomainEvent;
import design.domain.EventBroadcaster;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class ApplicationService {

    @Autowired
    private EventBroadcaster eventBroadcaster;

    protected abstract void actionWasRaised(@NonNull final DomainEvent event);

    protected void superActionWasRaised(@NonNull final DomainEvent event) {
        eventBroadcaster.broadcast(event);
    }
}
