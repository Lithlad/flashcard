package design.user.ui;

import static org.springframework.http.HttpStatus.OK;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import design.user.application.command.UserApplicationService;
import design.user.application.query.UserViewQueryService;
import design.user.domain.dto.UserDTO;
import design.user.ui.form.DisableUserForm;
import design.user.ui.form.EnableUserForm;
import design.user.ui.form.LockUserForm;
import design.user.ui.form.UnlockUserForm;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.NonNull;

@Api
@RestController
@RequestMapping("/api/user")
public class UserController {

	private final UserApplicationService userApplicationService;

	private final UserViewQueryService queryService;

	@Autowired
	public UserController(final UserApplicationService userApplicationService, final UserViewQueryService queryService) {
		this.userApplicationService = userApplicationService;
		this.queryService = queryService;
	}

	@ApiOperation("Get user")
	@GetMapping("{login}")
	protected ResponseEntity<UserDTO> getUser(@PathVariable @ApiParam(required = true) @NonNull String login) {
		@NonNull final UserDTO body = queryService.find(login);
		return ResponseEntity.ok(body);
	}

	@ApiOperation("Lock user")
	@PostMapping("/lock")
	protected ResponseEntity lockUser(@ApiParam(required = true)
	                                                  @NonNull @Validated @RequestBody LockUserForm form) {
		userApplicationService.lock(form);
		return new ResponseEntity(OK);
	}

	@ApiOperation("Unlock user")
	@PostMapping("/unlock")
	protected ResponseEntity unlockUser(@ApiParam(required = true)
	                                  @NonNull @Validated @RequestBody UnlockUserForm form) {
		userApplicationService.unlock(form);
		return new ResponseEntity(OK);
	}

	@ApiOperation("Enable user")
	@PostMapping("/enable")
	protected ResponseEntity enableUser(@ApiParam(required = true)
	                                  @NonNull @Validated @RequestBody EnableUserForm form) {
		userApplicationService.enable(form);
		return new ResponseEntity(OK);
	}

	@ApiOperation("Disable user")
	@PostMapping("/disable")
	protected ResponseEntity disableUser(@ApiParam(required = true)
	                                  @NonNull @Validated @RequestBody DisableUserForm form) {
		userApplicationService.disable(form);
		return new ResponseEntity(OK);
	}
}
