package design.user.ui.form;

import javax.validation.constraints.NotBlank;

import design.user.application.command.commands.EnableUserCommand;
import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@ApiModel
@EqualsAndHashCode
public class EnableUserForm extends UserModificationForm implements EnableUserCommand {
	public EnableUserForm(final @NotBlank String login, final int concurrencyVersion) {
		super(login, concurrencyVersion);
	}
}
