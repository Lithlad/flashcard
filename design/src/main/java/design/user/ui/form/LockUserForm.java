package design.user.ui.form;

import javax.validation.constraints.NotBlank;

import design.user.application.command.commands.LockUserCommand;
import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@ApiModel
@EqualsAndHashCode
public class LockUserForm extends UserModificationForm implements LockUserCommand {
	public LockUserForm(final @NotBlank String login, final int concurrencyVersion) {
		super(login, concurrencyVersion);
	}
}
