package design.user.ui.form;

import javax.validation.constraints.NotBlank;

import design.user.application.command.commands.RegisterUserCommand;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@ApiModel
@EqualsAndHashCode
public abstract class RegisterUserForm implements RegisterUserCommand {

	@NotBlank
	@ApiModelProperty(value = "login", required = true, example="login")
	private final String login;

	/**
	 * Password may be hashed on front-end side and sent to application as already hashed
	 * But in app password will be hashed on back-end side - it means pass will be sent as plain text
	 * It is one of the reasons why lithlad.flashcards.communication must be encrypted by SSL
	 */
	@NotBlank
	@ApiModelProperty(value = "pass", required = true, example = "password")
	private final String pass;

	@NotBlank
	@ApiModelProperty(value = "email", required = true, example = "learner@lithlad.eu")
	private final String email;

	public RegisterUserForm(final @NotBlank String login, final @NotBlank String pass, final @NotBlank String email) {
		this.login = login;
		this.pass = pass;
		this.email = email;
	}
}
