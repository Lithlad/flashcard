package design.user.ui.form;

import javax.validation.constraints.NotBlank;

import design.user.application.command.commands.DisableUserCommand;
import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@ApiModel
@EqualsAndHashCode
public class DisableUserForm extends UserModificationForm implements DisableUserCommand {
	public DisableUserForm(final @NotBlank String login, final int concurrencyVersion) {
		super(login, concurrencyVersion);
	}
}
