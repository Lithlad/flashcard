package design.user.infrastructure;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import design.user.domain.Login;
import design.user.domain.User;
import design.user.domain.UserInstanceRepository;
import design.user.domain.UserRepository;
import design.user.domain.dto.UserDTO;
import design.user.domain.exceptions.NoSuitableRepositoryForUserException;
import design.user.domain.exceptions.UserDoesNotExistException;
import design.user.domain.exceptions.UserInvalidConcurrencyVersionException;
import lombok.NonNull;

@Repository
public class UserRepositoryImplementation implements UserRepository {

	private final List<UserInstanceRepository> userInstanceRepositories;

	@Autowired
	public UserRepositoryImplementation(final List<UserInstanceRepository> userInstanceRepositories) {
		this.userInstanceRepositories = userInstanceRepositories;
	}

	@Override
	public User getByLoginAndConcurrencyVersion(final @NonNull Login login, final Integer concurrencyVersion) {
		return userInstanceRepositories.stream()
			.map(repository -> repository.findByLogin(login))
			.filter(Optional::isPresent)
			.findFirst()
			.orElseThrow(() -> new UserDoesNotExistException(login.value()))
			.filter(u -> u.getConcurrencyVersion() == concurrencyVersion)
			.orElseThrow(() -> new UserInvalidConcurrencyVersionException(login));
	}

	@Override
	public UserDTO save(final @NonNull User user) {
		return userInstanceRepositories.stream()
			.map(repository -> repository.saveIfSuitable(user))
			.flatMap(Optional::stream)
			.findFirst()
			.map(User::mapToDTO)
			.orElseThrow(() -> new NoSuitableRepositoryForUserException(user));
	}

	@Override
	public boolean doesExist(final Login login) {
		return userInstanceRepositories.stream().map(repository ->
			repository.doesExist(login))
			.filter(exists -> exists.equals(true))
			.findFirst()
			.orElse(false);
	}
}
