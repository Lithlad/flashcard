package design.user.infrastructure;

import java.util.List;
import java.util.Optional;

import javax.validation.constraints.NotBlank;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;

import design.user.domain.Login;
import design.user.domain.UserDetailsRepository;
import design.user.domain.UserInstanceRepository;
import design.user.domain.exceptions.UserDoesNotExistException;

@Repository
public class JpaUserDetailsRepository implements UserDetailsRepository {

	private final List<UserInstanceRepository> userInstanceRepositories;

	@Autowired
	public JpaUserDetailsRepository(final List<UserInstanceRepository> userInstanceRepositories) {
		this.userInstanceRepositories = userInstanceRepositories;
	}

	@Override
	public UserDetails loadByUsername(final @NotBlank String login) {
		final Login loginVO = Login.create(login);
		return userInstanceRepositories.stream().map(repository ->
			repository.findByLogin(loginVO))
			.filter(Optional::isPresent)
			.findFirst()
			.orElseThrow(() -> new UserDoesNotExistException(login))
			.orElseThrow(() -> new UserDoesNotExistException(login))
			.mapToUserDetails();
	}
}
