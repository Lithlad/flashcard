package design.user.application.command.commands;

import javax.validation.constraints.NotNull;

public interface RegisterUserCommand extends UserCommand {

	@NotNull
	String getPass();

	@NotNull
	String getEmail();
}
