package design.user.application.command;

import java.util.function.Consumer;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import design.application.command.ApplicationService;
import design.user.application.command.commands.DisableUserCommand;
import design.user.application.command.commands.EnableUserCommand;
import design.user.application.command.commands.LockUserCommand;
import design.user.application.command.commands.RegisterUserCommand;
import design.user.application.command.commands.UnlockUserCommand;
import design.user.domain.Login;
import design.user.domain.User;
import design.user.domain.UserRepository;
import design.user.domain.exceptions.UserAlreadyExistException;
import lombok.NonNull;

public abstract class UserInstanceApplicationService<U extends User, C extends RegisterUserCommand> extends ApplicationService {

	private final UserRepository userRepository;

	protected UserInstanceApplicationService(final UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	protected abstract void saveUser(@NonNull final U user);

	protected abstract U createUserInstance(@NonNull final C command);

	protected abstract boolean isAppropriate(@NonNull final Login login);

	@Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.DEFAULT)
	public void register(@NonNull final C command) {
		final Login login = Login.create(command.getLogin());

		if (userRepository.doesExist(login))
			throw new UserAlreadyExistException(login);

		U user = createUserInstance(command);
		saveUser(user);
	}

	@PreAuthorize("hasAnyAuthority('ADMIN','LEARNER')")
	void lock(@NonNull final LockUserCommand command) {
		modifyUserState(Login.create(command.getLogin()), command.getConcurrencyVersion(), User::lock);
	}

	@PreAuthorize("hasAuthority('ADMIN')")
	void unlock(@NonNull final UnlockUserCommand command) {
		modifyUserState(Login.create(command.getLogin()), command.getConcurrencyVersion(), User::unlock);
	}

	@PreAuthorize("hasAuthority('ADMIN')")
	void disable(@NonNull final DisableUserCommand command) {
		modifyUserState(Login.create(command.getLogin()), command.getConcurrencyVersion(), User::disable);
	}

	@PreAuthorize("hasAuthority('ADMIN')")
	void enable(@NonNull final EnableUserCommand command) {
		modifyUserState(Login.create(command.getLogin()), command.getConcurrencyVersion(), User::enable);
	}

	private void modifyUserState(final Login login, final Integer concurrencyVersion, final Consumer<User> userConsumer) {
		final User user =  userRepository.getByLoginAndConcurrencyVersion(login, concurrencyVersion);

		userConsumer.accept(user);
		saveUser((U) user);
	}
}
