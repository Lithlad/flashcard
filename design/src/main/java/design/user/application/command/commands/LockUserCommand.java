package design.user.application.command.commands;

import design.application.command.commands.ModificationCommand;

public interface LockUserCommand extends UserCommand, ModificationCommand { }
