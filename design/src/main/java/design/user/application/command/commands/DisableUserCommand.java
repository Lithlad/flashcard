package design.user.application.command.commands;

import design.application.command.commands.ModificationCommand;

public interface DisableUserCommand extends UserCommand, ModificationCommand { }
