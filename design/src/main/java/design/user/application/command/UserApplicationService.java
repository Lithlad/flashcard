package design.user.application.command;

import java.util.List;
import java.util.function.BiConsumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import design.user.application.command.commands.DisableUserCommand;
import design.user.application.command.commands.EnableUserCommand;
import design.user.application.command.commands.LockUserCommand;
import design.user.application.command.commands.UnlockUserCommand;
import design.user.application.command.commands.UserCommand;
import design.user.domain.Login;
import design.user.domain.exceptions.UserDoesNotExistException;
import lombok.NonNull;

@Service
public class UserApplicationService {

	private final List<UserInstanceApplicationService> userServices;

	@Autowired
	public UserApplicationService(final List<UserInstanceApplicationService> userServices) {
		this.userServices = userServices;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.DEFAULT)
	public void lock(@NonNull final LockUserCommand command) {
		invokeActionOnUserService(command, (service, c) -> service.lock(c));
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.DEFAULT)
	public void unlock(@NonNull final UnlockUserCommand command) {
		invokeActionOnUserService(command, (service, c) -> service.unlock(c));
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.DEFAULT)
	public void disable(@NonNull final DisableUserCommand command) {
		invokeActionOnUserService(command, (service, c) -> service.disable(c));
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.DEFAULT)
	public void enable(@NonNull final EnableUserCommand command) {
		invokeActionOnUserService(command, (service, c) -> service.enable(c));
	}

	private <C extends UserCommand> void invokeActionOnUserService(final @NonNull C command,
	                                                               final @NonNull BiConsumer<UserInstanceApplicationService, C> consumer) {
		final Login login = Login.create(command.getLogin());

		final UserInstanceApplicationService userInstanceApplicationService = userServices.stream()
			.filter(service -> service.isAppropriate(login))
			.findFirst()
			.orElseThrow(() -> new UserDoesNotExistException(login.value()));

		consumer.accept(userInstanceApplicationService, command);
	}
}
