package design.user.application.command.commands;

import javax.validation.constraints.NotEmpty;

public interface UserCommand {

	@NotEmpty
	String getLogin();
}
