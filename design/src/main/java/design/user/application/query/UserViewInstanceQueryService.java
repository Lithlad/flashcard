package design.user.application.query;

import java.util.Optional;

import design.user.domain.Login;
import design.user.domain.User;
import lombok.NonNull;

public interface UserViewInstanceQueryService {

	Optional<? extends User> findByLogin(@NonNull final Login login);
}
