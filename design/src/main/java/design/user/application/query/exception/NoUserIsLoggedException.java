package design.user.application.query.exception;

import design.domain.exceptions.DomainException;

public class NoUserIsLoggedException extends DomainException {

	public NoUserIsLoggedException() {
		super("No user is currently logged", "no.user.is.logged");
	}
}
