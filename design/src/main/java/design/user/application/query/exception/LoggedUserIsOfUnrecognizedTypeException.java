package design.user.application.query.exception;

import design.domain.exceptions.DomainException;

public class LoggedUserIsOfUnrecognizedTypeException extends DomainException {

	public LoggedUserIsOfUnrecognizedTypeException() {
		super("Logged user type is not recognized", "user.type.not.recognized");
	}
}
