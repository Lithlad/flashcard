package design.user.application.query;

import java.util.List;
import java.util.Optional;

import javax.validation.constraints.NotBlank;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import design.user.application.query.exception.LoggedUserIsOfUnrecognizedTypeException;
import design.user.application.query.exception.NoUserIsLoggedException;
import design.user.domain.dto.UserDTO;
import design.user.domain.Login;
import design.user.domain.User;
import design.user.domain.exceptions.UserDoesNotExistException;
import lombok.NonNull;

@Service
public class UserViewQueryService {

	private final List<UserViewInstanceQueryService> userViewInstanceQueryServices;

	@Autowired
	public UserViewQueryService(final List<UserViewInstanceQueryService> userViewInstanceQueryServices) {
		this.userViewInstanceQueryServices = userViewInstanceQueryServices;
	}

	@NonNull
	public UserDTO find(@NotBlank final String login) {
		final Login loginVO = Login.create(login);
		return userViewInstanceQueryServices.stream()
			.map(service -> service.findByLogin(loginVO))
			.filter(Optional::isPresent)
			.findFirst()
			.orElseThrow(() -> new UserDoesNotExistException(login))
			.map(User::mapToDTO).orElseThrow(() -> new UserDoesNotExistException(login));
	}

	public Login currentUserLogin() {
		return Optional.of(getAuthentication())
			.map(Authentication::getPrincipal)
			.filter(principal -> principal instanceof UserDetails)
			.map(UserDetails.class::cast)
			.map(UserDetails::getUsername)
			.map(Login::create)
			.orElseThrow(LoggedUserIsOfUnrecognizedTypeException::new);
	}

	private Authentication getAuthentication() {
		return Optional.ofNullable(SecurityContextHolder.getContext())
						.map(SecurityContext::getAuthentication)
						.filter(Authentication::isAuthenticated)
						.orElseThrow(NoUserIsLoggedException::new);
	}
}
