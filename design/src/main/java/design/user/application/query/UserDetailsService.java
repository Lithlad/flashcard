package design.user.application.query;

import javax.validation.constraints.NotBlank;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import design.security.infrastructure.BusinessApplicationUserService;
import design.user.domain.UserDetailsRepository;

@Service
public class UserDetailsService implements BusinessApplicationUserService {

    private final UserDetailsRepository userDetailsRepository;

    @Autowired
    public UserDetailsService(final UserDetailsRepository userDetailsRepository) {
        this.userDetailsRepository = userDetailsRepository;
    }

    @Override
    public UserDetails loadByUsername(@NotBlank final String username) {
        return userDetailsRepository.loadByUsername(username);
    }
}
