package design.user.domain.communication.event;

public class UserEventsIdentifiers {

	private UserEventsIdentifiers() {}

	public static final String USER_DOMAIN = "user_domain";

	public static final String USER_WAS_LOCKED = "user_was_locked";
	public static final String USER_WAS_UNLOCKED = "user_was_unlocked";
	public static final String USER_WAS_ENABLED = "user_was_enabled";
	public static final String USER_WAS_DISABLED = "user_was_disabled";
}
