package design.user.domain.communication.event;

import design.user.domain.Login;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode(exclude = { "concurrencyVersion", "eventDate" })
public class UserWasUnlockedEvent extends UserEvent {

	public UserWasUnlockedEvent(final Login login, final int concurrencyVersion) {
		super(login, concurrencyVersion);
	}

	@Override
	public String eventIdentifier() {
		return UserEventsIdentifiers.USER_WAS_UNLOCKED;
	}
}
