package design.user.domain.communication.event;

import design.domain.DomainEvent;
import design.user.domain.Login;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;

@Getter
@EqualsAndHashCode(callSuper = true)
abstract class UserEvent extends DomainEvent {

	private final String login;

	UserEvent(@NonNull final Login login, final int concurrencyVersion) {
		super(concurrencyVersion);
		this.login = login.value();
	}

	@Override
	public String eventDomain() {
		return UserEventsIdentifiers.USER_DOMAIN;
	}
}
