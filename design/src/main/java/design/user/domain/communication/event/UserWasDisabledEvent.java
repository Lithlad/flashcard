package design.user.domain.communication.event;

import design.user.domain.Login;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;

@Getter
@EqualsAndHashCode(exclude = { "concurrencyVersion", "eventDate" })
public class UserWasDisabledEvent extends UserEvent {
	public UserWasDisabledEvent(@NonNull final Login login, final int concurrencyValue) {
		super(login, concurrencyValue);
	}

	@Override
	public String eventIdentifier() {
		return UserEventsIdentifiers.USER_WAS_DISABLED;
	}
}
