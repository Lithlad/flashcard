package design.user.domain;

import javax.persistence.Embeddable;

import design.domain.ValueObject;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Embeddable
@NoArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class Enabled extends ValueObject<Boolean> {

	public static Enabled create(boolean locked) {
		return new Enabled(locked);
	}

	private Enabled(boolean locked) {
		super(locked);
	}

	public boolean isEnabled() {
		return value();
	}
}
