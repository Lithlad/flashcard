package design.user.domain.exceptions;

import static java.lang.String.format;

import design.domain.exceptions.DomainInvalidConcurrencyVersionException;
import design.user.domain.Login;
import lombok.NonNull;

public class UserInvalidConcurrencyVersionException  extends DomainInvalidConcurrencyVersionException {
	public UserInvalidConcurrencyVersionException(final @NonNull Login login) {
		super(format("User %s concurrency version is invalid", login.value()), "user");
	}
}
