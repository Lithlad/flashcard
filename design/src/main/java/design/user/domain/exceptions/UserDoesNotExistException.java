package design.user.domain.exceptions;

import static java.lang.String.format;

import javax.validation.constraints.NotBlank;

import design.domain.exceptions.DomainRequirementsNotFulfilledException;

public class UserDoesNotExistException extends DomainRequirementsNotFulfilledException {
	public UserDoesNotExistException(final @NotBlank String login) {
		super(format("User %s does not exist", login), "user.does.not.exist");
	}
}
