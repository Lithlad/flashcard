package design.user.domain.exceptions;

import static java.lang.String.format;

import design.domain.exceptions.DomainAlreadyExistsException;
import design.user.domain.Login;

public class UserAlreadyExistException extends DomainAlreadyExistsException {
	public UserAlreadyExistException(final Login login) {
		super(format("User with login %s already exists", login.value()), "user");
	}
}
