package design.user.domain.exceptions;

import static java.lang.String.format;

import design.domain.exceptions.DomainRequirementsNotFulfilledException;

public class EmailIsInvalidException extends DomainRequirementsNotFulfilledException {
	public EmailIsInvalidException(final String email) {
		super(format("Email %s is invalid", email), "email.is.invalid");
	}
}
