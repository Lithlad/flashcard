package design.user.domain.exceptions;

import static java.lang.String.format;

import design.domain.exceptions.DomainException;
import design.user.domain.User;
import lombok.NonNull;

public class NoSuitableRepositoryForUserException extends DomainException {
	public NoSuitableRepositoryForUserException(final @NonNull User user) {
		super(format("User %s was not saved - no suitable repository was found", user.toString()), "repository.not.found");
	}
}
