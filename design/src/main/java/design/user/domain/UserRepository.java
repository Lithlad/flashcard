package design.user.domain;

import design.user.domain.dto.UserDTO;
import lombok.NonNull;

public interface UserRepository {
	User getByLoginAndConcurrencyVersion(@NonNull final Login login, final Integer concurrencyVersion);

	UserDTO save(@NonNull final User learner);

	boolean doesExist(Login login);
}
