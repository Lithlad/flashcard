package design.user.domain;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import design.domain.Aggregate;
import design.domain.CreationDate;
import design.user.domain.communication.event.UserWasDisabledEvent;
import design.user.domain.communication.event.UserWasEnabledEvent;
import design.user.domain.communication.event.UserWasLockedEvent;
import design.user.domain.communication.event.UserWasUnlockedEvent;
import design.user.domain.dto.UserDTO;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false, exclude = { "enabled", "locked" })
@MappedSuperclass
@NoArgsConstructor
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class User extends Aggregate {

	@Embedded
	@AttributeOverride(name = "value", column = @Column(name = "login", unique = true))
	protected Login login;

	@Embedded
	@AttributeOverride(name = "value", column = @Column(name = "pass"))
	protected Pass pass;

	@Embedded
	@AttributeOverride(name = "value", column = @Column(name = "email"))
	protected Email email;

	@Embedded
	@AttributeOverride(name = "value", column = @Column(name = "enabled"))
	protected Enabled enabled;

	@Embedded
	@AttributeOverride(name = "value", column = @Column(name = "locked"))
	protected Locked locked;

	protected User(final CreationDate creationDate, final Login login, final Pass pass, final Email email) {
		super(creationDate);
		this.login = login;
		this.pass = pass;
		this.email = email;
		this.enabled = Enabled.create(true);
		this.locked = Locked.create(false);
	}

	public abstract UserDTO mapToDTO();

	protected abstract List<String> getRoles();

	public void lock() {
		if (locked.isLocked())
			return;
		locked = Locked.create(true);
		registerEvent(new UserWasLockedEvent(login, getConcurrencyVersion()));
	}

	public void unlock() {
		if (!locked.isLocked())
			return;
		locked = Locked.create(false);
		registerEvent(new UserWasUnlockedEvent(login, getConcurrencyVersion()));
	}

	public void enable() {
		if (enabled.isEnabled())
			return;
		enabled = Enabled.create(true);
		registerEvent(new UserWasEnabledEvent(login, getConcurrencyVersion()));
	}

	public void disable() {
		if (!enabled.isEnabled())
			return;
		enabled = Enabled.create(false);
		registerEvent(new UserWasDisabledEvent(login, getConcurrencyVersion()));
	}

	public Login login() {
		return login;
	}

	public UserDetails mapToUserDetails() {
		return new UserDetails() {
			@Override
			public Collection<? extends GrantedAuthority> getAuthorities() {
				return getRoles().stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
			}

			@Override
			public String getPassword() {
				return pass.value();
			}

			@Override
			public String getUsername() {
				return login.value();
			}

			@Override
			public boolean isAccountNonExpired() {
				return true;
			}

			@Override
			public boolean isAccountNonLocked() {
				return !locked.value();
			}

			@Override
			public boolean isCredentialsNonExpired() {
				return true;
			}

			@Override
			public boolean isEnabled() {
				return enabled.value();
			}
		};
	}
}
