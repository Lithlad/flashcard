package design.user.domain;

import design.domain.ValueObject;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;

@Embeddable
@NoArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class Pass extends ValueObject<String> {
    public static Pass create(@NotBlank final String pass) {
        return new Pass(pass);
    }

    public Pass(final String pass) {
        super(pass);
    }
}
