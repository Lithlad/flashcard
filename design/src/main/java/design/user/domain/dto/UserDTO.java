package design.user.domain.dto;

import java.time.LocalDateTime;

import javax.validation.constraints.NotBlank;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;

@Getter
@EqualsAndHashCode
public class UserDTO {

	@NonNull
	private final String login;

	@NonNull
	private final String email;

	@NonNull
	private final boolean locked;

	@NonNull
	private final boolean enabled;

	@NonNull
	@DateTimeFormat(iso = ISO.DATE_TIME)
	private final LocalDateTime creationDate;

	private final long concurrencyVersion;

	public UserDTO(@NotBlank final String login, @NotBlank final String email, final boolean locked, final boolean enabled, @NonNull final LocalDateTime creationDate, final long concurrencyVersion) {
		this.login = login;
		this.email = email;
		this.locked = locked;
		this.enabled = enabled;
		this.creationDate = creationDate;
		this.concurrencyVersion = concurrencyVersion;
	}
}
