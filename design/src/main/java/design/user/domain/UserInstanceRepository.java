package design.user.domain;

import java.util.Optional;

import lombok.NonNull;

/**
 * Each domain that implements User class must contain repository implementing this interfaces
 */
// Should be in future splitted to query repo and application repo
public interface UserInstanceRepository {

	Optional<? extends User> findByLogin(@NonNull final Login login);

	/**
	 * Method takes as argument object extending user and saves it, if user instance fits to repository type.
	 * @param user
	 * @return true if type is suitable to be saved by repository. false otherwise
	 */
	Optional<User> saveIfSuitable(@NonNull final User user);

	boolean doesExist(@NonNull final Login login);
}
