package design.user.domain;

import javax.persistence.Embeddable;

import design.domain.ValueObject;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Embeddable
@NoArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class Locked extends ValueObject<Boolean> {

	public static Locked create(boolean locked) {
		return new Locked(locked);
	}

	private Locked(boolean locked) {
		super(locked);
	}

	public boolean isLocked() {
		return value();
	}
}
