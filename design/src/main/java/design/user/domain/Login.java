package design.user.domain;

import design.domain.ValueObject;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;

@Embeddable
@NoArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class Login extends ValueObject<String> {

    public static Login create(@NotBlank final String login) {
        return new Login(login);
    }

    private Login(@NotBlank final String login) {
        super(login);
    }
}
