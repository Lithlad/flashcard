package design.user.domain;

import javax.validation.constraints.NotBlank;

import org.springframework.security.core.userdetails.UserDetails;

public interface UserDetailsRepository {
	UserDetails loadByUsername(@NotBlank final String login);
}
