package design.user.domain;

import design.domain.ValueObject;
import design.user.domain.exceptions.EmailIsInvalidException;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;

import org.apache.commons.validator.routines.EmailValidator;

@Embeddable
@NoArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class Email extends ValueObject<String> {

    public static Email create(@NotBlank final String email) {
        if (!EmailValidator.getInstance().isValid(email))
            throw new EmailIsInvalidException(email);
        return new Email(email);
    }

    private Email(final String email) { super(email); }
}
