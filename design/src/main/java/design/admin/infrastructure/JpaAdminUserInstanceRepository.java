package design.admin.infrastructure;

import static java.util.Optional.empty;
import static java.util.Optional.of;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import design.admin.domain.Admin;
import design.user.application.query.UserViewInstanceQueryService;
import design.user.domain.Login;
import design.user.domain.User;
import design.user.domain.UserInstanceRepository;
import lombok.NonNull;

@Repository
public class JpaAdminUserInstanceRepository implements UserInstanceRepository, UserViewInstanceQueryService {

	private final JpaAdminRepository jpaAdminRepository;

	@Autowired
	public JpaAdminUserInstanceRepository(final JpaAdminRepository jpaAdminRepository) {
		this.jpaAdminRepository = jpaAdminRepository;
	}

	@Override
	public Optional<? extends User> findByLogin(@NonNull final Login login) {
		return jpaAdminRepository.findByLogin(login);
	}

	@Override
	public Optional<User> saveIfSuitable(final User user) {
		if (!(user instanceof Admin))
			return empty();

		return of(jpaAdminRepository.save((Admin) user));
	}

	@Override
	public boolean doesExist(@NonNull final Login login) {
		return jpaAdminRepository.existsByLogin(login);
	}
}
