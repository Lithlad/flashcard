package design.admin.infrastructure;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import design.admin.domain.Admin;
import design.user.domain.Login;
import lombok.NonNull;

@Repository
public interface JpaAdminRepository extends JpaRepository<Admin, Long> {

	Optional<Admin> findByLogin(@NonNull final Login login);

	boolean existsByLogin(@NonNull final Login login);
}
