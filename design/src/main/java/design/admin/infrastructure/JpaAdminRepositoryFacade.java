package design.admin.infrastructure;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import design.admin.domain.Admin;
import design.admin.domain.AdminRepository;
import design.user.domain.Login;
import lombok.NonNull;

@Repository
public class JpaAdminRepositoryFacade implements AdminRepository {

	private final JpaAdminRepository jpaAdminRepository;

	@Autowired
	public JpaAdminRepositoryFacade(final JpaAdminRepository jpaAdminRepository) {
		this.jpaAdminRepository = jpaAdminRepository;
	}

	@Override
	public void save(final @NonNull Admin admin) {
		jpaAdminRepository.save(admin);
	}

	@Override
	public boolean doesExist(final @NonNull Login login) {
		return jpaAdminRepository.existsByLogin(login);
	}
}
