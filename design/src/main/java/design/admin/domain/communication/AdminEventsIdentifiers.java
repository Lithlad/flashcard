package design.admin.domain.communication;

public class AdminEventsIdentifiers {

	private AdminEventsIdentifiers() {}

	public static final String ADMIN_DOMAIN = "admin_domain";

	public static final String ADMIN_WAS_CREATED = "admin_was_created_queue";
}
