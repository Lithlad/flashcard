package design.admin.domain.communication.events;

import design.domain.DomainEvent;
import design.admin.domain.communication.AdminEventsIdentifiers;
import design.user.domain.Login;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;

@Getter
@EqualsAndHashCode(callSuper = true)
public abstract class AdminEvent extends DomainEvent {

	@NonNull
	private final String login;

	AdminEvent(@NonNull final Login login, final int concurrencyVersion) {
		super(concurrencyVersion);
		this.login = login.value();
	}

	@Override
	public String eventDomain() {
		return AdminEventsIdentifiers.ADMIN_DOMAIN;
	}
}
