package design.admin.domain.communication.events;

import design.domain.CreationDate;
import design.admin.domain.communication.AdminEventsIdentifiers;
import design.user.domain.Email;
import design.user.domain.Login;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode(callSuper = true)
public class AdminWasCreatedEvent extends AdminEvent {

	@NonNull
	private final String email;

	@NonNull
	private final long creationDate;

	public AdminWasCreatedEvent(@NonNull final Login login, @NonNull  final Email email, @NonNull final CreationDate creationDate, final int concurrencyVersion) {
		super(login, concurrencyVersion);
		this.email = email.value();
		this.creationDate = creationDate.epoch();
	}

	@Override
	public String eventIdentifier() {
		return AdminEventsIdentifiers.ADMIN_WAS_CREATED;
	}
}
