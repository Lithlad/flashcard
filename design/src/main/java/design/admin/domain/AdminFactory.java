package design.admin.domain;

import design.domain.CreationDate;
import design.user.domain.Email;
import design.user.domain.Login;
import design.user.domain.Pass;
import lombok.NonNull;

public class AdminFactory {

	private AdminFactory() {}

	public static Admin create(@NonNull final Login login, @NonNull final Pass pass, @NonNull final Email email, @NonNull final CreationDate creationDate) {
		return new Admin(login, pass, email, creationDate);
	}
}
