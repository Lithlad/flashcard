package design.admin.domain;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;

import design.admin.domain.communication.events.AdminWasCreatedEvent;
import design.domain.CreationDate;
import design.admin.domain.dto.AdminDTO;
import design.user.domain.Email;
import design.user.domain.Login;
import design.user.domain.Pass;
import design.user.domain.User;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "admn_admin")
@NoArgsConstructor
public class Admin extends User {

	public Admin(final Login login, final Pass pass, final Email email, final CreationDate creationDate) {
		super(creationDate, login, pass, email);
		registerEvent(new AdminWasCreatedEvent(login, email, creationDate, getConcurrencyVersion()));
	}

	public AdminDTO mapToDTO() {
		return new AdminDTO(
			login.value(),
			email.value(),
			locked.isLocked(),
			enabled.isEnabled(),
			creationDate.value(),
			getConcurrencyVersion());
	}

	@Override
	protected List<String> getRoles() {
		return List.of("ADMIN");
	}
}
