package design.admin.domain.dto;

import java.time.LocalDateTime;

import javax.validation.constraints.NotBlank;

import design.user.domain.dto.UserDTO;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;

@Getter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class AdminDTO extends UserDTO {

	public AdminDTO(
		final @NotBlank String login,
		final @NotBlank String email,
		final boolean locked,
		final boolean enabled,
		@NonNull final LocalDateTime creationDate,
		final long concurrencyVersion) {
		super(login, email, locked, enabled, creationDate, concurrencyVersion);
	}
}
