package design.admin.domain;

import design.user.domain.Login;
import lombok.NonNull;

public interface AdminRepository {

	void save(@NonNull final Admin admin);

	boolean doesExist(@NonNull final Login login);
}
