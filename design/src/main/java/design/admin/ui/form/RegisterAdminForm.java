package design.admin.ui.form;

import javax.validation.constraints.NotBlank;

import design.admin.application.command.commands.RegisterAdminCommand;
import io.swagger.annotations.ApiModel;
import design.user.ui.form.RegisterUserForm;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString(callSuper = true)
@ApiModel
@EqualsAndHashCode(callSuper = true)
public class RegisterAdminForm extends RegisterUserForm implements RegisterAdminCommand {
	public RegisterAdminForm(final @NotBlank String login, final @NotBlank String pass, final @NotBlank String email) {
		super(login, pass, email);
	}
}
