package design.admin.ui;

import static org.springframework.http.HttpStatus.CREATED;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import design.admin.application.command.AdminApplicationService;
import design.admin.ui.form.RegisterAdminForm;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.NonNull;

@Api
@RestController
@RequestMapping("/api/admin")
public class AdminController {

	private final AdminApplicationService applicationService;

	@Autowired
	public AdminController(final AdminApplicationService applicationService) {
		this.applicationService = applicationService;
	}

	@ApiOperation("Admin registration")
	@PostMapping("/register")
	protected ResponseEntity registerLearner(@ApiParam(required = true)
	                                         @NonNull @Validated @RequestBody RegisterAdminForm form) {
		applicationService.register(form);
		return new ResponseEntity(CREATED);
	}
}
