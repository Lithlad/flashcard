package design.admin.application.command.commands;

import design.user.application.command.commands.RegisterUserCommand;

public interface RegisterAdminCommand extends AdminCommand, RegisterUserCommand {
}
