package design.admin.application.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.event.TransactionalEventListener;

import design.admin.application.command.commands.RegisterAdminCommand;
import design.domain.CreationDate;
import design.domain.DomainEvent;
import design.infrastructure.TimeDefinitions;
import design.admin.domain.Admin;
import design.admin.domain.AdminFactory;
import design.admin.domain.AdminRepository;
import design.admin.domain.communication.events.AdminEvent;
import design.user.application.command.UserInstanceApplicationService;
import design.user.domain.Email;
import design.user.domain.Login;
import design.user.domain.Pass;
import design.user.domain.UserRepository;
import lombok.NonNull;

@Service
public class AdminApplicationService extends UserInstanceApplicationService<Admin, RegisterAdminCommand> {

	private final AdminRepository jpaAdminRepository;

	private final TimeDefinitions timeDefinitions;

	@Autowired
	protected AdminApplicationService(final UserRepository userRepository, final AdminRepository jpaAdminRepository, final TimeDefinitions timeDefinitions) {
		super(userRepository);
		this.jpaAdminRepository = jpaAdminRepository;
		this.timeDefinitions = timeDefinitions;
	}

	@Override
	@TransactionalEventListener(value = AdminEvent.class)
	protected void actionWasRaised(final @NonNull DomainEvent event) {
		superActionWasRaised(event);
	}

	@Override
	protected void saveUser(final Admin user) {
		jpaAdminRepository.save(user);
	}

	@Override
	protected Admin createUserInstance(final RegisterAdminCommand command) {
		final Login login = Login.create(command.getLogin());
		final Pass pass = Pass.create(command.getPass());
		final Email email = Email.create(command.getEmail());
		return AdminFactory.create(login, pass, email, CreationDate.create(timeDefinitions.now()));
	}

	@Override
	protected boolean isAppropriate(final @NonNull Login login) {
		return jpaAdminRepository.doesExist(login);
	}
}
