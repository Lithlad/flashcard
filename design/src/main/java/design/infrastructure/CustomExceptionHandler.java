package design.infrastructure;

import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import design.domain.exceptions.DomainAlreadyExistsException;
import design.domain.exceptions.DomainException;
import design.domain.exceptions.DomainInvalidConcurrencyVersionException;
import design.domain.exceptions.DomainNotFoundException;
import design.domain.exceptions.DomainRequirementsNotFulfilledException;
import design.domain.exceptions.DomainUnauthorizedException;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler  {

	public static final String INTERNAL_SYSTEM_EXCEPTION_ERROR_CODE = "internal.system.exception";
	public static final String OPTIMISTIC_LOCK_EXCEPTION_ERROR_CODE = "optimistic.lock.exception";
	public static final String PERSISTENCE_EXCEPTION_ERROR_CODE = "persitence.exception";

	private static final Logger LOG = LogManager.getLogger(CustomExceptionHandler.class);

	@ExceptionHandler(DomainAlreadyExistsException.class)
	protected ResponseEntity<Object> handleDomainAlreadyExistsException(final DomainAlreadyExistsException exception, final WebRequest request) {
		LOG.error(exception);
		return handleExceptionInternal(exception, mapToExceptionResponse(exception),
			new HttpHeaders(), HttpStatus.PRECONDITION_FAILED, request);
	}

	@ExceptionHandler(DomainInvalidConcurrencyVersionException.class)
	protected ResponseEntity<Object> handleDomainAlreadyExistsException(final DomainInvalidConcurrencyVersionException exception, final WebRequest request) {
		LOG.error(exception);
		return handleExceptionInternal(exception, mapToExceptionResponse(exception),
			new HttpHeaders(), HttpStatus.UNAUTHORIZED, request);
	}

	@ExceptionHandler(DomainNotFoundException.class)
	protected ResponseEntity<Object> handleDomainNotFoundException(final DomainNotFoundException exception, final WebRequest request) {
		LOG.error(exception);
		return handleExceptionInternal(exception, mapToExceptionResponse(exception),
			new HttpHeaders(), HttpStatus.NOT_FOUND, request);
	}

	@ExceptionHandler(DomainRequirementsNotFulfilledException.class)
	protected ResponseEntity<Object> handleDomainRequirementsNotFulfilledException(final DomainRequirementsNotFulfilledException exception, final WebRequest request) {
		LOG.error(exception);
		return handleExceptionInternal(exception, mapToExceptionResponse(exception),
			new HttpHeaders(), HttpStatus.PRECONDITION_FAILED, request);
	}

	@ExceptionHandler(DomainUnauthorizedException.class)
	protected ResponseEntity<Object> handleDomainRequirementsNotFulfilledException(final DomainUnauthorizedException exception, final WebRequest request) {
		LOG.error(exception);
		return handleExceptionInternal(exception, mapToExceptionResponse(exception),
			new HttpHeaders(), HttpStatus.UNAUTHORIZED, request);
	}

	@ExceptionHandler(OptimisticLockException.class)
	protected ResponseEntity<Object> handleOptimisticLockExceptionException(final OptimisticLockException exception, final WebRequest request) {
		LOG.error(exception);
		final ExceptionResponse exceptionResponse = new ExceptionResponse(OPTIMISTIC_LOCK_EXCEPTION_ERROR_CODE, exception.getMessage());

		return handleExceptionInternal(exception, exceptionResponse,
			new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
	}

	@ExceptionHandler(PersistenceException.class)
	protected ResponseEntity<Object> handlePersistanceException(final PersistenceException exception, final WebRequest request) {
		LOG.error(exception);
		final ExceptionResponse exceptionResponse = new ExceptionResponse(PERSISTENCE_EXCEPTION_ERROR_CODE, exception.getMessage());

		return handleExceptionInternal(exception, exceptionResponse,
			new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
	}

	@ExceptionHandler(Exception.class)
	protected ResponseEntity<Object> handleInternalSystemException(final Exception exception, final WebRequest request) {
		LOG.error(exception);
		final ExceptionResponse exceptionResponse = new ExceptionResponse(INTERNAL_SYSTEM_EXCEPTION_ERROR_CODE, exception.getMessage());

		return handleExceptionInternal(exception, exceptionResponse,
			new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
	}

	private ExceptionResponse mapToExceptionResponse(final DomainException domainException) {
		return new ExceptionResponse(domainException.getErrorCode(), domainException.getMessage());
	}

	private class ExceptionResponse {
		public final String errorCode;
		public final String errorMessage;

		private ExceptionResponse(final String errorCode, final String errorMessage) {
			this.errorCode = errorCode;
			this.errorMessage = errorMessage;
		}
	}
}
