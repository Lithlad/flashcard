package design.infrastructure.auditing;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import design.user.application.query.UserViewQueryService;

@Configuration
@EnableJpaAuditing(auditorAwareRef = "auditorProvider")
public class AuditingConfiguration {

	private final UserViewQueryService userQueryService;

	@Autowired
	public AuditingConfiguration(final UserViewQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}

	@Bean
	public AuditorAware<String> auditorProvider() {
		return () -> Optional.of(userQueryService.currentUserLogin().value());
	}
}
