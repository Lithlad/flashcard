package design.infrastructure.repository.criteria;

import lombok.Getter;
import lombok.NoArgsConstructor;

//https://www.baeldung.com/spring-mvc-custom-data-binder
// Spring doesnt map type to abstract T. It is why specified classes are needed
@Getter
@NoArgsConstructor
public class SearchCriterium<T> {

	private SearchOperation operation;

	private T value;

	public SearchCriterium(final SearchOperation operation, final T value) {
		this.operation = operation;
		this.value = value;
	}
}
