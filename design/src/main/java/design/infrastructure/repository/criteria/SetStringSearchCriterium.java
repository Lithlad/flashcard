package design.infrastructure.repository.criteria;

import java.util.Set;

import lombok.Data;

@Data
public class SetStringSearchCriterium {

	private Set<String> values;
}
