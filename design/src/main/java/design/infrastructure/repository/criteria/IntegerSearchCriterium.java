package design.infrastructure.repository.criteria;

import lombok.Data;

// Requires getter and setter - no constructor allowed
@Data
public class IntegerSearchCriterium implements SearchCriteriumGeneralizer<Integer> {

	private SearchOperation operation;

	private Integer value;

	public SearchCriterium<Integer> generalize() {
		return new SearchCriterium<>(operation, value);
	}
}
