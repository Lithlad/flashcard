package design.infrastructure.repository.criteria;

public enum SearchOperation {
	GREATER,
	LESS,
	SAME,
	IN
}
