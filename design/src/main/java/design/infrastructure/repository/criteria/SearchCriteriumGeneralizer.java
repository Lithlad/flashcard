package design.infrastructure.repository.criteria;

public interface SearchCriteriumGeneralizer<T> {
	SearchCriterium<T> generalize();
}
