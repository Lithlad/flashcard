package design.infrastructure.repository.criteria;

import lombok.Data;

// Requires getter and setter - no constructor allowed
@Data
public class DoubleSearchCriterium implements SearchCriteriumGeneralizer<Double> {

	private SearchOperation operation;

	private Double value;

	public SearchCriterium<Double> generalize() {
		return new SearchCriterium<>(operation, value);
	}
}
