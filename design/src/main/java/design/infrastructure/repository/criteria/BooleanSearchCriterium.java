package design.infrastructure.repository.criteria;

import lombok.Data;

// Requires getter and setter - no constructor allowed
@Data
public class BooleanSearchCriterium implements SearchCriteriumGeneralizer<Boolean> {

	private SearchOperation operation;

	private Boolean value;

	public SearchCriterium<Boolean> generalize() {
		return new SearchCriterium<>(operation, value);
	}
}
