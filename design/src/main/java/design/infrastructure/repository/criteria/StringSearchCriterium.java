package design.infrastructure.repository.criteria;

import lombok.Data;

// Requires getter and setter - no constructor allowed
@Data
public class StringSearchCriterium implements SearchCriteriumGeneralizer<String> {

	private SearchOperation operation;

	private String value;

	public SearchCriterium<String> generalize() {
		return new SearchCriterium<>(operation, value);
	}
}
