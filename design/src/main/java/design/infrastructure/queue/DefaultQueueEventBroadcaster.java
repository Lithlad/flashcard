package design.infrastructure.queue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import design.domain.DomainEvent;
import design.domain.EventBroadcaster;
import lombok.NonNull;

@Component
public class DefaultQueueEventBroadcaster implements EventBroadcaster {

    private static final Logger logger = LogManager.getLogger(DefaultQueueEventBroadcaster.class);

    private final KafkaTemplate<String, DomainEvent> kafkaTemplate;

    @Autowired
    public DefaultQueueEventBroadcaster(final KafkaTemplate<String, DomainEvent> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void broadcast(@NonNull final DomainEvent event) {
        final String eventIdentifier = event.eventIdentifier() + " " + event.getEventDate();
        logger.info("Event to be broadcasted " + eventIdentifier);
        final ListenableFuture<SendResult<String, DomainEvent>> send = kafkaTemplate.send(event.eventIdentifier(), event);
        send.addCallback(new ListenableFutureCallback<SendResult<String, DomainEvent>>() {
            @Override
            public void onFailure(final Throwable throwable) {
                logger.warn("Event was not broadcasted " + eventIdentifier);
                // TODO resend
            }

            @Override
            public void onSuccess(final SendResult<String, DomainEvent> stringDomainEventSendResult) {
                logger.info("Event was successfully broadcasted " + eventIdentifier);
            }
        });
    }
}
