package design.infrastructure.queue.configuration;

import org.apache.kafka.clients.admin.NewTopic;

public abstract class QueueConfiguration {

    private static final int PARTITIONS_NUMBER = 1;

    private static final short REPLICATION_FACTOR = (short) 1;

    protected NewTopic generateTopic(final String topicName) {
        return new NewTopic(topicName, PARTITIONS_NUMBER, REPLICATION_FACTOR);
    }
}
