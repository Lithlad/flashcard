package design.infrastructure.queue.configuration;

import java.util.AbstractMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import design.domain.DomainEvent;

// TODO remove link
//https://www.baeldung.com/spring-kafka
@EnableKafka
@Configuration
public abstract class ConsumerQueueConfiguration<E extends DomainEvent> {

	@Bean
	public abstract ConcurrentKafkaListenerContainerFactory<String, E> listenerContainerFactoryBean(final ConsumerFactory<String, E> consumerFactory);

	@Bean
	public abstract ConsumerFactory<String, E> consumerFactoryBean();

	protected abstract Set<String> trustedPackages();

	protected abstract String groupId();

	protected ConsumerFactory<String, E> consumerFactory(final String bootstrapAddress) {
		final Map<String, Object> configProps = Stream.of(
			new AbstractMap.SimpleEntry<>(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress),
			new AbstractMap.SimpleEntry<>(ConsumerConfig.GROUP_ID_CONFIG, groupId()),
			new AbstractMap.SimpleEntry<>(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class),
			new AbstractMap.SimpleEntry<>(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class),
			new AbstractMap.SimpleEntry<>(JsonDeserializer.TRUSTED_PACKAGES, mapTrustedPackagesToString())
		).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
		return new DefaultKafkaConsumerFactory<>(configProps);
	}

	private String mapTrustedPackagesToString() {
		return trustedPackages().stream().collect(Collectors.joining(", "));
	}

	protected ConcurrentKafkaListenerContainerFactory<String, E> listenerContainerFactory(final ConsumerFactory<String, E> consumerFactory) {
		final ConcurrentKafkaListenerContainerFactory<String, E> factory =
			new ConcurrentKafkaListenerContainerFactory<>();
		factory.setConsumerFactory(consumerFactory);
		return factory;
	}
}
