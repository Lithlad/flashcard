package design.infrastructure.queue.configuration;

import java.util.AbstractMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaAdmin;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;

import design.domain.DomainEvent;

@Configuration
public class ProducerQueueConfiguration {

	private final String bootstrapAddress;

	public ProducerQueueConfiguration(@Value("${kafka.bootstrapAddress}") final String bootstrapAddress) {
		this.bootstrapAddress = bootstrapAddress;
	}

	@Bean
	public KafkaAdmin kafkaAdmin() {
		final Map<String, Object> configs = Stream.of(
			new AbstractMap.SimpleEntry<>(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress))
			.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

		return new KafkaAdmin(configs);
	}

	@Bean
	public ProducerFactory<String, DomainEvent> producerFactory() {
		final Map<String, Object> configProps = Stream.of(
			new AbstractMap.SimpleEntry<>(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress),
			new AbstractMap.SimpleEntry<>(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class),
			new AbstractMap.SimpleEntry<>(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class))
			.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
		// thread safe
		return new DefaultKafkaProducerFactory<>(configProps);
	}

	@Bean
	public KafkaTemplate<String, DomainEvent> kafkaTemplate() {
		// KafkaTemplate is thread safe
		return new KafkaTemplate<>(producerFactory());
	}
}
