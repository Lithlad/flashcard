package design.infrastructure;

import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class TimeDefinitions {

    public LocalDateTime now() {
        return LocalDateTime.now();
    }
}
