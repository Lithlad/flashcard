package design.infrastructure;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import design.application.query.AuthenticationInfo;

@Component
public class AuthenticationInfoDefaultImplementation implements AuthenticationInfo {

	@Override
	public String loggedUserLogin() {
		return getAuthentication().getName();
	}

	private Authentication getAuthentication() {
		return SecurityContextHolder.getContext().getAuthentication();
	}
}
