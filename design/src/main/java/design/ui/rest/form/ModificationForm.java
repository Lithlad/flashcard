package design.ui.rest.form;

import design.application.command.commands.ModificationCommand;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.ToString;

@ToString
@ApiModel
public class ModificationForm implements ModificationCommand {

	@ApiModelProperty(value = "Concurrency version", required = true, example = "4")
	private final int concurrencyVersion;

	public ModificationForm(final int concurrencyVersion) {
		this.concurrencyVersion = concurrencyVersion;
	}

	@Override
	public Integer getConcurrencyVersion() {
		return concurrencyVersion;
	}
}
