package design.ui.web.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api
@RestController
@RequestMapping("/api/status")
public class StatusController {

    @ApiOperation("Test if application is alive")
    @GetMapping("/alive")
    public ResponseEntity<String> isAlive() {
        return ResponseEntity.ok("Application is alive");
    }
}
