package design.ui.web;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.BasicAuth;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

//https://github.com/springfox/springfox
@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
            .groupName("Flashcards")
            .select()
            .apis(RequestHandlerSelectors.any())
            .paths(PathSelectors.ant("/api/**"))
            .build()
            .apiInfo(apiInfo())
            .securityContexts(Collections.singletonList(actuatorSecurityContext()))
            .securitySchemes(Collections.singletonList(basicAuth()))
            .useDefaultResponseMessages(false);

                // TODO fill after oauth installation
                // .paths(Predicates.or(tokenPath(), apiPath()))
                // .securitySchemes(Lists.newArrayList(
                //         apiAuthorizationScheme(),
                //         tokenAuthorizationScheme(),
                //         tokenGrantTypeScheme(),
                //         tokenRefreshTokenScheme(),
                //         tokenUserScheme(),
                //         tokenPassScheme()))
                // .securityContexts(Lists.newArrayList(tokenContext(), apiContext()));
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Flashcards")
                .description("Flashcard to ease foregin language leatning")
                .license("Copyright")
                .version("0.0.1-SNAPSHOT")
                .build();
    }

    private BasicAuth basicAuth() {
        return new BasicAuth("basicAuth");
    }

    private SecurityReference basicAuthReference() {
        return new SecurityReference("basicAuth", new AuthorizationScope[0]);
    }

    private SecurityContext actuatorSecurityContext() {
        return SecurityContext.builder()
            .securityReferences(Collections.singletonList(basicAuthReference()))
            .forPaths(PathSelectors.ant("/api/**"))
            .build();
    }
}
