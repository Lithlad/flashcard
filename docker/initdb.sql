-- schema.sql
-- Since we might run the import many times we'll drop if exists
CREATE DATABASE sonar_db;
CREATE USER sonar WITH encrypted password 'sonarsecretpassword';
CREATE DATABASE local_flashcard;
CREATE USER flashcard WITH encrypted password 'backend';
GRANT ALL PRIVILEGES ON DATABASE sonar_db TO sonar;
GRANT ALL PRIVILEGES ON DATABASE local_flashcard TO flashcard;

---- Make sure we're using our `local_flashcard` database
--\c local_flashcard;